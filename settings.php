<?php
// This file is part of Trainingtroops.
// 
// Trainingtroops is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trainingtroops is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trainingtroops.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config options generator
 *
 * @package     local_teacherboard
 * @author      Charly Piva
 * @copyright   (C) Académie de Versailles 2018
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


// Ensure the configurations for this site are set
if ( $hassiteconfig ){
 
	// Create the new settings page
	// - in a local plugin this is not defined as standard, so normal $settings->methods will throw an error as
	// $settings will be NULL
	$settings = new admin_settingpage( 'local_teacherboard', 'Teacherboard' );
 
	// Create 
	$ADMIN->add( 'localplugins', $settings );
 
	// Add a setting field to the settings for this page
	$settings->add( new admin_setting_configtextarea(
 
		// This is the reference you will use to your configuration
		'local_teacherboard/message',
 
		// This is the friendly title for the config, which will be displayed
		'Message',
 
		// This is helper text for this config field
		'Un message qui sera affiché au-dessus du Teacherboard',
 
		// This is the default value
		'',
 
		// This is the type of Parameter this config is
        PARAM_RAW
 
	) );


    $settings->add( new admin_setting_configtext(
    // This is the reference you will use to your configuration
        'local_teacherboard/messagebackgroundcolor',
        // This is the friendly title for the config, which will be displayed
        'Couleur de fond',
        // This is helper text for this config field
        'La couleur (CSS) de fond du message',
        // This is the default value
        '#47beda',
        // This is the type of Parameter this config is
        PARAM_TEXT
    ) );

    $settings->add( new admin_setting_configtext(
    // This is the reference you will use to your configuration
        'local_teacherboard/messagetextcolor',
        // This is the friendly title for the config, which will be displayed
        'Couleur du texte',
        // This is helper text for this config field
        'La couleur (CSS) du texte du message',
        // This is the default value
        '#ffffff',
        // This is the type of Parameter this config is
        PARAM_TEXT
    ) );
 
}


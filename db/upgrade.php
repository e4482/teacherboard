<?php
// This file keeps track of upgrades to
// the teacherboard module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installation to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the methods of database_manager class
//
// Please do not forget to use upgrade_set_timeout()
// before any action that may take longer time to finish.

defined('MOODLE_INTERNAL') || die();

function xmldb_local_teacherboard_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2017020800) {

        $group_table = new xmldb_table('teacherboard_group');

        $groupid = new xmldb_field('id');
        $groupid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
        $group_table->addField($groupid);

        $ownerid = new xmldb_field('userid');
        $ownerid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , 'id');
        $group_table->addField($ownerid);

        $groupname = new xmldb_field('name');
        $groupname->set_attributes(XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 'group', 'userid');
        $group_table->addField($groupname);

        $key1 = new xmldb_key('primary');
        $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
        $group_table->addKey($key1);
        $group_table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $status = $dbman->create_table($group_table);



        $group_members_table = new xmldb_table('teacherboard_group_members');


        $gmid = new xmldb_field('id');
        $gmid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null , null);
        $group_members_table->addField($gmid);

        $groupid = new xmldb_field('groupid');
        $groupid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $group_members_table->addField($groupid);

        $userid = new xmldb_field('userid');
        $userid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $group_members_table->addField($userid);

        $key1 = new xmldb_key('primary');
        $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
        $group_members_table->addKey($key1);
        $group_members_table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $group_members_table->add_index('groupid', XMLDB_INDEX_NOTUNIQUE, array('groupid'));
        $status = $dbman->create_table($group_members_table);

        upgrade_plugin_savepoint(true, 2017020800, 'local', 'teacherboard');
    }

    return true;
}

<?php
// This file is part of TeacherBoard
//
// Simplepage is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Simplepage is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
  
  global $CFG;
  include_once('local/teacherboard/lib/model/lib.php');
  class lib_test extends advanced_testcase
  {
    /*
     * Admin user hasRne
     * 
     */
    function testFunction1()
    {
      global $USER;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $this->setAdminUser();
      $USER->editing = 1;
      $currentUser = new teacherboard\User();
      $this->AssertFalse($currentUser->hasRne());	
    }
    /*
     * getCourseName
     * 
     */    
    function testFunction2()
    {
      global $USER;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $course1 = $this->getDataGenerator()->create_course(
        array('fullname'=>'toto'));
      $user1 = $this->getDataGenerator()->create_user();
      $this->setUser($user1);
      
      $currentUser = new teacherboard\User();
      $fullname = $currentUser->getCourseName($course1->id);
      $this->AssertEquals($course1->fullname, $fullname);	
    }   
    /*
     * renameCourse
     * 
     */    
    function testFunction3()
    {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $course1 = $this->getDataGenerator()->create_course(
        array('fullname'=>'toto'));
      $user1 = $this->getDataGenerator()->create_user();
      $this->setUser($user1);
      
      $currentUser = new teacherboard\User();
      $result = $currentUser->renameCourse($course1->id, 'titi');

      $course1_next = $DB->get_record('course', array('id'=>$course1->id));

      $this->AssertEquals($course1_next->fullname, 'titi');	
    }   

    /*
     * hasFolders
     * 
     */    
    function testFunction4()
    {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());

      $user1 = $this->getDataGenerator()->create_user();
      $this->setUser($user1);
      
      $currentUser = new teacherboard\User();
      $nbFolders = $currentUser->hasFolders();
      $this->AssertEquals($nbFolders, 0);	
      
      $currentUser->addRootFolder("folder:root");
      $currentUser->addRootFolder("folder:root");
      $nbFolders = $currentUser->hasFolders();
      $this->AssertEquals($nbFolders, 2);	
    } 
    /*
     * folderAddCourseButton
     * 
     */    
    function testFunction5()
    {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());

      $user1 = $this->getDataGenerator()->create_user();
      $this->setUser($user1);
      
      $currentUser = new teacherboard\User();
      $nbFolders = $currentUser->hasFolders();
      $this->AssertEquals($nbFolders, 0);	
      
      $folderid = $currentUser->addRootFolder("folder:root");
      $buttonHtml = $currentUser->folderAddCourseButton($folderid);
      $this->AssertEquals($buttonHtml, "<div style='margin:0px;background-color:#718DA3;color:white;text-align:center;line-height:2em;border-bottom-right-radius:7px;border-bottom-left-radius:7px;' data-folderid='".$folderid."' class='addcoursefolder'>Add a new course this folder</div>\n");	
    }     
  } 

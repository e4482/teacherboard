<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>

global $CFG;

include_once($CFG->dirroot . '/local/teacherboard/globals.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/controller/frontcontroller.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
require_once($CFG->libdir . '/formslib.php');

class ajaxaddfolder_test extends advanced_testcase
{
    /*
     * emulate folder creation
     *
     */

    function test1()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxaddfolder';
        $_POST['sesskey'] = $USER->sesskey;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $folder = $DB->get_record('teacherboard_page', array('userid' => $USER->id));

        $this->AssertContains("<li style=\\\"position:relative;\\\" class='dd-item' id='page_".$folder->id."' data-folderid='".$folder->id."'>", $a);
        $this->AssertEquals('New Folder', $folder->name);
        $this->AssertEquals('0', $folder->parentid);
        $this->AssertEquals('0', $folder->sortorder);


/*
EXPECTED OUTPUT

<li style="position:relative;" class="dd-item" id="page_2" data-folderid="2">
	<span style="position:absolute;width:100%;height:50px;" data-folderid="2" class="showactivities"></span>

    <span class="dd-handle" style="position:relative;">
        <img style="height:20px;" src="http://dev.elea.ac-versailles.fr/local/teacherboard/lib/template/images/default/move.png" alt="Déplacer ce dossier" title="Déplacer ce dossier">
    </span>
    <input class="addcoursefolder" data-folderid="2" type="hidden">
    <!-- <img style='height:20px;position:relative;' data-folderid='2' class='showactivities' src='http://dev.elea.ac-versailles.fr/local/teacherboard/lib/template/images/default/folder.png' alt='Afficher les parcours de ce dossier' title='Afficher les parcours de ce dossier' /> -->
    <img style="height:20px;position:relative;margin-left:20px;" class="edit_title" src="http://dev.elea.ac-versailles.fr/local/teacherboard/lib/template/images/default/edit.png" alt="Modifier le nom de ce dossier" title="Modifier le nom de ce dossier">
    <input style="position:relative;" class="input_course" name="2" value="Nouveau dossier" size="35" title="">

    <span style="float:right;position:relative;"><img style="height:20px;" data-folderid="2" class="deletepage" src="http://dev.elea.ac-versailles.fr/local/teacherboard/lib/template/images/default/delete.png" alt="Supprimer ce dossier" title="Supprimer ce dossier"></span>
    <table class="dd modules_table hideactivities" style="position:relative;" id="table_2">
        <!-- <thead>
            <tr>
                <th></th><th></th><th></th><th></th><th></th>
            </tr>
        </thead> -->
    </table>

</li>

*/

    }
    /*
     * no sesskey
     *
     */

    function test2()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxaddfolder';

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $this->AssertContains("Merci de bien vouloir vous reconnecter en cliquant <a href='\/login\/index.php'>ici<\/a>", $a);


    }
    /*
     * wrong sesskey
     *
     */

    function test3()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxaddfolder';
        $_POST['sesskey'] = "IRON MAN IS HERE";

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $this->AssertContains("Merci de bien vouloir vous reconnecter en cliquant <a href='\/login\/index.php'>ici<\/a>", $a);
    }
}

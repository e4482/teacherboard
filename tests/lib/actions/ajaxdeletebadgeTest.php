<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>

global $CFG;

include_once($CFG->dirroot . '/local/teacherboard/globals.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/controller/frontcontroller.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
require_once($CFG->libdir . '/formslib.php');

class ajaxdeletebadge_test extends advanced_testcase
{
    /*
     * emulate badge deletion
     *
     */

    function testBadgeDeletion()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course(
                        array('fullname'=>'toto')
                    );
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        $_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course1->id;
        $_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        $_POST['name'] = 'badge 1';
        $_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $badge1 = $DB->get_record('badge', array('courseid' => $course1->id));
        $this->AssertRegExp('/{"state":"success","id":' . $badge1->id . '}/', $a);
        $this->AssertEquals('badge 1', $badge1->name);

        // emulate AJAX POST Request deletion
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxdeletebadge';
        $_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course1->id;
        $_POST['id'] = $badge1->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();
        $this->AssertRegExp('/{"state":"success"}/', $a);

        $badge2 = $DB->get_record('badge', array('courseid' => $course1->id));
        $this->AssertEquals(BADGE_STATUS_ARCHIVED, $badge2->status);

    }

}

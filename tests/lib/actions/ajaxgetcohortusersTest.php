<?php
// This file is part of TeacherBoard
//
// Teacherboard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>

global $CFG;

include_once($CFG->dirroot . '/local/teacherboard/globals.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/controller/frontcontroller.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
include_once($CFG->dirroot . '/enrol/cohort/lib.php');
require_once($CFG->libdir . '/formslib.php');

class ajaxgetcohortusers_test extends advanced_testcase
{
    /*
     *
     *
     */

     public function setUp()
     {
       global $USER, $DB;
     }

    function test1() {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $this->setAdminUser();
      $USER->editing = 1;

      $categ = new StdClass;
      $categ->name = 'Établissements';
      $categ->sortorder = 1;
      $categ->id = $DB->insert_record('user_info_category', $categ);

      $userfield = new StdClass;
      $userfield->shortname = 'rne1';
      $userfield->name = 'rne1';
      $userfield->datatype = 'text';
      $userfield->descriptionformat = '1';
      $userfield->categoryid = $categ->id;
      $userfield->sortorder = 1;
      $userfield->required = 0;
      $userfield->locked = 0;
      $userfield->visible = 2;
      $userfield->forceunique = 0;
      $userfield->signup = 0;
      $userfield->param1 = 30;
      $userfield->param2 = 2048;
      $userfield->param3 = 0;
      $userfield->id = $DB->insert_record('user_info_field', $userfield);

      $this->rne = '0921241z';

      $userrne = new StdClass;
      $userrne->fieldid = $userfield->id;
      $userrne->data = $this->rne;
      $userrne->userid = $USER->id;
      $DB->insert_record('user_info_data', $userrne);

      $category = $this->getDataGenerator()->create_category(array('idnumber' => $this->rne));

      $cohort = new \stdClass();
      $cohort->contextid = context_coursecat::instance($category->id)->id;
      $cohort->name = "3A";
      $cohort->id = cohort_add_cohort($cohort);

      $user1 = $this->getDataGenerator()->create_user(array(
          'email'=>'user1@example.com',
          'username'=>'user1',
          'firstname'=>'Tony',
          'lastname'=>'Stark')
      );
      $user2 = $this->getDataGenerator()->create_user(array(
          'email'=>'user2@example.com',
          'username'=>'user2',
          'firstname'=>'Robert',
          'lastname'=>'Dawney Junior')
      );
      cohort_add_member($cohort->id, $user1->id);
      cohort_add_member($cohort->id, $user2->id);

      // emulate AJAX POST request
      $_SERVER['REQUEST_METHOD'] = 'POST';
      $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxgetcohortusers';
      $_POST['cohortid'] = $cohort->id;
      $_POST['sesskey'] = $USER->sesskey;

      $context = $DB->get_record('context', array('id' => $cohort->contextid));

      ob_start();
      $front = frontController::getInstance()->dispatch();
      $a=ob_get_contents();
      ob_end_clean();

      $this->AssertEquals($this->rne, $category->idnumber);
      $this->AssertContains('{"status":"success","content":"{\"'.$user2->id.'\":{\"id\":\"'.$user2->id.'\",\"firstname\":\"'.$user2->firstname.'\",\"lastname\":\"'.$user2->lastname.'\"},\"'.$user1->id.'\":{\"id\":\"'.$user1->id.'\",\"firstname\":\"'.$user1->firstname.'\",\"lastname\":\"'.$user1->lastname.'\"}}"}', $a);

    }
    /*
     * no sesskey
     *
     */

    function test2() {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $this->setAdminUser();
      $USER->editing = 1;

      // emulate AJAX POST request
      $_SERVER['REQUEST_METHOD'] = 'POST';
      $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxgetcohortusers';

      ob_start();
      $front = frontController::getInstance()->dispatch();
      $a=ob_get_contents();
      ob_end_clean();

      $this->AssertContains("Token non valide", $a);
    }
    /*
     * wrong sesskey
     *
     */

    function test3() {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $this->setAdminUser();
      $USER->editing = 1;

      // emulate AJAX POST request
      $_SERVER['REQUEST_METHOD'] = 'POST';
      $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxgetcohortusers';
      $_POST['sesskey'] = "IRON MAN IS HERE";

      ob_start();
      $front = frontController::getInstance()->dispatch();
      $a=ob_get_contents();
      ob_end_clean();

      $this->AssertContains("Token non valide", $a);
    }

    /*
     * wrong sesskey
     *
     */
    function test4() {
      global $USER, $DB;
      $this->resetAfterTest(true);
      $PAGE = new moodle_page();
      $PAGE->set_context(context_system::instance());
      $this->setAdminUser();
      $USER->editing = 1;

      $categ = new StdClass;
      $categ->name = 'Établissements';
      $categ->sortorder = 1;
      $categ->id = $DB->insert_record('user_info_category', $categ);

      $userfield = new StdClass;
      $userfield->shortname = 'rne1';
      $userfield->name = 'rne1';
      $userfield->datatype = 'text';
      $userfield->descriptionformat = '1';
      $userfield->categoryid = $categ->id;
      $userfield->sortorder = 1;
      $userfield->required = 0;
      $userfield->locked = 0;
      $userfield->visible = 2;
      $userfield->forceunique = 0;
      $userfield->signup = 0;
      $userfield->param1 = 30;
      $userfield->param2 = 2048;
      $userfield->param3 = 0;
      $userfield->id = $DB->insert_record('user_info_field', $userfield);

      $this->rne = '0921241z';

      $userrne = new StdClass;
      $userrne->fieldid = $userfield->id;
      $userrne->data = $this->rne;
      $userrne->userid = $USER->id;
      $DB->insert_record('user_info_data', $userrne);

      $category = $this->getDataGenerator()->create_category(array('idnumber' => $this->rne));

      $cohort = new \stdClass();
      $cohort->contextid = context_coursecat::instance($category->id)->id;
      $cohort->name = "3A";
      $cohort->id = cohort_add_cohort($cohort);

      $user1 = $this->getDataGenerator()->create_user(array(
          'email'=>'user1@example.com',
          'username'=>'user1',
          'firstname'=>'Tony',
          'lastname'=>'Stark')
      );
      $user2 = $this->getDataGenerator()->create_user(array(
          'email'=>'user2@example.com',
          'username'=>'user2',
          'firstname'=>'Robert',
          'lastname'=>'Dawney Junior')
      );
      cohort_add_member($cohort->id, $user1->id);
      cohort_add_member($cohort->id, $user2->id);

      // emulate AJAX POST request
      $_SERVER['REQUEST_METHOD'] = 'POST';
      $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxgetcohortusers';
      $_POST['cohortid'] = $cohort->id;
      $_POST['sesskey'] = $USER->sesskey;

      $context = $DB->get_record('context', array('id' => $cohort->contextid));

      ob_start();
      $front = frontController::getInstance()->dispatch();
      $a=ob_get_contents();
      ob_end_clean();

      $this->AssertEquals($this->rne, $category->idnumber);
      $this->AssertContains('{"status":"success","content":"{\"'.$user2->id.'\":{\"id\":\"'.$user2->id.'\",\"firstname\":\"'.$user2->firstname.'\",\"lastname\":\"'.$user2->lastname.'\"},\"'.$user1->id.'\":{\"id\":\"'.$user1->id.'\",\"firstname\":\"'.$user1->firstname.'\",\"lastname\":\"'.$user1->lastname.'\"}}"}', $a);

    }

}

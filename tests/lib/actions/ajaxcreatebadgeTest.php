<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>

global $CFG;

include_once($CFG->dirroot . '/local/teacherboard/globals.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/controller/frontcontroller.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
require_once($CFG->libdir . '/formslib.php');

class ajaxcreatebadge_test extends advanced_testcase
{
    /*
     * emulate badge creation
     *
     */

    function testBadgeCreation()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course(
                        array('fullname'=>'toto')
                    );
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        $_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course1->id;
        $_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        $_POST['name'] = 'badge 1';
        $_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $badge1 = $DB->get_record('badge', array('courseid' => $course1->id));
        $this->AssertRegExp('/{"state":"success","id":' . $badge1->id . '}/', $a);
        $this->AssertEquals('badge 1', $badge1->name);

        // emulate GET Request to view badges
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_GET['id'] = $course1->id;
        $_SERVER['REQUEST_URI'] = 'index.php?action=badges&id=' . $course1->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $badges = badges_get_badges(BADGE_TYPE_COURSE, $course1->id, '', '' , '', '');
        $coursecontext = context_course::instance($course1->id);
        foreach ($badges as &$badge) {
            $badge->image = moodle_url::make_pluginfile_url($coursecontext->id, 'badges', 'badgeimage', $badge->id, '/', "f1", false);
        }

        $badges = array_values($badges);
        $badge = $badges[0];
        $this->AssertContains("<img  style='width:50px;' src='".$badge->image."' alt='".$badge->name."' title='".$badge->name."' />", $a);

    }
    /*
     * no sesskey
     *
     */

    function testBadgeSecurity()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course(
                        array('fullname'=>'toto')
                    );
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        //$_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course1->id;
        $_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        $_POST['name'] = 'badge 1';
        $_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $this->AssertContains("Merci de bien vouloir vous reconnecter en cliquant <a href='\/login\/index.php'>ici<\/a>", $a);


    }
    /*
     * wrong sesskey
     *
     */

    function testBadgeSecurity2()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course(
                        array('fullname'=>'toto')
                    );
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        $_POST['sesskey'] = "IRON MAN IS HERE";
        $_POST['courseid'] = $course1->id;
        $_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        $_POST['name'] = 'badge 1';
        $_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $this->AssertContains("Merci de bien vouloir vous reconnecter en cliquant <a href='\/login\/index.php'>ici<\/a>", $a);

    }
    /*
     * missing POST entries
     *
     */

    function testBadgeSecurity3()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course(
                        array('fullname'=>'toto')
                    );
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        // emulate AJAX POST request
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        $_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course1->id;
        //$_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        //$_POST['name'] = 'badge 1';
        //$_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $badge1 = $DB->get_record('badge', array('courseid' => $course1->id));
        $this->AssertRegExp('/{"state":"success","id":' . $badge1->id . '}/', $a);
        $this->AssertEquals('Hmmm, trying to hack something ?', $badge1->name);

    }
    /*
     * Wrong capabilities
     *
     */

    function testBadgeSecurity4()
        {
        global $USER, $DB;
        $this->resetAfterTest(true);
        $PAGE = new moodle_page();
        $PAGE->set_context(context_system::instance());
        $this->setAdminUser();
        $USER->editing = 1;

        // create course with some activities
        $course1 = $this->getDataGenerator()->create_course();
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $module1 = $generator->create_instance(array('course'=>$course1->id));
        $module2 = $generator->create_instance(array('course'=>$course1->id));

        $course2 = $this->getDataGenerator()->create_course();

        // create teacher for course1 only
        $user1 = $this->getDataGenerator()->create_user();
        $this->setUser($user1);

        $teacherroleid = 3;
        $this->getDataGenerator()->enrol_user($user1->id, $course1->id, $teacherroleid, 'manual');

        // emulate AJAX POST request on course2
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_SERVER['REQUEST_URI'] = 'ajax.php?action=ajaxcreatebadge';
        $_POST['sesskey'] = $USER->sesskey;
        $_POST['courseid'] = $course2->id;
        $_POST['source'] = '/local/teacherboard/lib/template/images/badges/badge1.png';
        $_POST['name'] = 'badge 1';
        $_POST['modules'] = $module1->id . ";" . $module2->id;

        ob_start();
        $front = frontController::getInstance()->dispatch();
        $a=ob_get_contents();
        ob_end_clean();

        $this->AssertRegExp('/{"state":"fail","id":null}/', $a);

    }
}

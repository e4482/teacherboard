<?php

// This file is part of Teacherboard.
// 
// Teacherboard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Teacherboard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Teacherboard.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cohort key admission
 *
 * @package     local_teacherboard
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(__DIR__)) . '/config.php');
defined('MOODLE_INTERNAL') || die();

require_login();

$cohort = $DB->get_record('cohort', [
    'contextid' => 1,
    'idnumber' => 'acces-parcours-ressources'
]);

if (!is_object($cohort)) {
    throw new Exception("La cohorte n'existe pas sur cette plateforme !");
}

require_once($CFG->dirroot . '/cohort/lib.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url('/local/teacherboard/cohortkey.php'));

$PAGE->set_pagelayout('standard');
$PAGE->set_title("Intégrer une cohorte spéciale");
$PAGE->set_heading("Intégrer une cohorte spéciale");

echo $OUTPUT->header();

$case = null;
if ($_POST) {
    $key = filter_input(INPUT_POST, 'key', FILTER_SANITIZE_STRING);
    if ($key === 'parcours2017') {
        cohort_add_member($cohort->id, $USER->id);
        $case = "added";
    } else {
        $case = "error";
    }
} else if (cohort_is_member($cohort->id, $USER->id)) {
    $case = "added";
}

?>

<style type="text/css">
    label {
        white-space: nowrap;
    }
</style>

<?php if ($case === "added"): ?>
<h1><?php echo "Vous appartenez au groupe : $cohort->name" ?></h1>
<a class="btn btn-primary btn-lg" href="/" role="button">Revenir à la page d'accueil</a>
<?php endif; ?>

<?php if ($case === "error"): ?>
<div class="alert alert-danger" role="alert"><?php echo "La clé introduite est erronée !" ?></div>
<?php endif; ?>

<?php if ($case !== "added"): ?>
<form class="form-horizontal" method="post">
    <h2>Veuillez fournir la clé reçue pour rejoindre un groupe spécifique</h2>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nom du groupe</label>
        <div class="col-sm-10">
            <select class="form-control" id="name" name="name">
                <option><?php echo $cohort->name ?></option>
            </select>
            <!-- prevent firefox from autocompleting key field -->
            <input type="text" class="hidden" name="no-passwd" value="no-passwd" disabled>
        </div>
    </div>
    <div class="form-group">
        <label for="key" class="col-sm-2 control-label">Clé transmise</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="key" name="key">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Valider</button>
        </div>
</form>
<?php endif; ?>

<?php echo $OUTPUT->footer() ?>
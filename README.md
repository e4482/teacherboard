## teacherboard

### How to install

Just copy the teacherboard folder in the local directory

### How to compile

Begin with : yarn (or npm install)

Compile SCSS files : yarn build (or npm run build)
You can watch files : yarn watch (or npm run watch)

>>>
You can use a Docker image for not having to install Yarn : sudo docker run -it --rm -v $(pwd):/workspace -w /workspace kkarczmarczyk/node-yarn /bin/bash
>>>

### How to start unit tests

vendor/phpunit

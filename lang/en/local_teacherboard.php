<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// english
$string['pluginname'] = "teacherboard";
$string['local_teacherboard'] = "teacherboard";
$string['tbtitle'] = "My space";
$string['tbachievement'] = "Achievement of course ";

$string['teacherboard:addcourse'] = "Créer un parcours";
$string['teacherboard:deletecourse'] = "Supprimer le parcours";

$string['error:restricted'] = "Restricted area";
$string['error:nomoodlecategory'] = "You are not declared as a teacher on this plateform. Thus, you can not add new course.";

$string['addcourseicon'] = "Add new course";
$string['repositoryaccess'] = "Access repository";
$string['managegroups'] = "Manage groups";
$string['addgroups'] = "Add a group";
$string['course:shortname'] = "newcourse";
$string['course:affectation'] = "Following courses have been stored in your first folder";
$string['course:name'] = "New Course";
$string['course:editname'] = "Edit course name";
$string['course:prefixduplication'] = "copy - ";
$string['course:showhide'] = "Show/Hide this course";
$string['course:unenrol'] = "Unenrol from this course";
$string['course:delete'] = "Delete this course";
$string['course:params'] = "Edit course parameters";
$string['course:enrolcohorts'] = "Enrol/Unenrol cohorts";
$string['course:enrolteacher'] = "Enrol/Unenrol teachers";
$string['course:enrolmetacourse'] = "Link to users from another course";
$string['course:enrolledusers'] = "Enrolled users list";
$string['course:duplicate'] = "Duplicate this course";
$string['course:badges'] = "Manage badges";
$string['course:badges:add'] = "Add a new badge";
$string['course:badges:title'] = "Badges for ";
$string['course:badges:new'] = "New badge";

$string['achievement:label'] = "Achievement";
$string['achievement:others'] = "Individual enrollment";

$string['folder:root'] = "My courses";
$string['folder:name'] = "New Folder";
$string['folder:add'] = "Add new Folder";
$string['folder:contains_items'] = "Can't delete this folder because it contains courses";
$string['folder:move'] = "Move this folder";
$string['folder:showcourses'] = "Show courses included in this folder";
$string['folder:editname'] = "Edit the folder name";
$string['folder:delete'] = "Remove this folder";
$string['folder:addcourse'] = "Add a new course this folder";

$string['group:add'] = "Add new group";

$string['users:cohorts:title'] = "Users list";

$string['movePageAlternate'] = "Move this folder";
$string['title4'] = "Show list of courses included in this folder";
$string['title5'] = "Add new course in this folder";
$string['title8'] = "Folder name";
$string['title9'] = "Delete this folder";
$string['editTitle'] = "Modify the title of this folder";
$string['warningDeleteCourse'] = "Are you sure you want to remove this course, its activities and its students achievements ?";
$string['warningUnEnrolCourse'] = "Are you sure you want to unenrol yourself from this course ?";
$string['warningDeleteFolder'] = "This folder can't be removed because it is not empty";
$string['msgConfirmDeletion'] = "Confirm";
$string['message:validation'] = "Create course";
$string['cohorts:title'] = "Select cohorts you want to enrol :";
$string['teachers:title'] = "Select teachers you want to enrol :";
$string['metacourse:title'] = "Link users from another course:";

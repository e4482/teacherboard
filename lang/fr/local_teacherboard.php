<?php

// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// french
$string['pluginname'] = "teacherboard";
$string['local_teacherboard'] = "teacherboard";
$string['tbtitle'] = "Mon espace";
$string['tbachievement'] = "Suivi du parcours ";

$string['error:restricted'] = "Espace réservé";
$string['error:nomoodlecategory'] = "Vous n'êtes pas déclaré enseignant sur cette plateforme. Vous ne pouvez donc pas créer de parcours.";

$string['addcourseicon'] = "Ajouter un nouveau parcours";
$string['repositoryaccess'] = "Accéder à la Éléathèque";
$string['managegroups'] = "Gérer mes groupes";
$string['addgroups'] = "Ajouter un groupe";

$string['course:shortname'] = "parcours";
$string['course:affectation'] = "Les parcours suivants viennent d'être rangés dans le premier dossier.";
$string['course:name'] = "Nouveau Parcours";
$string['course:prefixduplication'] = "copie - ";
$string['course:showhide'] = "Afficher/Cacher ce parcours";
$string['course:params'] = "Paramètres de ce parcours";
$string['course:enrolcohorts'] = "Inscrire/Désinscrire des classes";
$string['course:enrolteacher'] = "Inscrire/Désinscrire des enseignants";
$string['course:enrolmetacourse'] = "Lier aux utilisateurs d&#145;un autre parcours";
$string['course:enrolledusers'] = "Liste des utilisateurs inscrits";
$string['course:duplicate'] = "Dupliquer ce parcours";
$string['course:editname'] = "Modifier le nom de ce parcours";
$string['course:delete'] = "Supprimer ce parcours";
$string['course:unenrol'] = "Se désinscrire de ce parcours";
$string['course:move'] = "Déplacer ce parcours";
$string['course:badges'] = "Gérer les badges";
$string['course:badges:add'] = "Ajouter un nouveau badge";
$string['course:badges:title'] = "Badges pour ";
$string['course:badges:new'] = "Nouveau badge";

$string['achievement:label'] = "Suivi";
$string['achievement:others'] = "Inscriptions individuelles";

$string['folder:root'] = "Mes parcours";
$string['folder:name'] = "Nouveau dossier";
$string['folder:add'] = "Ajouter un nouveau dossier";
$string['folder:contains_items'] = "Suppression du dossier impossible car il contient des parcours";
$string['folder:move'] = "Déplacer ce dossier";
$string['folder:showcourses'] = "Afficher les parcours de ce dossier";
$string['folder:editname'] = "Modifier le nom de ce dossier";
$string['folder:delete'] = "Supprimer ce dossier";
$string['folder:addcourse'] = "Ajouter un nouveau parcours dans ce dossier";

$string['group:add'] = "Ajouter un nouveau groupe";

$string['users:cohorts:title'] = "Liste des élèves";

$string['movePageAlternate'] = "Déplacer ce dossier";

$string['title4'] = "Afficher la liste des parcours de ce dossier";
$string['title5'] = "Ajouter un nouveau parcours dans ce dossier";
$string['title8'] = "Nom du dossier";
$string['title9'] = "Supprimer ce dossier";
$string['title4'] = "title4";
$string['title5'] = "title5";
$string['title8'] = "title8";
$string['editTitle'] = "Modifier le nom de ce dossier";
$string['warningDeleteCourse'] = "&Ecirc;tes-vous sûr de vouloir supprimer ce parcours avec toutes ses activités ainsi que les données utilisateurs ?";
$string['warningUnEnrolCourse'] = "&Ecirc;tes-vous sûr de vouloir vous désinscrire de ce parcours ?";
$string['msgConfirmDeletion'] = "Confirmer";
$string['message:validation'] = "Créer le parcours";
$string['warningDeleteFolder'] = "Ce dossier ne peut pas être supprimé car il n'est pas vide";
$string['cohorts:title'] = "Classes inscrites au parcours :";
$string['teachers:title'] = "Enseignants inscrits au parcours :";
$string['metacourse:title'] = "Lier aux utilisateurs d'un autre parcours :";

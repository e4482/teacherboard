<?php
/**
 * Created Nicolas Daugas
 * Date: 13/12/18
 * Time: 11:56
 */

namespace teacherboard;

class Role {

    private static $_instance;

    public $COURSE_CREATOR;
    public $EDITING_TEACHER;
    public $TEACHER;

    private function __construct()
    {
        global $DB;
        $this->COURSE_CREATOR = $DB->get_record('role', ['shortname' => 'coursecreator'], 'id')->id;
        $this->EDITING_TEACHER = $DB->get_record('role', ['shortname' => 'editingteacher'], 'id')->id;
        $this->TEACHER = $DB->get_record('role', ['shortname' => 'teacher'], 'id')->id;
    }

    public static function instance() {
        if (self::$_instance) {
            return self::$_instance;
        }
        self::$_instance = new Role();
        return self::$_instance;
    }
}

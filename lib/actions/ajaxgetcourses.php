﻿<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/course/lib.php");
require_once($CFG->dirroot.'/enrol/manual/locallib.php');

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ajaxgetcoursesAction extends Action
{
    private $output;

    public function launch(Request $request, Response $response)
    {
        global $CFG;

        $this->content = ['state' => 'fail'];
        if ($this->checkloggedin($request)) {
            $systemcontext = context_system::instance();

            $courseid = $request->getParam('courseid');
            if ($courseid) {
                $this->content['state'] = 'done';
                $currentuser = new teacherboard\User();
                $folders = $currentuser->getCoursesForMetaEnrolment($courseid);
                $this->content['html'] = $this->course_to_html($folders, $courseid);
            }
        }

        $response->addVar('content', $this->content);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/ajaxSuccess.php");
        $this->printOut();
    }

    private function course_to_html($folders, $courseid) {
        $this->init_output();

        foreach($folders as $folder) {
            $this->start_folder($folder->name, $folder->id);

            foreach($folder->courses as $course) {
                if ($course['course']->id !== $courseid) {
                    $this->course_output($course['course']->fullname, $course['course']->id, $course['enrolled']);
                }
            }

            $this->end_folder();
        }

        return $this->output;
    }

    private function init_output() {
        $output = "";
    }

    private function start_folder($foldername, $folderid) {
        $this->output .= "
            <ul class='metacoursepopup-folder'>
                <li><h4>$foldername</h4></li>
            </ul>
            <div id='tab-$folderid'>";
    }

    private function end_folder() {
        $this->output .= "</div>";
    }

    private function course_output($coursename, $courseid, $isenrolled) {
        if ($isenrolled) {
            $enrolmentclass = 'course_selected';
        } else {
            $enrolmentclass = 'course_not_selected';
        }
        $this->output .= "
            <li class='enrolmeta $enrolmentclass' data-id='$courseid'>
            <div class='enroldiv'>
                $coursename
            </div>
            </li>";
    }
}

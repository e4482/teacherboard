<?php

// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/output.php");

/**
 * show groups defined by current user
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class managegroupsAction extends Action
{

    public $coursecontext = null;

    /*
     *
     */
    public function launch(Request $request, Response $response)
    {
        global $CFG, $USER;

        require_login();

        $systemcontext = context_system::instance();
        $currentuser = new teacherboard\User();
        $output = new teacherboard\Output();
        if ($currentuser->hasRNE()) {
            $message = "";
            if (!$currentuser->hasGroups()) {
              $message = "aucun groupe n'est disponible actuellement";
            }
            else {
              $groups = $currentuser->getGroups();
              $message = $output->renderGroups($groups);
            }

            $response->addVar('message', $message);
            $response->addVar('currentuser', $currentuser);
            $response->addVar('mainBoard',
              $CFG->wwwroot . "/local/teacherboard/index.php");
            $response->addVar('sesskey', $USER->sesskey);
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/managegroupsSuccess.php");
            $this->printOut();
        }
        else {
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/forbiddenSuccess.php");
            $this->printOut();
        }
    }
}

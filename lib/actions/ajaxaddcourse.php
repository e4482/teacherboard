﻿<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursSimple.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursAvecCarte.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursInverse.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursGamifie.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursExpress.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursVide.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/course/lib.php");
include_once($CFG->libdir . "/badgeslib.php");
include_once($CFG->libdir . "/resourcelib.php");
include_once($CFG->dirroot . "/course/modlib.php");
require_once($CFG->dirroot.'/enrol/manual/locallib.php');
require_once($CFG->libdir.'/formslib.php');

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2016 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ajaxaddcourseAction extends Action
{

    private $editingteacherid;
    private $coursecreatorid;

    public function __construct($controller)
    {
        parent::__construct($controller);

        global $DB;
        $this->editingteacherid = $DB->get_record('role', ['shortname' => 'editingteacher'], 'id', MUST_EXIST)->id;
        $this->coursecreatorid = $DB->get_record('role', ['shortname' => 'coursecreator'], 'id', MUST_EXIST)->id;
    }


    public function launch(Request $request, Response $response)
    {
        global $CFG, $DB, $OUTPUT, $PAGE;
        global $USER;

        $this->content = [];
        if ($this->checkloggedin($request)) {
            // create new course
            $currentuser = new teacherboard\User();
            $data = new stdClass;
            $data->category = $currentuser->getDefaultCategory()->id; //mandatory
            if ($data->category) {
                $data->shortname = get_string('course:shortname', 'local_teacherboard') . "-" . uniqid(); //mandatory
                $data->idnumber = $USER->id . "-" . uniqid(); //mandatory
                $course_name = $request->getParam('course_name');
                $course_type = $request->getParam('course_type');

                if ($course_name) {
                    $data->fullname = base64_decode(str_replace(" ","+",$course_name));
                }
                else {
                    $data->fullname = get_string('course:name', 'local_teacherboard');
                }

                $data->enablecompletion = true;
                $data->coursedisplay = COURSE_DISPLAY_MULTIPAGE;

                $gabaritParcours = $this->gabaritClassFromCourseType($course_type);

                $numsections = $gabaritParcours->sectionNumber();
                if($numsections===-1) {
                    $data->format = 'singleactivity';
                    $data->activitytype = $gabaritParcours->makeContent();
                } else {
                    $data->numsections = $numsections;
                }

                // disable announcements forum auto-creation
                $data->newsitems = 0;
                // enable separated groups by default elea/platform#404
                $data->groupmode = 1;

                $newcourse = create_course($data);
                $coursecontext = context_course::instance($newcourse->id);

                // enrol user with editingteacher role + coursecreator role
                $enrolData = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$newcourse->id));
                $enrol_manual = enrol_get_plugin('manual');
                $enrol_manual->enrol_user($enrolData, $USER->id, $this->editingteacherid);
                $enrol_manual->enrol_user($enrolData, $USER->id, $this->coursecreatorid);

                // add image in the summary
                // First, add this image in the moodle file storage

                $fs = get_file_storage();

                $fileinfo = array(
                    'contextid' => $coursecontext->id, // ID of context
                    'component' => 'course',     // usually = table name
                    'filearea' => 'overviewfiles',     // usually = table name
                    'itemid' => 0,               // usually = ID of row in table
                    'filepath' => '/',           // any path beginning and ending in /
                    'filename' => 'course_desc.jpg'); // any filename
                $fs->create_file_from_pathname($fileinfo, $CFG->dirroot . '/local/teacherboard/images/course_desc.jpg');

                $folderid = $request->getParam('folderid');
                if ($folderid) {
                    $item = Array();
                    $item['course'] = $newcourse;
                    $item['folderitem'] = $currentuser->addCourseToFolder($folderid, $newcourse->id);
                    // immediat redirection to course view
                    //$content = $currentuser->getHtmlCourse($item);
                    $this->content['courseid'] = $newcourse->id;
                }
                rebuild_course_cache($newcourse->id, true);

                $PAGE->set_context(context_course::instance($newcourse->id));

                $gabaritParcours->makeContent($newcourse);

                $manager = new \core_completion\manager($newcourse->id);
                // Default completion parameter
                $modids = [];
                $modnames = [];
                foreach ($allmodulesobj = $DB->get_records('modules', null, 'id ASC', 'id, name') as $moduleobj) {
                    $modids[] = $moduleobj->id;
                    $modnames[] = $moduleobj->name;
                }

                $modidstobechanged = $modids;
                $completiondata = (object) [
                    'id' => $newcourse->id,
                    'completion' => COMPLETION_TRACKING_AUTOMATIC,
                    'completionview' => COMPLETION_VIEW_REQUIRED,
                    'completionexpected' => 0,
                    'completionusegrade' => 1,
                    'modids' => $modidstobechanged
                ];
                $manager->apply_default_completion($completiondata, false);
                // For labels and mapmodules :
                $completiondata->completion = COMPLETION_TRACKING_NONE;
                $modidstobechanged = [];
                foreach ($allmodulesobj as $moduleobj) {
                    if ($moduleobj->name==='label' || $moduleobj==='mapmodules') {
                        $modidstobechanged[] = $moduleobj->id;
                    }
                }
                $completiondata->modids = $modidstobechanged;
                $manager->apply_default_completion($completiondata, true);
            }

        }


        $response->addVar('content', $this->content);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/ajaxSuccess.php");
        $this->printOut();
    }

    /**
     * return a new object of the fitted class from the type
     */
    public function gabaritClassFromCourseType($course_type)
    {

        switch ($course_type) {
            case 'qcm':
                return new ParcoursSimple('simpleqcm');
            case 'epikmatching':
                return new ParcoursSimple('epikmatching');
            case 'millionnaire':
                return new ParcoursSimple('millionnaire');
            case 'devoir':
                return new ParcoursSimple('');
            case 'parcoursAvecCarte':
                return new ParcoursAvecCarte();
            case 'parcoursGamifie':
                return new ParcoursGamifie();
            case 'parcoursClasseInverse':
                return new ParcoursInverse();
            case 'parcoursExpress':
                return new ParcoursExpress();
            case 'parcoursVide':
                return new ParcoursVide();
            default:
                throw new InvalidArgumentException('Course name not managed: '. $course_type);
        }

    }

}

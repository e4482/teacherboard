﻿<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/course/lib.php");
require_once($CFG->dirroot.'/enrol/manual/locallib.php');
require_once($CFG->dirroot.'/enrol/cohort/locallib.php');

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2015 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ajaxtoggleenrolcohortAction extends Action
{
    public function launch(Request $request, Response $response)
    {
        global $CFG;

        $this->content = [];

        if ($this->checkloggedin($request)) {
            $courseid = $request->getParam('courseid');
            $cohortid = $request->getParam('cohortid');
            if ($courseid && $cohortid) {
                $currentuser = new teacherboard\User();
                $this->content['cohortstate'] = $currentuser->toggleEnrolCohort($cohortid, $courseid);
            }

            $trace = new null_progress_trace();
            enrol_cohort_sync($trace,$courseid);
            $trace->finished();
        }
        $response->addVar('content', $this->content);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/ajaxSuccess.php");
        $this->printOut();
    }
}

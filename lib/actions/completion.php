<?php

// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



global $CFG;
include_once($CFG->dirroot . "/course/lib.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");

/**
 * prepare a page display
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2015 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class completionAction extends Action
{

    public $coursecontext = null;

    /*
     *
     */
    public function launch(Request $request, Response $response)
    {
        global $CFG, $USER;

        require_login();

        $currentuser = new teacherboard\User();
        $message = "";
        $courseid = $request->getParam('id');
        $students = null;
        $coursename = "";
        $activities = $this->getActivitiesInfo($courseid);
        if ($courseid) {
            $enrols = $currentuser->getStudentsCompletionByEnrol($courseid);
            $coursename = $currentuser->getCourseName($courseid);
        }
        // nb of columns used in array to show courses parameters
        $response->addVar('arrayNbColumns', $currentuser->getNbColumns());
        $response->addVar('activities', $activities);
        $response->addVar('enrols', $enrols);
        $response->addVar('coursename', $coursename);
        $response->addVar('message', $message);
        $response->addVar('mainBoard',
                $CFG->wwwroot . "/local/teacherboard/index.php");
        $response->addVar('sesskey', $USER->sesskey);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/completionSuccess.php");
        $this->printOut();

    }

    private function getActivitiesInfo($courseid) {
        $modinfos = get_array_of_activities($courseid);
        $activities = [];
        foreach ($modinfos as $modinfo) {
            $deletioninprogress = isset($modinfo->deletioninprogress) && $modinfo->deletioninprogress;
            $trackingcompletion = isset($modinfo->completion) && $modinfo->completion != COMPLETION_TRACKING_NONE;
            if ($trackingcompletion && !$deletioninprogress) {
                $name = $modinfo->name;
                $moduletype = $modinfo->mod;
                $activities[] = [
                    'name' => $name,
                    'module' => $moduletype
                ];
            }
        }

        return $activities;
    }
}

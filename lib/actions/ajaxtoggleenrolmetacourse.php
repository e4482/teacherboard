﻿<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/course/lib.php");
require_once($CFG->dirroot.'/enrol/manual/locallib.php');

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2015 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ajaxtoggleenrolmetacourseAction extends Action
{
    public function launch(Request $request, Response $response)
    {
        global $CFG, $DB;

        $this->content = [];
        if ($this->checkloggedin($request)) {
            $coursetoenrol = $request->getParam('coursetoenrol');
            $metacourse = $request->getParam('metacourse');
            $toenrol = $request->getParam('toenrol')!=='false';
            if ($coursetoenrol != $metacourse) { // Avoid unpredictable loops
                $enrol = $DB->get_record('enrol', ['enrol' => 'meta', 'courseid' => $coursetoenrol, 'customint1' => $metacourse]);

                $enrolplugin = enrol_get_plugin('meta');

                if($enrol!==false && !$toenrol) {
                    // Unenrolling
                    $enrolplugin->delete_instance($enrol);
                    $this->content['metaenrol'] = false;
                } else if ($enrol===false && $toenrol) {
                    // Enrolling
                    $enrolplugin->add_instance($DB->get_record('course', ['id' => $coursetoenrol])
                        , ['customint1' => $metacourse]);
                    $this->content['metaenrol'] = true;
                } else {
                    // nothing to do as it's miraculously done
                    error_log("Inconsistency between request and database : course $coursetoenrol :
                    \twant to have a metacourse enrolment = $toenrol
                    \twith course $metacourse and that's already the case!");
                    $this->content['metaenrol'] = $toenrol;
                }
            }
        }
        $response->addVar('content', $this->content);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/ajaxSuccess.php");
        $this->printOut();
    }
}

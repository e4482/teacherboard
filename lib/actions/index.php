<?php

// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/actions/action.class.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/model/lib.php");
include_once($CFG->dirroot . "/course/lib.php");
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursLTI.php");

/**
 * prepare a page display
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2015 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class indexAction extends Action
{

    public $coursecontext = null;

    /*
     *
     */
    public function launch(Request $request, Response $response)
    {
        global $CFG, $USER, $PAGE;
        $PAGE->set_pagelayout('standard');
        $systemcontext = context_system::instance();
        $PAGE->set_context($systemcontext);
        $message = "";

        require_login();

        if (isset($USER->username) && ($USER->username != "guest")) {
            $currentuser = new teacherboard\User();
            //$currentuser->dumpCourses();
            /*if (has_capability('local/teacherboard:addcourse', $systemcontext)) {
                $response->addVar('addcourse',
                    $CFG->wwwroot . '/'. IMG_FOLDER .'/addcourse.png');
            }*/

            // if user has no folder yet, just create one
            $folderid = $currentuser->getFirstFolder();

            /*if (!$currentuser->getDefaultCategory()) {

                $message = get_string("error:nomoodlecategory", "local_teacherboard");
            }*/

            // if user is enrolled in courses not already stored in folder, store
            // them in the first folder

            $message .= $currentuser->checkOrphansCourses();

            $cohortsList = $currentuser->getCohorts();
            $cohortsListHtml = $currentuser->getCohortsHtml($cohortsList);

            // build nested array of user pages
            $tab = $currentuser->getChainedPages();
            reset($tab);
            current($tab);
            $i = key($tab);
            ob_start();
            $currentuser->generateHtmlPagesTree($i, $tab, 0);
            $usertree = ob_get_contents();
            ob_end_clean();


            $user_picture=new user_picture($USER);
            $user_picture->size = 128;
            $srcuserprofil=$user_picture->get_url($PAGE);
            $response->addVar('srcUserProfil', $srcuserprofil);

            // nb of columns used in array to show courses parameters
            $response->addVar('arrayNbColumns', $currentuser->getNbColumns());

            // html reprsentation of user tree folders
            $response->addVar('usertree', $usertree);
            $response->addVar('selectedFolder', $currentuser->getSelectedFolder());
            $response->addVar('currentuser', $currentuser);
            $response->addVar('folderid_prefix', $currentuser->get('folderid_prefix'));
            $response->addVar('cohortsListHtml', $cohortsListHtml);
            $response->addVar('message', $message);
            $response->addVar('sesskey', $USER->sesskey);
            $response->addVar('userid', $USER->id);
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/indexSuccess.php");
            $this->printOut();
        }
        else {
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/forbiddenSuccess.php");
            $this->printOut();
        }
    }
}

﻿<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
include_once($CFG->dirroot . '/local/teacherboard/lib/actions/action.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
require_once($CFG->libdir . '/formslib.php');

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2016 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ajaxcreatebadgeAction extends Action
{
    function add_badge($courseid, $badgename, $badgeimage, $modulesid)
    {
        global $USER, $DB, $CFG;
        $fordb = new stdClass();
        $fordb->id = null;
        $now = time();
        $fordb->name = $badgename;
        $fordb->description = "Badge de validation d'activités";
        $fordb->timecreated = $now;
        $fordb->timemodified = $now;
        $fordb->usercreated = $USER->id;
        $fordb->usermodified = $USER->id;
        $fordb->issuername = "ELEA";
        $fordb->issuerurl = "plateforme elea";
        $fordb->issuercontact = "noreply-dane@ac-versailles.fr";
        $fordb->expiredate = null;
        $fordb->expireperiod = null;
        $fordb->type = BADGE_TYPE_COURSE;
        $fordb->courseid = $courseid;
        $fordb->messagesubject = get_string('messagesubject', 'badges');
        $fordb->message = get_string('messagebody', 'badges',
                html_writer::link(
                    $CFG->wwwroot . '/badges/mybadges.php',
                    get_string('managebadges', 'badges'
                    )
                ));
        $fordb->attachment = 1;
        $fordb->notification = BADGE_MESSAGE_ALWAYS;
        $fordb->status = BADGE_STATUS_ACTIVE;

        $newid = $DB->insert_record('badge', $fordb, true);

        $tempbadge = tempnam('/tmp', 'UL_IMAGE');
        file_put_contents($tempbadge, $badgeimage);

        $newbadge = new badge($newid);
        badges_process_badge_image($newbadge, $tempbadge);

        if (count($modulesid)) {

            // insertion needed to avoid error on badges/renderer.php
            $data = new \stdClass();
            $data->badgeid = $newid;
            $data->criteriatype = BADGE_CRITERIA_TYPE_OVERALL;
            $data->method = BADGE_CRITERIA_AGGREGATION_ALL;
            $data->descriptionformat = 1;
            $critid = $DB->insert_record('badge_criteria', $data, true);

            // creation badge criteria
            $data = new \stdClass();
            $data->badgeid = $newid;
            $data->criteriatype = BADGE_CRITERIA_TYPE_ACTIVITY;
            $data->method = BADGE_CRITERIA_AGGREGATION_ALL;
            $data->descriptionformat = 1;
            $critid = $DB->insert_record('badge_criteria', $data, true);
        }

        // insert modules in the criteria
        foreach ($modulesid as $moduleid) {

            $data = new \stdClass();
            $data->critid = $critid;
            $data->name = "module_" . $moduleid;
            $data->value = $moduleid;
            $DB->insert_record('badge_criteria_param', $data, true);

        }
        return $newid;

    }

    public function launch(Request $request, Response $response)
    {
        global $CFG, $DB, $OUTPUT, $PAGE;


        $this->content = array('state' => 'fail', 'id' => NULL);

        $courseid = $request->getParam('courseid');
        if ($this->checkloggedin($request, $courseid)) {
            $this->coursecontext = context_course::instance($courseid);

            if (has_capability(PERMISSION_MANAGE_BADGES, $this->coursecontext)) {

                $badgesource = $request->getParam('source');
                $badgeimage = NULL;
                if ($badgesource) {
                    if (strpos($badgesource, ';base64,') !== false) {
                        $start = strpos($badgesource, ';base64,') + strlen(';base64,');
                        $badgeimage = base64_decode(substr($badgesource, $start));
                    }
                    else {
                        $badgeimage = file_get_contents($CFG->dirroot . $badgesource);
                    }
                }
                else {
                    $badgeimage = file_get_contents($CFG->dirroot . '/local/teacherboard/lib/template/images/badges/badge1.png');
                }
                $badgename = $request->getParam('name');
                if ($badgename == NULL) {
                    $badgename = "Hmmm, trying to hack something ?";
                }
                $modulesid = explode(";", $request->getParam('modules'));

                $currentuser = new teacherboard\User();
                $this->content['state'] = 'success';
                $this->content['id'] = $this->add_badge($courseid, $badgename, $badgeimage, $modulesid);
            }
        }

        $response->addVar('content', $this->content);
        $this->render($CFG->dirroot . "/local/teacherboard/lib/template/ajaxSuccess.php");
        $this->printOut();
    }
}

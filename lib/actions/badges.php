<?php

// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



global $CFG;
include_once($CFG->dirroot . '/local/teacherboard/lib/actions/action.class.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/model/lib.php');
require_once($CFG->libdir . '/badgeslib.php');
/**
 * manage badges
 *
 * @package    teacherboard
 * @subpackage actions
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class badgesAction extends Action
{
    public $coursecontext = null;

    /*
     * 	badges white list
     */

    public function badgesImages() {
        return Array(
            Array(
            'title' => 'astro1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_astro1.png'
            ),
            Array(
            'title' => 'astro2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_astro2.png'
            ),
            Array(
            'title' => 'astro3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_astro3.png'
            ),
            Array(
            'title' => 'astro4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_astro4.png'
            ),
            Array(
            'title' => 'astro5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_astro5.png'
            ),
            Array(
            'title' => 'bio1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_bio1.png'
            ),
            Array(
            'title' => 'bio2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_bio2.png'
            ),
            Array(
            'title' => 'bio3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_bio3.png'
            ),
            Array(
            'title' => 'bio4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_bio4.png'
            ),
            Array(
            'title' => 'bio5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_bio5.png'
            ),
            Array(
            'title' => 'geo1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_geo1.png'
            ),
            Array(
            'title' => 'geo2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_geo2.png'
            ),
            Array(
            'title' => 'geo3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_geo3.png'
            ),
            Array(
            'title' => 'geo4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_geo4.png'
            ),
            Array(
            'title' => 'geo5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_geo5.png'
            ),
            Array(
            'title' => 'histoire1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_histoire1.png'
            ),
            Array(
            'title' => 'histoire2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_histoire2.png'
            ),
            Array(
            'title' => 'histoire3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_histoire3.png'
            ),
            Array(
            'title' => 'histoire4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_histoire4.png'
            ),
            Array(
            'title' => 'histoire5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_histoire5.png'
            ),
            Array(
            'title' => 'lettres1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_lettres1.png'
            ),
            Array(
            'title' => 'lettres2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_lettres2.png'
            ),
            Array(
            'title' => 'lettres3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_lettres3.png'
            ),
            Array(
            'title' => 'lettres4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_lettres4.png'
            ),
            Array(
            'title' => 'lettres5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_lettres5.png'
            ),
            Array(
            'title' => 'maths1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_maths1.png'
            ),
            Array(
            'title' => 'maths2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_maths2.png'
            ),
            Array(
            'title' => 'maths3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_maths3.png'
            ),
            Array(
            'title' => 'maths4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_maths4.png'
            ),
            Array(
            'title' => 'maths5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges_disciplines_maths5.png'
            ),
            Array(
            'title' => 'Cuisine 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/cuisine1.png'
            ),
            Array(
            'title' => 'Cuisine 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/cuisine2.png'
            ),
            Array(
            'title' => 'Cuisine 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/cuisine3.png'
            ),
            Array(
            'title' => 'Cuisine 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/cuisine4.png'
            ),
            Array(
            'title' => 'Cuisine 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/cuisine5.png'
            ),
            Array(
            'title' => 'Louise 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges-Louise1.png'
            ),
            Array(
            'title' => 'Louise 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges-Louise2.png'
            ),
            Array(
            'title' => 'Louise 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges-Louise3.png'
            ),
            Array(
            'title' => 'Louise 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badges-Louise4.png'
            ),
            Array(
            'title' => 'Coupe 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge3.png'
            ),
            Array(
            'title' => 'Coupe 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe2.png'
            ),
            Array(
            'title' => 'Coupe 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe3.png'
            ),
            Array(
            'title' => 'Coupe 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe4.png'
            ),
            Array(
            'title' => 'Coupe 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe5.png'
            ),
            Array(
            'title' => 'Coupe 6',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe6.png'
            ),
            Array(
            'title' => 'Coupe 7',
            'src'   => '/local/teacherboard/lib/template/images/badges/coupe7.png'
            ),
            Array(
            'title' => 'Fusée 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee1.png'
            ),
            Array(
            'title' => 'Fusée 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee2.png'
            ),
            Array(
            'title' => 'Fusée 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee3.png'
            ),
            Array(
            'title' => 'Fusée 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee4.png'
            ),
            Array(
            'title' => 'Fusée 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee5.png'
            ),
            Array(
            'title' => 'Fusée 6',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee6.png'
            ),
            Array(
            'title' => 'Fusée 7',
            'src'   => '/local/teacherboard/lib/template/images/badges/fusee7.png'
            ),
            Array(
            'title' => 'Monstre 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre1.png'
            ),
            Array(
            'title' => 'Monstre 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre2.png'
            ),
            Array(
            'title' => 'Monstre 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre3.png'
            ),
            Array(
            'title' => 'Monstre 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre4.png'
            ),
            Array(
            'title' => 'Monstre 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre5.png'
            ),
            Array(
            'title' => 'Monstre 6',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre6.png'
            ),
            Array(
            'title' => 'Monstre 7',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre7.png'
            ),
            Array(
            'title' => 'Monstre 8',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre8.png'
            ),
            Array(
            'title' => 'Monstre 9',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre9.png'
            ),
            Array(
            'title' => 'Monstre 10',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre10.png'
            ),
            Array(
            'title' => 'Monstre 11',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre11.png'
            ),
            Array(
            'title' => 'Monstre 12',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre12.png'
            ),
            Array(
            'title' => 'Monstre 13',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre13.png'
            ),
            Array(
            'title' => 'Monstre 14',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre14.png'
            ),
            Array(
            'title' => 'Monstre 15',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre15.png'
            ),
            Array(
            'title' => 'Monstre 16',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre16.png'
            ),
            Array(
            'title' => 'Monstre 17',
            'src'   => '/local/teacherboard/lib/template/images/badges/Monstre17.png'
            ),
            Array(
            'title' => 'Super héros 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-a.png'
            ),
            Array(
            'title' => 'Super héros 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-b.png'
            ),
            Array(
            'title' => 'Super héros 3',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-c.png'
            ),
            Array(
            'title' => 'Super héros 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-d.png'
            ),
            Array(
            'title' => 'Super héros 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-e.png'
            ),
            Array(
            'title' => 'Super héros 6',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-f.png'
            ),
            Array(
            'title' => 'Super héros 7',
            'src'   => '/local/teacherboard/lib/template/images/badges/superheros-g.png'
            ),
            Array(
            'title' => 'badge 1',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge1.png'
            ),
            Array(
            'title' => 'badge 2',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge2.png'
            ),
            Array(
            'title' => 'badge 4',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge4.png'
            ),
            Array(
            'title' => 'badge 5',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge5.png'
            ),
            Array(
            'title' => 'badge 6',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge6.png'
            ),
            Array(
            'title' => 'badge 7',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge7.png'
            ),
            Array(
            'title' => 'badge 8',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge8.png'
            ),
            Array(
            'title' => 'badge 9',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge9.png'
            ),
            Array(
            'title' => 'badge 10',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge10.png'
            ),
            Array(
            'title' => 'badge 11',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge11.png'
            ),
            Array(
            'title' => 'badge 12',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge12.png'
            ),
            Array(
            'title' => 'badge 13',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge13.png'
            ),
            Array(
            'title' => 'badge 14',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge14.png'
            ),
            Array(
            'title' => 'badge 15',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge15.png'
            ),
            Array(
            'title' => 'badge 16',
            'src'   => '/local/teacherboard/lib/template/images/badges/badge16.png'
            ),
        );
    }

    /*
     *
     */
    public function launch(Request $request, Response $response)
    {
        global $CFG, $USER;

        require_login();

        $courseid = $request->getParam('id');
        $this->coursecontext = context_course::instance($courseid);

        if (has_capability(PERMISSION_MANAGE_BADGES, $this->coursecontext)) {
            $currentuser = new teacherboard\User();
            $message = "";
            $coursename = "";
            $nb_badges = 0;
            if ($courseid) {
                $coursename = $currentuser->getCourseName($courseid);
                $badges = badges_get_badges(BADGE_TYPE_COURSE, $courseid, '', '' , '', '');
                $nb_badges = count($badges);
                foreach ($badges as &$badge) {
                    $badge->image = moodle_url::make_pluginfile_url(
                        $this->coursecontext->id,
                        'badges',
                        'badgeimage',
                        $badge->id,
                        '/',
                        "f1",
                        false
                    );
                }
            }

            $response->addVar('coursemodules', $currentuser->getCourseModules($courseid));
            $response->addVar('coursename', $coursename);
            $response->addVar('courseid', $courseid);
            $response->addVar('badges_images', $this->badgesImages());
            $response->addVar('badges', array_values($badges));
            $response->addVar('nb_badges', $nb_badges);
            $response->addVar('message', $message);
            $response->addVar('sesskey', $USER->sesskey);
            $response->addVar('mainBoard',
                $CFG->wwwroot . "/local/teacherboard/index.php");
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/badgesSuccess.php");
            $this->printOut();
        }
        else {
            $this->render($CFG->dirroot . "/local/teacherboard/lib/template/forbiddenSuccess.php");
            $this->printOut();
        }
    }
}

<?php
    global $OUTPUT, $CFG;
    //if ($message) echo $OUTPUT->error_text($message);
    echo $OUTPUT->container_start('tbachievement', 'idtbcontainer');
?>
<h3 class='tbtitle'>
    <a href='<?php echo $mainBoard; ?>'><img style='width:30px;margin-right:10px;' src='<?php echo RETURNMENU; ?>' alt='' title=''/></a>
        <span><?php echo get_string('managegroups', 'local_teacherboard'); ?></span>
</h3>

<?php
    echo $OUTPUT->container_start('tbmenu', 'idtbmenu');
?>


<ul class="tbmenu_ul">
    <?php if ($currentuser->hasRNE()): ?>
    <li>
        <a id="addgroup" href="<?php echo $CFG->wwwroot; ?>/local/teacherboard/index.php?action=addgroups">
            <?php echo get_string('group:add', 'local_teacherboard'); ?>
        </a>
    </li>
  <?php endif; ?>
</ul>

<?php
    echo $OUTPUT->container_end(); ?>

<?php if ($message): ?>
<div style="text-align:center;margin: 10px; background-color: white;">
<?php echo $message; ?>
</div>
<?php endif; ?>

<?php
    echo $OUTPUT->container_end();
?>

<div id="popup_delete" style="display:none;">
    <p><?php echo "êtes-vous sûr de vouloir supprimer ce groupe ?"; ?></p>
</div>

<script>
$( document ).ready(function() {

    // =================================
    //
    //  used to store global params
    //
    // =================================
    var RegistryObject = function() {
        this.enrolCourseId = null;
    }

    var registry = new RegistryObject();

    // =================================
    //
    //  used to activate debug mode
    //
    // =================================
    var DebugObject = function() {
        this.active = true;
    }
    DebugObject.prototype.log = function(msg) {
        if (this.active) console.log(msg);
    }

    var debug = new DebugObject();

    // =================================
    //
    //  deleting group
    //
    // =================================

    var deleteGroup = function() {
        var groupid = $(this).data('id')
        var grouptitle = $(this).parent().children('h3').html()
        var groupBlock = $(this).parent()
        var sesskey = '<?php echo $sesskey; ?>';
        $('#popup_delete').dialog({
            closeOnEscape: true,
            width: '100%',
            title: grouptitle,
            open: function(){
                var originalTop = $(this).parent().css('top')
                $(this).parent().css({top: '0px'})
                $(this).parent().animate({
                  'top': originalTop,
                },{
                  duration : 400,
                  easing : "linear",
                  queue : false
                })
            },
            show: "fade",
            buttons: {
                '<?php echo get_string('msgConfirmDeletion', 'local_teacherboard'); ?>': function() {
                    $(this).dialog('close')
                    $.ajax({
                        url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletegroup',
                        type: 'POST',
                        data: 'groupid=' + groupid + '&sesskey=' + sesskey,
                        success: function(_data) {
                            console.log(_data)
                            if (_data.state == "success") {
                                groupBlock.remove()
                            }
                            else {
                                console.log(_data.message)
                            }
                        }
                    })
                }
            },
            modal:true
        })
        $('.ui-dialog :button').blur()

    }

    // =================================
    //
    //  Bind events
    //
    // =================================
    var bindEvents = function() {
        $(".delete_group").on('click', deleteGroup);
    }

    bindEvents();

});
</script>


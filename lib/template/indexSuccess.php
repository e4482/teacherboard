<?php if ($message): ?>
<div style="background-image : url(/local/teacherboard/lib/template/images/notification.png); background-repeat: no-repeat; background-color: white;border-radius: 3px;background-position: 8px 12px; background-size: 50px; width:60%;padding:16px;padding-left:72px;margin: 10px auto;box-shadow:0px 3px 3px 0px #999;">
    <h4>Notification</h4>
    <p style="color: #999;"><?php echo $message; ?></p>
</div>
<?php endif; ?>

<!-- Message -->
<?php
if ($currentuser->hasRne() && $message = trim(get_config('local_teacherboard', 'message'))):
$backgroundcolor = trim(get_config('local_teacherboard', 'messagebackgroundcolor'));
if(!$backgroundcolor){
    $backgroundcolor = '#47beda';
}
$textcolor = trim(get_config('local_teacherboard', 'messagetextcolor'));
if(!$textcolor) {
    $textcolor = '#ffffff';
}
?>
<div class="well well-sm" style="background-color: <?php echo $backgroundcolor ?>;">
    <h4 style="color: <?php echo $textcolor ?>;">
        <?php echo $message ?>
    </h4>
</div>
<?php endif; ?>

<!-- Avertissement si c'est un prof d'un bac à sable -->
<?php if ($currentuser->hasRne() && substr($currentuser->getFirstRne(),0,3) == "000"): ?>
<div class="well well-sm" style="background-color: #47beda;">
    <h4 style="color: #fff;">
        Attention : le compte que vous utilisez a été créé pour une formation, et sera supprimé fin août.<br/>
        Si vous créez des parcours que vous souhaitez conserver, veillez à les exporter.
    </h4>
</div>
<?php endif; ?>

<!-- Avertissement si c'est un compte de l'année dernière -->
<?php if ($currentuser->isOldUser()): ?>
    <div class="well well-sm" style="background-color: #ff4136;">
        <h4 style="color: #fff;">
            Attention : le compte que vous utilisez sera supprimé début décembre.<br/>
            Contactez le référent e-éducation/Éléa de votre établissement.
        </h4>
    </div>
<?php endif; ?>

<?php
    global $OUTPUT, $CFG;
    //if ($message) echo $OUTPUT->error_text($message);
    echo $OUTPUT->container_start('tbcontainer', 'idtbcontainer');
    echo $OUTPUT->heading(get_string('tbtitle', 'local_teacherboard'), 3, 'tbtitle', 'teacherboard_title');
?>

<?php
    echo $OUTPUT->container_start('tbmenu', 'idtbmenu');
?>

<nav class="navbar navbar-default">
    <ul class="nav navbar-nav tbmenu_ul">
        <li><a id="addfolder" href="#"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true">&nbsp;</span><?php echo get_string('folder:add', 'local_teacherboard'); ?></a></li>

        <?php if ($currentuser->hasRNE()): ?>
        <li><a id="addcourse" href="#"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true">&nbsp;</span><?php echo get_string('addcourseicon', 'local_teacherboard'); ?></a></li>
        <li><a href="<?php echo new moodle_url('/local/knowledgegate/index.php?way=search.page.bytags') ?>">
                <span class="glyphicon glyphicon-book" aria-hidden="true">&nbsp;</span>
                <?php echo get_string('repositoryaccess', 'local_teacherboard'); ?>
        </a></li>
        <!-- <li id="managegroups"><a  href="<?php echo $CFG->wwwroot; ?>/local/teacherboard/index.php?action=managegroups"><?php echo get_string('managegroups', 'local_teacherboard'); ?></a></li> -->
        <?php endif; ?>
    </ul>
    <?php if ($currentuser->hasRNE()): ?>
    <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
            </a>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="<?php echo new moodle_url('/local/faq/index.php?role=prof') ?>" target="_blank">
                <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder aux tutoriels
            </a>
        </li>
    </ul>
    <?php endif; ?>
</nav>

<?php
    echo $OUTPUT->container_end(); ?>

<div class="nestable-lists">

    <div class="spinner" style="display:none;">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>

    <div class="dd" id="nestable">
        <ol class="dd-list">
            <?php echo $usertree; ?>
        </ol>
    </div>
</div>

<?php

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/template/popup.php");



$popupdelete = new Popup(
        'popup_delete',
        'Suppression d\'un parcours',
        '<div class="modal-body course-modal-body"><div style="font-size: large; margin-top: 15px; margin-bottom: 15px">' . get_string('warningDeleteCourse', 'local_teacherboard') . '</div>
<button id="popup_delete_confirmation" type="button" class="btn btn-secondary newcourse-modal-valbtn" data-dismiss="modal">'
. get_string('msgConfirmDeletion', 'local_teacherboard') . '</button></div>');
$popupunenrol = new Popup(
        'popup_unenrol',
        'Désincription à un parcours',
        '<div class="modal-body course-modal-body"><div style="font-size: large; margin-top: 15px; margin-bottom: 15px;">' . get_string('warningUnEnrolCourse', 'local_teacherboard') . '</div>
<button id="popup_unenrol_confirmation" type="button" class="btn btn-secondary newcourse-modal-valbtn">'
. get_string('msgConfirmDeletion', 'local_teacherboard') . '</button></div>');
$popupdeleteforbidden = new Popup(
    'popup_deletefolder_forbidden',
    get_string('warningDeleteFolder', 'local_teacherboard'),
    '');
$popupenrolmeta = new Popup(
    'popup_enrolmeta',
    get_string('metacourse:title', 'local_teacherboard'),
    '<div id="tab-metacourses-block" class="modal-body course-modal-body">
                <div id="metacourse-filter-block" style="margin-top: 0.5em;">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    Rechercher un parcours : <input type="text" id="metacoursefilter"/><br/>
                    <span id="metacourse-filter-block-fail" style="display: none">Aucun parcours ne correspond à votre recherche.</span>
                </div>
                <div id="tab-enrolmeta"></div>
             </div>');
$popupcohort = new Popup(
    'popup_cohorts',
    get_string('cohorts:title', 'local_teacherboard'),
    '<div id="tab-cohorts" class="modal-body course-modal-body">' . $cohortsListHtml . '</div>');
$popupteachers = new Popup(
    'popup_teachers',
    get_string('teachers:title', 'local_teacherboard'),
    '<div id="tab-teachers-block" class="modal-body course-modal-body">
                <div id="teacher-filter-block" style="margin-top: 0.5em;">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    Rechercher un enseignant : <input type="text" id="teacherfilter"/><br/>
                    <span id="teacher-filter-block-fail" style="display: none">Aucun enseignant ne correspond à votre recherche.</span>
                </div>
                <div id="tab-teachers" class="modal-body course-modal-body">nothing here...</div>
            </div>');
$popupaddcourse = new Popup(
    'popup_addcourse',
    'Création d\'un nouveau parcours',
    '<!-- Modal principal -->
            <div id="newcourse-modal-step1" class="modal-body course-modal-body">
                <div class="row course-modal-row">
                    <div class="row course-modal-title"><h3 class="course-modal-title">Activités uniques</h3></div>
                    <div class="col-sm-4">
                        <div id="new-course-qcm"  class="course-modal-activityblock">
                            <div class="row"><img src="/mod/simpleqcm/pix/iconLarge.png" class="img-responsive course-modal-simpleactivityimage"></div>
                            <div class="row"><h4>QCM</h4></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div id="new-course-millionnaire" class="course-modal-activityblock">
                            <div class="row"><img src="/mod/millionnaire/pix/iconLarge.png" class="img-responsive course-modal-simpleactivityimage"></div>
                            <div class="row"><h4>Millionnaire</h4></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div id="new-course-matching" class="course-modal-activityblock">
                            <div class="row"><img src="/mod/epikmatching/pix/iconLarge.png" class="img-responsive course-modal-simpleactivityimage"></div>
                            <div class="row"><h4>Appariement</h4></div>
                        </div>
                    </div>
                </div>

                <div class="row course-modal-row">
                    <div class="row course-modal-title"><h3 class="course-modal-title">Gabarits</h3></div>
                    <div class="row course-modal-completecourse" id="new-course-express">
                        <div class="col-sm-6 course-modal-description" >
                            <h4>Parcours express</h4>
                        </div>
                        <div class="col-sm-6 course-modal-description">
                            Gabarit sans section pour une intégration rapide.
                        </div>
                    </div>
                    <div class="row course-modal-completecourse" id="new-course-inverse">
                        <div class="col-sm-6 course-modal-description">
                            <h4>Parcours pour <br/>inverser les temps</h4>
                        </div>
                        <div class="col-sm-6 course-modal-description">
                            Gabarit en deux sections dont l\'une est dédiée au travail à distance.
                        </div>
                    </div>
                    <div class="row course-modal-completecourse" id="new-course-map">
                        <div class="col-sm-6 course-modal-description" >
                            <h4>Parcours avec <br/>navigation visible</h4>
                        </div>
                        <div class="col-sm-6 course-modal-description">
                            Gabarit avec une carte de progression pour aider les élèves à se repérer dans le parcours.
                        </div>
                    </div>
                    <div class="row course-modal-completecourse" id="new-course-gamified">
                        <div class="col-sm-6 course-modal-description">
                            <h4>Parcours gamifié</h4>
                        </div>
                        <div class="col-sm-6 course-modal-description">
                            Gabarit utilisant les leviers du jeu pour favoriser l\'engagement des élèves.
                        </div>
                    </div>
                    <div class="row course-modal-completecourse" id="new-course-empty">
                        <div class="col-sm-6 course-modal-description">
                            <h4>Parcours vide</h4>
                        </div>
                        <div class="col-sm-6 course-modal-description">
                            Gabarit vide laissant libre cours à votre créativité !
                        </div>
                    </div>
                </div>
            </div>

            <!-- 2e partie du modal : choix du nom, du dossier -->
            <div id="newcourse-modal-step2" class="modal-body course-modal-body" style="display: none">

                <div class="form-group">
                    <label for="courseNameInput"><h3 class="course-modal-title">Entrez le nom du parcours</h3></label>
                    <input type="text" class="form-control" id="courseNameInput" placeholder="Nouveau Parcours">
                </div>

                <div class="form-group">
                    <label for="folderNameInput"><h3 class="course-modal-title">Dossier</h3></label>
                    <select class="form-control" id="folderNameInput">
                    </select>
                </div>

                <button id="new-course-validation" type="button" class="btn btn-secondary newcourse-modal-valbtn">Valider</button>
            </div>'
);

    echo $popupdelete->display();
    echo $popupenrolmeta->display();
    echo $popupcohort->display();
    echo $popupunenrol->display();
    echo $popupdeleteforbidden->display();
    echo $popupteachers->display();
    echo $popupaddcourse->display();

    echo $OUTPUT->container_end();
?>

<script>
$( document ).ready(function() {

    $('#accordion').accordion({
        heightStyle: 'content'
    });
    // =================================
    //
    //  used to store global params
    //
    // =================================
    var RegistryObject = function() {
        this.enrolCourseId = null;
    }

    var registry = new RegistryObject();

    // =================================
    //
    //  used to activate debug mode
    //
    // =================================
    var DebugObject = function() {
        this.active = true;
    }
    DebugObject.prototype.log = function(msg) {
        if (this.active) console.log(msg);
    }

    var debug = new DebugObject();


    // =================================
    //
    //  convert teachers list into jquery ui tabs
    //
    // =================================

    //$("#tab-teachers").tabs();

    var enrolTeachers = function(ev) {
        ev.preventDefault();
        var _courseid = $(this).data('courseid');
        registry.enrolCourseId = _courseid;
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxgetteachers',
            type: 'POST',
            data: 'courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }

                $("#tab-teachers").html(_data.teachers);
                $("#teacherfilter").val('');
                $("#teacher-filter-block-fail").hide();
                $("#popup_teachers").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                });
            }
        });
    };

    /*
     *
     *
     */
    var toggleEnrolTeacher = function() {
        var _teacherid = $(this).data('id');
        var _courseid = registry.enrolCourseId;
        var object_clicked = $(this);
        var object_childs = object_clicked.children()
        if (_teacherid != <?php echo $userid; ?> && !object_childs.hasClass("enrolling")) {
            if (object_childs.hasClass("enrolled")) {
                object_childs.removeClass("enrolled")
                toggleEnrolClass = function () {$(this).removeClass("enrolling").addClass("notenrolled")}
            } else {
                object_childs.removeClass("notenrolled")
                toggleEnrolClass = function () {$(this).removeClass("enrolling").addClass("enrolled")}
            }

            // warn that (un)enrolling is processing
            object_childs.addClass("enrolling")

            $.ajax({
                url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxtoggleenrolteacher',
                type: 'POST',
                data: 'teacherid=' + _teacherid + '&courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
                success: function(_data) {
                    if (!_data.isloggedin) {
                        document.location.href = _data.logurl
                        return
                    }
                    object_childs.each(toggleEnrolClass);
                }
            }).catch(function () {
                // Closing the popup, forcing to reinterrogate database to know the truth
                $("#popup_teachers").modal('hide')
            });
        }
        else {
            // not allowed to unenroll himself
        }

    }

    var setEnrolClass = function(object) {
        object.removeClass("enrolled").addClass("notenrolled")
    }


    // =================================
    //
    //  convert cohorts list into jquery ui tabs
    //
    // =================================

    $("#tab-cohorts").tabs();

    var enrolCohorts = function(ev) {
        ev.preventDefault();
        var _courseid = $(this).data('courseid');
        registry.enrolCourseId = _courseid;
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxgetcohorts',
            type: 'POST',
            data: 'courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }

                $("#cohortfilter").val('');
                $("#cohort-filter-block-fail").hide();
                var _li_cohorts = [];
                var cohorts = _data.cohorts
                for(var index in cohorts) {
                    _li_cohorts.push(cohorts[index].customint1);
                }
                $("#tab-cohorts-block").find("li").each(function(){
                    $(this).show();
                    if (_li_cohorts.indexOf($(this).data('id').toString()) != -1) {
                        $(this).removeClass("cohort_not_selected").addClass("cohort_selected");
                    }
                    else {
                        $(this).removeClass("cohort_selected").addClass("cohort_not_selected");
                    }
                });

                $("#popup_cohorts").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                });
            }
        });
    };
    /*
     *
     *
     */
    var filterCohorts = function(filterObject, tabname) {
        var filter = $(filterObject).val().toLowerCase();
        var blockObject = $("#tab-" + tabname + "s-block")
        blockObject.find("li").each(function() {
            $(this).show()
        })
        if(filter){
            $("#" + tabname + "-filter-block-fail").show()
            blockObject.find("li").each(function() {
                var div = $(this).find('div')
                // no div when this is a title
                if (div.length > 0) {
                    var name = $(this).find('div').html().toLowerCase()
                    if(name.indexOf(filter) === -1) $(this).hide()
                    else $("#" + tabname + "-filter-block-fail").hide()
                }
            })
        } else {
            $("#" + tabname + "-filter-block-fail").hide()
        }
    }


    /*
     *
     *
     */
    var toggleEnrolCohort = function() {
        var _cohortid = $(this).data('id');
        var _courseid = registry.enrolCourseId;
        var object_clicked = $(this);

        if (!object_clicked.hasClass("cohort_inprogress")) {
            if (object_clicked.hasClass("cohort_selected")) {
                object_clicked.removeClass("cohort_selected")
                toggleEnrolClass = function (object_clicked) {
                    object_clicked.removeClass("cohort_inprogress").addClass("cohort_not_selected")
                }
            } else {
                object_clicked.removeClass("cohort_not_selected")
                toggleEnrolClass = function () {
                    object_clicked.removeClass("cohort_inprogress").addClass("cohort_selected");
                }
            }

            // warn that (un)enrolling is processing
            object_clicked.addClass("cohort_inprogress")

            $.ajax({
                url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxtoggleenrolcohort',
                type: 'POST',
                data: 'cohortid=' + _cohortid + '&courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
                success: function() {toggleEnrolClass(object_clicked)}
            }).catch(function (_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }
                // Closing the popup, forcing to reinterrogate database to know the truth
                $('#popup_cohorts').modal('hide')
            });
        }
    }

    // =================================
    //
    //  Meta course enrolment
    //
    // =================================

    var enrolCourses = function(ev) {
        ev.preventDefault();
        var _courseid = $(this).data('courseid');
        registry.enrolCourseId = _courseid;
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxgetcourses',
            type: 'POST',
            data: 'courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }

                $("#tab-enrolmeta").html(_data.html)
                $("#metacoursefilter").val('');
                $("#metacourse-filter-block-fail").hide();
                $("#popup_enrolmeta").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                });
            }
        });
    }

    /*
 *
 *
 */
    var toggleEnrolMetacourse = function() {
        var _metacourse = $(this).data('id');
        var _coursetoenrol = registry.enrolCourseId;
        var object_clicked = $(this);
        var _toenrol = object_clicked.hasClass("course_not_selected");

        if (!object_clicked.hasClass("course_inprogress")) {
        // Set it as in progress until ajax request returns
        object_clicked
            .removeClass("course_selected")
            .removeClass("course_not_selected")
            .addClass("course_inprogress")

            $.ajax({
                url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxtoggleenrolmetacourse',
                type: 'POST',
                data: 'coursetoenrol=' + _coursetoenrol
                    + '&metacourse=' + _metacourse
                    + '&toenrol=' + _toenrol
                    + '&sesskey=<?php echo $sesskey; ?>',
                success: function(_data) {
                    if (!_data.isloggedin) {
                        document.location.href = _data.logurl
                        return
                    }

                    if (_data.metaenrol) {
                        object_clicked.removeClass("course_inprogress").addClass("course_selected")
                    } else {
                        object_clicked.removeClass("course_inprogress").addClass("course_not_selected")
                    }
                }
            }).catch(function () {
                // Closing the popup, forcing to reinterrogate database to know the truth
                $("#popup_teachers").modal('hide')
            });
        }
        else {
            // enrolling : no interaction allowed
        }

    }

    // =================================
    //
    //  function called each time
    //  we want to add a NEW FOLDER
    //
    // =================================

    var addFolder = function(ev) {
        return $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxaddfolder',
            type: 'POST',
            data: 'sesskey=<?php echo $sesskey; ?>',
            success: function(data) {
                if (!data.isloggedin) {
                    document.location.href = data.logurl
                    return
                }
                $("#nestable").children().append(data['html']);
                // bind event on new <table>
                $(".modules_table").last().nestable({
                    listNodeName    : 'tbody',
                    itemNodeName    : 'tr',
                    rootClass       : 'dd',
                    listClass       : 'dd-list-table',
                    handleClass     : 'dd-handle2',
                    maxDepth        : 1,
                    group           : 1
                }).on('change', updateOutput);
            }
        });
    };



    _dragitem = new Array();

    // =================================
    //
    //  function called each time
    //  we rename the current FOLDER
    //
    // =================================

    var renameFolder = function(newfolder) {
        var sesskey = "<?php echo $sesskey; ?>";
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxrenamefolder',
            type: "POST",
            data: "folderid=" + newfolder.id + "&foldername=" + newfolder.name + "&sesskey=" + sesskey,
            success: function(data) {
                if (!data.isloggedin) {
                    document.location.href = data.logurl
                    return
                }
                $('.input_course[name="' + newfolder.id + '"]').attr('value', newfolder.name)
            }
        });
    }


    // =================================
    //
    //  toggle button to edit the name
    //  of the current FOLDER
    //
    // =================================

    var toggleEditButton = function(ev) {
        if ($(this).attr('src') == '<?php echo EDIT; ?>') {
            $(this).attr('src','<?php echo EDIT_OK; ?>');
            $(this).parent().children('.input_course').css({'background-color': '#FFFFFF','cursor':'text'});
        }
        else {
            $(this).attr('src','<?php echo EDIT; ?>');
            $(this).parent().children('.input_course').css({'background-color': 'transparent','cursor':'pointer'});
            var newfolder = {
                name: $(this).parent().children('.input_course').val(),
                id: $(this).parent().children('.input_course').attr('name')
            };
            renameFolder(newfolder);
        }
    };

    // =================================
    //
    //  record name of the PAGE if we press enter
    //
    // =================================

    var folderTitleKeypress = function(ev) {
        if(ev.keyCode == 13) {
            $(this).css({'background-color':'transparent','cursor':'pointer'});
            $(this).parent().children('.edit_title').attr('src','<?php echo EDIT; ?>');
            var newfolder = {
                name: $(this).val(),
                id: $(this).attr('name')
            };
            renameFolder(newfolder);
        }
    };


    // =================================
    //
    //  Prevent input nominal behavior to show/hide activities instead
    //
    // =================================
    var preventTitleModification = function(ev) {
        if ($(this).parent().children('.edit_title').attr('src') == '<?php echo EDIT; ?>') {
            ev.preventDefault();
            $(this).parent().children('.showactivities').click();
        }
    };

    // =================================
    //
    //  Delete a FOLDER if it is empty
    //
    // =================================

    var deleteFolder = function(ev) {
        ev.preventDefault();
        var _folderid = $(this).data('folderid');
        var _li = $(this).parent().parent();
        debug.log("delete folder");
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletefolder',
            type: "POST",
            data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>"
        }).done(function(data){
            if (!data.isloggedin) {
                document.location.href = data.logurl
                return
            }

            if (data.state != 'done') {
                $("#popup_deletefolder_forbidden").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                });
            } else {
                _li.fadeOut('slow', function(){$(this).remove();});

            }
        });
    };


    // =================================
    //
    //  Add COURSE in the current FOLDER
    //
    // =================================

    var addCourseCallback = function(object_clicked) {

        $("#newcourse-modal-step2").hide();
        $("#newcourse-modal-step1").show();
        $("#popup_addcourse_title").html("Création d'un nouveau parcours");

        // Save selected folder id
        var _folderid = object_clicked.data('folderid');
        $("#popup_addcourse").data('folderid', _folderid);

        // Get folder name list and transmit it to modal
        $("#folderNameInput").empty();
        $(".input_course").each(function() {
            var selectedtxt = "";
            if(_folderid == $(this).attr("name")) {
                selectedtxt = " selected=\"selected\"";
            }

            $("#folderNameInput").append("<option value=\"" + $(this).attr("name") +  "\"" + selectedtxt + ">" + $(this).attr("value") + "</option>");
        })

        $("#popup_addcourse").modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });
    }

    var chooseCourse = function(ev) {
        var course_type = ev.data.course_type;
        $("#popup_addcourse").data('course_type', course_type);
        $("#newcourse-modal-step1").hide();
        $("#newcourse-modal-step2").show();
        var title = "";
        switch (course_type) {
            case 'qcm':
                title += "Activité QCM";
                break;
            case 'epikmatching':
                title += "Activité Appariement";
                break;
            case 'millionnaire':
                title += "Activité Millionnaire";
                break;
            case 'parcoursExpress':
                title += "Parcours express";
                break;
            case 'parcoursAvecCarte':
                title += "Parcours avec navigation visible";
                break;
            case 'parcoursGamifie':
                title += "Parcours gamifié";
                break;
            case 'parcoursClasseInverse':
                title += "Parcours pour inverser les temps";
                break;
            case 'parcoursVide':
                title += "Parcours vide";
                break;
            default:
                console.error('Course name not managed: ' + course_type);
        }

        $("#popup_addcourse_title").html(title);
    };

    var createCourse = function(ev) {
        // Deactivate validation button
        $('#new-course-validation').off('click')
        $('#popup_addcourse').modal('hide')

        var course_name = $('#courseNameInput').val();
        if(course_name == "") {
            course_name = "Nouveau Parcours";
        }

        var _folderid = $("#folderNameInput").val();
        var course_type = $("#popup_addcourse").data('course_type');

        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxaddcourse',
            type: "POST",
            data: "folderid=" + _folderid +
                "&sesskey=<?php echo $sesskey; ?>" +
                "&course_name=" + btoa(unescape(encodeURIComponent(course_name))) +
                "&course_type=" + course_type
        }).done(function (_data) {
            if (!_data.isloggedin) {
                document.location.href = _data.logurl
                return
            }

            if (_data['courseid']) {
                window.location.replace("<?php echo $CFG->wwwroot; ?>/course/view.php?id=" + _data['courseid'] + "&notifyeditingon=1");
            }
        });
    }

    var addCourse = function(ev) {
        ev.preventDefault();
        var _table = $(this).parent().children(".dd");
        // tbody not visible
        if (_table.hasClass("hideactivities")) {
            showCourses(ev, addCourseCallback, $(this));
        }
        // tbody already visible
        else {
            addCourseCallback($(this));
        }
    };

    // ================================
    //
    //	function called when we want
    //  to add new course from top button
    //
    // ================================
    var addCourseFromRoot = function(ev) {
        ev.preventDefault();
        var firstFolder = $("#nestable").children(".dd-list").children(".dd-item").first();
        debug.log("addCourseFromRoot");
        if (firstFolder.length == 0) {
            debug.log("add new folder");
            addFolder().then(function(){
                firstFolder = $("#nestable").children(".dd-list").children(".dd-item").first();
                debug.log(firstFolder);
                var _table = firstFolder.children(".dd");
                // tbody not visible
                if (_table.hasClass("hideactivities")) {
                    showCourses(ev, addCourseCallback, firstFolder.children(".addcoursefolder"));
                }
                // tbody already visible
                else {
                    addCourseCallback(firstFolder.children(".addcoursefolder"));
                }

            });
        }
        else {
            var _table = firstFolder.children(".dd");
            // tbody not visible
            if (_table.hasClass("hideactivities")) {
                showCourses(ev, addCourseCallback, firstFolder.children(".addcoursefolder"));
            }
            // tbody already visible
            else {
                debug.log("addCourseFromRoot addCourseCallback");
                addCourseCallback(firstFolder.children(".addcoursefolder"));
            }
        }
    };

    // =================================
    //
    //  function called at mousedown
    //  on a PAGE
    //
    // =================================
    var dragitem = function(e)
    {
        _dragitem.push($(this).parent().children(".input_course").attr('name'));
    };

    // =================================
    //
    //  function called each time
    //  we mousedown on an item
    //
    // =================================

    var drag_item = function(object_clicked) {

        _dragitem.push(object_clicked.parent().data("id"));

    };

    // =================================
    //
    //  function called each time
    //  a MODULE or a PAGE is dropped down
    //
    // =================================
    var updateOutput = function(e)
    {

        if (e.target.id == this.id){

            if ((this.id == 'nestable') && (_dragitem[0] != undefined)) {

                _parent = $("#<?php echo $folderid_prefix; ?>"+_dragitem[0]).parent().parent().children(".input_course").attr('name');
                _previous = $("#<?php echo $folderid_prefix; ?>"+_dragitem[0]).prev().children(".input_course").attr('name');
                _current = _dragitem[0];
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxmovefolder',
                    type: "POST",
                    data: "parentid=" + _parent + "&previousid=" + _previous + "&currentid=" + _current + "&sesskey=<?php echo $sesskey; ?>",
                    success: function(data) {
                        if (!data.isloggedin) {
                            document.location.href = data.logurl
                            return
                        }

                        debug.log(data.html);
                    }
                });
            }
            else {
                if (_dragitem[0] != undefined) {
                    debug.log(_dragitem[0]);
                    var _tr = "#item_" + _dragitem[0];
                    var _tbody = $(_tr).parent();
                    var _table = $(_tbody).parent();
                    var _li = $(_table).parent();
                    var _currentItemId = _dragitem[0];
                    var _previousItemId = $(_tr).prev().data("id");
                    var _folderId = $(_li).children(".input_course").attr('name');
                    $.ajax({
                        url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxmovecourse',
                        type: "POST",
                        data: "previousitemId=" + _previousItemId + "&currentItemId=" + _currentItemId + "&folderid=" + _folderId + "&sesskey=<?php echo $sesskey; ?>",
                        success: function(data) {
                            if (!data.isloggedin) {
                                document.location.href = data.logurl
                                return
                            }

                            debug.log(data.html);
                        }
                    });
                }
            }
            _dragitem.length = 0;
        }
    };

    // =================================
    //
    //  show COURSES of specified FOLDER
    //
    // =================================

    var showCourses = function(ev, addCourseCallback, object_clicked ) {

        ev.preventDefault();
        addCourseCallback = addCourseCallback || null;
        object_clicked = object_clicked || $(this);
        var _folderid = object_clicked.data("folderid");
        var _container = object_clicked.parent().children(".dd");
        if (_container.hasClass("hideactivities")) {
            if (object_clicked.attr('src') == '<?php echo FOLDER; ?>') {
                object_clicked.attr('src','<?php echo FOLDER_OPENED; ?>');
            }
            else {
                object_clicked.parent().children(".showactivities").attr('src','<?php echo FOLDER_OPENED; ?>');
            }

            if (_container.children(".dd-list-table").length) {
                // Wrap table into a div to make slidedown effect working
                // the div wrapper is removed at the end of this effect
                _container.wrap("<div style='width:100%;' class='table-wrapper'/>");
                _container.parent().hide();
                _container.show();
                _container.parent().slideDown("slow", 'swing', function(){
                    _container.unwrap();
                    if (addCourseCallback != null) {
                        addCourseCallback(object_clicked);
                    }
                });

                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxfolderselected',
                    type: "POST",
                    data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>",
                    success: function(data) {
                        if (!data.isloggedin) {
                            document.location.href = data.logurl
                            return
                        }
                    }
                });
            }
            else {
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxshowcourses',
                    type: "POST",
                    data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>",
                    success: function(data) {
                        if (!data.isloggedin) {
                            document.location.href = data.logurl
                            return
                        }

                        _container.children(".dd-list-table").remove();
                        _container.append(data.arraycourses);
                        _container.after(data.addcoursebtn);
                        // Wrap table into a div to make slidedown effect working
                        // the div wrapper is removed at the end of this effect
                        _container.wrap("<div style='width:100%;' class='table-wrapper'/>");
                        _container.parent().hide();
                        _container.show();
                        _container.parent().slideDown("slow", 'swing', function(){
                            _container.unwrap();
                            if (addCourseCallback != null) {
                                addCourseCallback(object_clicked);
                            }
                        });
                    }
                });
            }
        }
        else {
            _container.fadeOut('slow');
            object_clicked.attr('src','<?php echo FOLDER; ?>');
        }
        _container.toggleClass("hideactivities");
    };


    // =================================
    //
    //  function called each time
    //  we want to hide or show a COURSE
    //
    // =================================
    showhideCourse = function() {
        var _itemid = $(this).data("itemid");
        var _sesskey = '<?php echo $sesskey; ?>';
        if ($(this).hasClass('hiddencourse')) {
            $(this).attr('src','<?php echo EYE_OPENED; ?>');
        }
        else {
            $(this).attr('src','<?php echo EYE_CLOSED; ?>');
        }
        $(this).toggleClass('hiddencourse');
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxshowhidecourse',
            type: 'POST',
            data: 'itemid=' + _itemid + '&sesskey=' + _sesskey,
            success: function(data) {
                if (!data.isloggedin) {
                    document.location.href = data.logurl
                    return
                }
            }
        });
    }

    // =================================
    //
    //  function called each time
    //  we want to delete a COURSE
    //
    // =================================
    deleteCourse = function(that,unenrol) {

        // get the <TR> tag
        var _tr = $(that).parent().parent();

        // get the <TBODY> tag
        var _tbody = _tr.parent();

        var _itemid = $(that).data("itemid");
        var _sesskey = '<?php echo $sesskey; ?>';

        popupid = unenrol ? '#popup_unenrol' : '#popup_delete';
        $(popupid).data('itemid', _itemid);
        $(popupid).data('sesskey', _sesskey);
        $(popupid).data('tr', _tr);
        $(popupid).data('tbody', _tbody);
        $(popupid).modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });
    }

    confirmDeleteCourse = function(unenrol) {

        popupid = unenrol ? '#popup_unenrol' : '#popup_delete';

        $(popupid).modal('hide');

        _itemid = $(popupid).data('itemid');
        _sesskey = $(popupid).data('sesskey');
        _tbody = $(popupid).data('tbody');
        _tr = $(popupid).data('tr');

        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletecourse',
            type: 'POST',
            data: 'itemid=' + _itemid + '&sesskey=' + _sesskey,
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }

                if (_data.state != 'done') return;

                _tr.fadeOut('slow').remove();
                if (_tbody.children('.dd-item').length == 0) {
                    _tbody.append("<tr class='dd-empty'><td class='dd-empty' colspan='<?php echo $arrayNbColumns; ?>'></td></tr>")
                }
            }
        });


    }

    enterKeyPressed = function(event) {
        event.preventDefault()
        if (event.keyCode === 13) {
            eval(event.data.instructions)
        }
    }


    // =================================
    //
    //  Bind events
    //
    // =================================
    var bindEvents = function() {

        $('.modules_table').on('mousedown', '.dd-handle2', function(e) {
            drag_item($(this));
        });
        $("#addfolder").on('click', addFolder);
        $("#addcourse").on('click', addCourseFromRoot);
        $(".enrolcohort").on('click', toggleEnrolCohort);
        $("#tab-enrolmeta").on('click', '.enrolmeta', toggleEnrolMetacourse)
        $("#cohortfilter").on('keyup', function () {filterCohorts(this, 'cohort')});
        $("#teacherfilter").on('keyup', function () {filterCohorts(this, 'teacher')});
        $("#metacoursefilter").on('keyup', function () {filterCohorts(this, 'metacourse')});
        $("#tab-teachers").on('click', '.enrolteacher_li', toggleEnrolTeacher);
        $('#nestable').on('mousedown', '.dd-handle', dragitem);
        $("#nestable").on('click', '.deletepage', deleteFolder);
        $("#nestable").on('click', '.enrolcourses', enrolCourses);
        $("#nestable").on('click', '.enrolcohorts', enrolCohorts);
        $("#nestable").on('click', '.enrolteacher', enrolTeachers);
        $("#nestable").on('mousedown', '.input_course', preventTitleModification);
        $("#nestable").on('keypress', '.input_course', folderTitleKeypress);
        $("#nestable").on('click', '.edit_title', toggleEditButton);
        $("#nestable").on('click', '.showactivities', showCourses);
        $("#nestable").on('click', '.showhidecourse', showhideCourse);
        $("#nestable").on('click', '.addcoursefolder', addCourse);
        $("#nestable").on('click', '.deleteitem', function(){deleteCourse(this,false)});
        $("#nestable").on('click', '.unenrolitem', function(){deleteCourse(this,true)});
        $('#nestable').nestable({
            maxDepth    : 3,
            expandBtnHTML   : '<button data-action="expand" type="button"><img style="width:60%;" src="<?php echo UP; ?>" alt="-" /></button>',
            collapseBtnHTML : '<button data-action="collapse" type="button"><img style="width:60%;" src="<?php echo DOWN; ?>" alt="-" /></button>',
        }).on('change', updateOutput);
        $(".modules_table").nestable({
            listNodeName    : 'tbody',
            itemNodeName    : 'tr',
            rootClass       : 'dd',
            listClass       : 'dd-list-table',
            handleClass     : 'dd-handle2',
            maxDepth        : 1,
            group           : 1
        }).on('change', updateOutput);

        $("#new-course-qcm").on('click', {course_type: 'qcm'}, chooseCourse);
        $("#new-course-millionnaire").on('click', {course_type: 'millionnaire'}, chooseCourse);
        $("#new-course-matching").on('click', {course_type: 'epikmatching'}, chooseCourse);
        $("#new-course-express").on('click', {course_type: 'parcoursExpress'}, chooseCourse);
        $("#new-course-inverse").on('click', {course_type: 'parcoursClasseInverse'}, chooseCourse);
        $("#new-course-map").on('click', {course_type: 'parcoursAvecCarte'}, chooseCourse);
        $("#new-course-gamified").on('click', {course_type: 'parcoursGamifie'}, chooseCourse);
        $("#new-course-empty").on('click', {course_type: 'parcoursVide'}, chooseCourse);
        $("#new-course-validation").on('click', {course_type: 'parcoursVide'}, createCourse);
        $("#popup_delete_confirmation").on('click', function() {confirmDeleteCourse(false)});
        $('#popup_delete').on('keydown', {instructions: 'confirmDeleteCourse(false)'}, enterKeyPressed)
        $("#popup_unenrol_confirmation").on('click', function() {confirmDeleteCourse(true)});
        $('#popup_unenrol').on('keydown', {instructions: 'confirmDeleteCourse(true)'}, enterKeyPressed)
    }

    bindEvents();
    <?php if ($selectedFolder != ""): ?>
        $('#' + "<?php echo $selectedFolder; ?>").children(".showactivities").click();
    <?php endif; ?>

});
</script>

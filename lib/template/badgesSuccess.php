<?php
    global $OUTPUT, $CFG;
    if ($message) echo $OUTPUT->error_text($message);
    echo $OUTPUT->container_start('tbcontainer', 'idtbcontainer');
    include_once($CFG->dirroot . "/local/teacherboard/lib/template/popup.php");
?>

<h3 class='tbtitle'>
    <a href='<?php echo $mainBoard; ?>'><img style='width:30px;margin-right:10px;' src='<?php echo RETURNMENU; ?>' alt='' title=''/></a>
        <span><?php echo get_string('course:badges:title', 'local_teacherboard') . $coursename; ?></span>
</h3>

<?php
    echo $OUTPUT->container_start('tbmenu', 'idtbmenu');
?>

<ul class="tbmenu_ul">
    <li><a id="addbadge" href="#"><?php echo get_string('course:badges:add', 'local_teacherboard'); ?></a></li>
</ul>

<?php
    echo $OUTPUT->container_end(); ?>

<div style="margin: 10px;">
    <div class="badges container_array_badges" style='display:<?php echo $nb_badges ? "block" : "none"; ?>'>
        <table class='dd modules_table main_modules' id="array_badges" style="background:none;border:0px;display:table;">
            <thead>
                <tr class="row_head_modules">
                <th style='background:none;'></th>
                <?php foreach($badges as $key => $badge): ?>
                <th class='badge_header' style='background:none;position:relative;' data-rank='<?php echo $key; ?>'>
                    <img  style='width:50px;' src='<?php echo $badge->image; ?>' alt='<?php echo $badge->name; ?>' title='<?php echo $badge->name; ?>' />
                    <img class='remove' src='<?php echo DELETE; ?>' data-source='<?php echo $badge->image; ?>' data-name='<?php echo $badge->name; ?>' data-id='<?php echo $badge->id; ?>' alt='supprimer' title='supprimer le badge' style='width:20px;position:absolute;right:2px;bottom:5px;cursor:pointer;' />
                </th>
                <?php endforeach; ?>
                </tr>
            </thead>
            <tbody class='dd-list-table'>
            <?php foreach ($coursemodules as $modules): ?>
            <?php if (count($modules)): ?>
            <?php foreach ($modules as $module): ?>
                <tr class='dd-item row_modules' data-id='<?php echo $module['cm']->id; ?>'>
                    <td class='dd-cell'><?php echo $module['name']; ?></td>
                    <?php foreach($badges as $badge): ?>
                    <?php if ($module['completion']): ?>
                    <?php if (is_object($badge->criteria[BADGE_CRITERIA_TYPE_ACTIVITY]) &&
                                is_array($badge->criteria[BADGE_CRITERIA_TYPE_ACTIVITY]->params) &&
                                is_object($module['cm']) &&
                                array_key_exists($module['cm']->id, $badge->criteria[BADGE_CRITERIA_TYPE_ACTIVITY]->params)): ?>
                        <td class="module_checked" title="activité à valider pour l'obtention du badge">
                            <!-- <input class="checkbox_badges" checked disabled data-id="<?php echo $module['cm']->id; ?>" type="checkbox" style="cursor:pointer;"> -->
                        </td>
                    <?php else: ?>
                        <td>
                            <!-- <input class="checkbox_badges" disabled data-id="<?php echo $module['cm']->id; ?>" type="checkbox" style="cursor:pointer;"> -->
                        </td>
                    <?php endif; ?>
                    <?php else: ?>
                        <td><!-- <input type="checkbox" disabled title="activité sans achèvement"> -->
                        </td>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="badges no_badges" style="text-align:center;padding:10px;background-color:white;display:<?php echo $nb_badges ? "none" : "block"; ?>">
        aucun badge disponible actuellement sur ce parcours
    </div>

</div>


<?php

$popup_deletebadge = new Popup(
        'popup_deletebadge',
        'Suppression de badge',
        '
<div class="modal-body course-modal-body">    
    <fieldset style="margin-top:10px;">
        <div style="display: flex;align-items: center;">
            <div style="flex: 4;">
                <input id="badge_name_delete" type="text" style="width:100%;padding:10px;" disabled value="" />
            </div>
            <div style="flex: 1;">
                <img id="badge_selector_delete" src=\'/local/teacherboard/lib/template/images/badges/badge10.png\' alt=\'\' style="width:80%;margin:0 auto;display:block;"/>
            </div>
        </div>
    </fieldset>
    <fieldset style="margin-top:10px;overflow-x:auto;">
        <div>
            Vous êtes sur le point de supprimer ce badge. Sachez néanmoins que les élèves l\'ayant déjà obtenu en gardent le bénéfice même après sa suppression.
        </div>
    </fieldset>
    
    <button id="popup_deletebadge_confirmation" type="button" class="btn btn-secondary newcourse-modal-valbtn">Supprimer le badge</button>
</div>
'
);

echo $popup_deletebadge->display();

$addpagecontent = '
<div class="modal-body course-modal-body">
    <fieldset style="margin-top:10px;">
        <legend><h3 class="course-modal-title">1. Saisissez le nom du badge</h3></legend>
        <div style="display: flex;align-items: center;">
            <div style="flex: 4;">
                <input id="badge_name" type="text" style="width:100%;padding:10px;" placeholder="Nouveau Badge" value="" />
            </div>
            <div style="flex: 1;">
                <img id="badge_selector" src=\'/local/teacherboard/lib/template/images/badges/badge10.png\' alt=\'\' style="width:80%;margin:0 auto;display:block;"/>
            </div>
        </div>
    </fieldset>
    <fieldset style="margin-top:10px;overflow-x:auto;">
        <legend><h3 class="course-modal-title">2. Choisissez un badge</h3></legend>
        <div style=\'display: flex;align-items: top;width:400%;background-color:#efefef;\'>';

foreach ($badges_images as $key => $badge_image) {
    if ($key%3 == 0) {
        $addpagecontent .= '<ul style="flex: 1;list-style-type:none;margin:0;">';
    }
    $addpagecontent .= '<li><img class="badgeimage" style="cursor:pointer;width:90%;display:block;margin:5px auto;"  
        src="' . $badge_image['src'] . '" alt="' . $badge_image['title'] . '" title="' . $badge_image['title'] . '"/></li>';
    if ($key%3 == 2) {
        $addpagecontent .= '</ul>';
    }
}
$addpagecontent .= '
        </div>
    </fieldset>

        <div class="input-file-container">
          <input class="input-file" id="uploadimage" type="file">
          <label for="my-file" class="input-file-trigger" tabindex="0">...ou chargez une image</label>
        </div>

    <fieldset style="margin-top:10px;">
        <legend><h3 class="course-modal-title">3. sélectionnez les activités à valider</h3><h4>(pour l\'obtention du badge)</h4></legend>';

foreach ($coursemodules as $modules) {
    if (count($modules)) {
        $addpagecontent .= '<table class="dd modules_table" style="display:table;">
            <tbody class="dd-list-table">';

        foreach ($modules as $module) {
            $addpagecontent .= '        <tr class="dd-item">
            <td class="dd-cell">' . $module['name'] . '</td>
            <td>';

            if($module['completion']) {
                $addpagecontent .= '<input class="module_selected" data-id="' . $module['cm']->id . '" type="checkbox" style="cursor:pointer;">';
            } else {
                $addpagecontent .= '<input type="checkbox" disabled title="activité sans achèvement">';
            }

            $addpagecontent .= '
            </td>
        </tr>
';
        }

        $addpagecontent .= '
        </tbody>
        </table>';
    }
}
$addpagecontent .= '
    </fieldset>

    <button id="popup_addbadge_confirmation" type="button" class="btn btn-secondary newcourse-modal-valbtn">Créer le badge</button>
</div>';

$popup_addbadge = new Popup(
        'popup_addbadge',
        get_string('course:badges:new', 'local_teacherboard'),
        $addpagecontent
);
    echo $popup_addbadge->display();


    echo $OUTPUT->container_end();
?>

<script>
$( document ).ready(function() {


    // =================================
    //
    //  used to store global params
    //
    // =================================
    var RegistryObject = function() {

    }

    var registry = new RegistryObject()
    registry.nb_badges = <?php echo count($badges); ?>

    // =================================
    //
    //  used to activate debug mode
    //
    // =================================
    var DebugObject = function() {
        this.active = true;
    }
    DebugObject.prototype.log = function(msg) {
        if (this.active) console.log(msg)
    }
    var debug = new DebugObject()

    // =================================
    //
    //  send request to server
    //  to create badge
    //
    // =================================

    var createBadge = function() {
        $("#popup_addbadge").modal('hide')

        var _badgename = $("#badge_name").val()
        var _badgesource = $("#badge_selector").attr('src')
        var _badgeid = 0
        if (! _badgename) _badgename = "Nouveau badge"
        var _modules_selected = ""
        $(".module_selected").each(function(){
            if ($(this).is(':checked')) {
                if (_modules_selected == "") {
                    _modules_selected = $(this).data('id')
                }
                else {
                    _modules_selected = _modules_selected + ";" + $(this).data('id')
                }
            }
        })
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxcreatebadge',
            type: 'POST',
            data: 'courseid=<?php echo $courseid; ?>'
                    + '&sesskey=<?php echo $sesskey; ?>'
                    + '&name=' + encodeURIComponent(_badgename)
                    + '&modules=' + _modules_selected
                    + '&source=' + encodeURIComponent(_badgesource),
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }
                //var _badge = $.parseJSON(_data)
                var _badgeid = _data.id
                $('.no_badges').hide();
                $('.container_array_badges').show();
                $('.row_head_modules').append('<th class="badge_header" data-rank="' + registry.nb_badges + '" style="background:none;position:relative;"><img style="width:50px;" src="' + _badgesource + '" alt="' + _badgename + '" title="' + _badgename + '" /><img data-id="'+ _badgeid +'" data-name="'+ _badgename +'" data-source="'+ _badgesource +'" class="remove" src="<?php echo DELETE; ?>" alt="supprimer" title="supprimer le badge" style="width:20px;position:absolute;right:2px;bottom:5px;cursor:pointer;" /></th>')
                registry.nb_badges++
                $('.row_modules').each(function(){
                    if (_modules_selected.toString().indexOf($(this).data('id')) > -1) {
                        $(this).append('<td class="module_checked"  title="activité à valider pour l\'obtention du badge"><!-- <input class="checkbox_badges" checked disabled data-id="'+$(this).data('id')+'" type="checkbox" style="cursor:pointer;"> --></td>')
                    }
                    else {
                        $(this).append('<td><!-- <input class="checkbox_badges" disabled data-id="'+$(this).data('id')+'" type="checkbox" style="cursor:pointer;"> --></td>')
                    }
                })
            }
        })
    }

    // =================================
    //
    //  add new badge
    //
    // =================================

    var addBadge = function(ev) {
        ev.preventDefault()
        $('.module_selected').prop('checked', false);
        $("#popup_addbadge").modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });
    };
    // =================================
    //
    //  delete badge
    //
    // =================================

    var deleteBadge = function() {
        $("#popup_deletebadge").modal('hide')

        _badgeid = $("#popup_deletebadge").data('badgeid');
        _badgerank = $("#popup_deletebadge").data('badgerank');

        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletebadge',
            type: 'POST',
            data: 'courseid=<?php echo $courseid; ?>'
                    + '&sesskey=<?php echo $sesskey; ?>'
                    + '&id=' + (_badgeid),
            success: function(_data) {
                if (!_data.isloggedin) {
                    document.location.href = _data.logurl
                    return
                }
                if (_data.state == 'success') {
                    registry.nb_badges--
                    if (registry.nb_badges == 0) {
                        $('.no_badges').show();
                        $('.container_array_badges').hide();
                    }
                    $('#array_badges tr').find('th:nth-child('+_badgerank+'), td:nth-child('+_badgerank+')').hide()
                }
            }
        })
    };
    // =================================
    //
    //  dialog to delete badge
    //
    // =================================

    var dialogDeleteBadge = function(ev) {
        ev.preventDefault()
        var _badgename = $(this).data('name')
        var _badgesource = $(this).data('source')
        var _badgeid = $(this).data('id')
        //var _badgerank = $(this).parent().data('rank') + 2
        var _badgerank = parseInt($(this).parent().index()) + 1
        debug.log(_badgerank)
        $("#badge_name_delete").val(_badgename)
        $("#badge_selector_delete").attr('src', _badgesource)
        $("#popup_deletebadge").data('badgeid', _badgeid)
        $("#popup_deletebadge").data('badgerank', _badgerank)

        $("#popup_deletebadge").modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });
    };
    // =================================
    //
    //  select badge
    //
    // =================================

    var selectBadge = function(ev) {
        ev.preventDefault()
        var source = $(this).attr("src")
        var title = $(this).attr("title")

        $("#badge_selector").attr("src", source)
        $("#badge_selector").attr("title", title)

    };
    // =================================
    //
    //
    // =================================

    var previewFile = function() {
        var file    = document.querySelector('#uploadimage').files[0]
        var reader  = new FileReader()
        reader.addEventListener("load", function () {
            $("#badge_selector").attr("src", reader.result)
            $("#badge_selector").attr("title", "yo")
        }, false);

        if (file) {
            reader.readAsDataURL(file)
        }
    }
    // =================================
    //
    //
    // =================================

    var checkboxToggle = function() {
        if ($(this).parent().css('background-color') == 'rgb(176, 189, 198)') {
            $(this).parent().css('background-color', '#A9C031');
        }
        else {
            $(this).parent().css('background-color', '');
        }
    }

    var handlerIn = function(ev) {

        var t = parseInt($(this).index()) + 1;
        if (t != 1) {
            $('td:nth-child(' + t + ')').each(function(){
                if ($(this).hasClass('module_checked')) {
                    $(this).addClass('highlighted-checked');
                }
                else {
                    $(this).addClass('highlighted');
                }
            })
        }

    }
    var handlerOut = function(ev) {
        var t = parseInt($(this).index()) + 1;
        $('td:nth-child(' + t + ')').removeClass('highlighted');
        $('td:nth-child(' + t + ')').removeClass('highlighted-checked');


    }

    enterKeyPressed = function(event) {
        event.preventDefault()
        if (event.keyCode === 13) {
            deleteBadge()
        }
    }
    // =================================
    //
    //  Bind events
    //
    // =================================
    var bindEvents = function() {
        $("#addbadge").on('click', addBadge)
        $("#array_badges").on('click','.remove', dialogDeleteBadge)
        $("#uploadimage").on('change', previewFile)
        $(".badgeimage").on('click', selectBadge)
        $("#array_badges").on('click', '.checkbox_badges', checkboxToggle)
        $("#array_badges").on('mouseenter tap', 'td, th', handlerIn)
        $("#array_badges").on('mouseleave', 'td, th', handlerOut)
        $("#popup_deletebadge_confirmation").on('click', deleteBadge)
        $("#popup_addbadge_confirmation").on('click', createBadge)
        $("#popup_deletebadge").on('keydown', {instructions: 'confirmDeleteCourse(true)'}, enterKeyPressed)

    }
    bindEvents()

});
</script>

<?php
    global $OUTPUT, $CFG;
    if ($message) echo $OUTPUT->error_text($message);
    echo $OUTPUT->container_start('tbachievement', 'idtbcontainer');
?>    
<h3 class='tbtitle'>
    <a href='<?php echo $mainBoard; ?>'><img style='width:30px;margin-right:10px;' src='<?php echo RETURNMENU; ?>' alt='' title=''/></a>
        <span><?php echo get_string('tbachievement', 'local_teacherboard') . $coursename; ?></span>
</h3>



<div style="margin: 10px; background-color: white;">

    <?php
    // Activity line
    $htmlline = "
    <table class=\"table_achievement\" style=\"width:100%;border-collapse: separate;border-spacing: 0px 2px;table-layout: fixed;\">
        <tr>
        <td class='identity'>Activités</td>";
        foreach ($activities as $activity) {
            $htmlline .= "<td class='cell_grey hrow' title=\"" . $activity['name'] . "\">";
            $htmlline .= $OUTPUT->pix_icon('icon', $activity['name'], $activity['module'], ['class'=> 'iconsize-big']);
            $shortname = $activity['name'];
            $htmlline .= "<span><br>$shortname</span>";
            $htmlline .= "</td>";
        }
        $htmlline .= "
        </tr>
    </table>";
    echo $htmlline;
    ?>

    <?php foreach ($enrols as $name => $students): ?>
    <?php 
    // if cohorts enrollment exists, show cohort name
    // else, do not display anything
    if (count($enrols) > 1): ?>
    <h3 style="margin: 0px; padding: 10px;"><?php echo $name ?></h3>
    <?php endif; ?>
    <table class='table_achievement' style="width:100%;border-collapse: separate;border-spacing: 0px 2px;table-layout: fixed;">
        <?php foreach ($students as $student): ?>
        <tr>
            <td class="identity"><?php echo $student['identity']; ?></td>
            <?php foreach($student['activities'] as $activity): ?>
            <td class='<?php echo $activity['style']; ?>' title='<?php echo $activity['title']; ?>'>
            <?php if ($activity['circle'] == "transparent"): ?>
                <div class="not_completed"></div>
            <?php elseif ($activity['circle'] == "black"): ?>    
                <div class="completed">
                   <?php if ($activity['attempts']): ?>
                    <div class="completed_attempts" style="position:relative; color: white; bottom: 22px;">
                        <?php echo $activity['attempts'] ?>
                    </div>
                   <?php endif;?>
                </div>
            <?php endif; ?>
            </td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endforeach; ?>
</div>

<?php

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/template/popup.php");



$popupdelete = new Popup(
        'popup_delete',
        'Suppression d\'un parcours',
        '<div class="modal-body course-modal-body"><div>' . get_string('warningDeleteCourse', 'local_teacherboard') . '</div>
<button id="popup_delete_confirmation" type="button" class="btn btn-secondary newcourse-modal-valbtn">'
. get_string('msgConfirmDeletion', 'local_teacherboard') . '</button></div>');
$popupdeleteforbidden = new Popup(
    'popup_deletefolder_forbidden',
    get_string('warningDeleteFolder', 'local_teacherboard'),
    '');

echo $popupdelete->display();
echo $popupdeleteforbidden->display();

?>


<?php
    echo $OUTPUT->container_end();
?>

<script>
$( document ).ready(function() {


    // =================================
    //
    //  used to store global params
    //
    // =================================            
    var RegistryObject = function() {
        this.enrolCourseId = null;
    }

    var registry = new RegistryObject();

    // =================================
    //
    //  used to activate debug mode
    //
    // =================================            
    var DebugObject = function() {
        this.active = true;
    }
    DebugObject.prototype.log = function(msg) {
        if (this.active) console.log(msg);
    }

    var debug = new DebugObject();

    // =================================
    //
    //  convert cohorts list into jquery ui tabs
    //
    // =================================            

    $("#tab-cohorts").tabs();

    var enrolCohorts = function() {
        var _courseid = $(this).data('courseid');
        registry.enrolCourseId = _courseid;
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxgetcohorts',
            type: 'POST',
            data: 'courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                $("#cohortfilter").val('');
                $("#cohort-filter-block-fail").hide();
                var _li_cohorts = [];
                for(var index in _data) {
                    _li_cohorts.push(_data[index].customint1);
                    //_li_cohorts[parseInt(_data[index].customint1)] = "x";
                }
                $("#tab-cohorts-block").find("li").each(function(){
                    $(this).show();
                    if (_li_cohorts.indexOf($(this).data('id').toString()) != -1) {
                        $(this).removeClass("cohort_not_selected").addClass("cohort_selected");
                    }
                    else {
                        $(this).removeClass("cohort_selected").addClass("cohort_not_selected");
                    }
                });
                
                $("#popup_cohorts").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                })
            }
        });        
    }
    /*
     *
     *
     */
    var filterCohorts = function() {
        var filter = $(this).val().toLowerCase();
        $("#tab-cohorts-block").find("li").each(function() {
            $(this).show()
        })
        if(filter){
            $("#cohort-filter-block-fail").show()
            $("#tab-cohorts-block").find("li").each(function() {
                var name = $(this).find('div').html().toLowerCase()
                if(name.indexOf(filter) === -1) $(this).hide()
                else $("#cohort-filter-block-fail").hide()
            })
        }
    }
    /*
     * 
     * 
     */
    var toggleEnrolCohort = function() {
        var _cohortid = $(this).data('id');
        var _courseid = registry.enrolCourseId;
        var object_clicked = $(this);
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxtoggleenrolcohort',
            type: 'POST',
            data: 'cohortid=' + _cohortid + '&courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                if (object_clicked.hasClass("cohort_selected")) {
                    object_clicked.removeClass("cohort_selected").addClass("cohort_not_selected");                    
                }
                else {
                    object_clicked.removeClass("cohort_not_selected").addClass("cohort_selected");
                }
            }
        });     
    
    }
    /*
     * 
     * 
     */
    var duplicateCourse = function(ev) {
        ev.preventDefault();
        $(".spinner").fadeIn();
        var _courseid = $(this).data('courseid');
        var _td = $(this).parent().parent();
        var _tr = _td.parent();
        var _tbody = _tr.parent();
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxduplicatecourse',
            type: 'POST',
            data: 'courseid=' + _courseid + '&sesskey=<?php echo $sesskey; ?>',
            success: function(_data) {
                $(".spinner").fadeOut();
                if (_data != "") {
                    _tbody.append(_data);
                }
            }
        });     
    
    }
    // =================================
    //
    //  function called each time
    //  we want to add a NEW FOLDER
    //
    // =================================            
    $("#addfolder").on('click', function(){
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxaddfolder',
            type: 'POST',
            data: 'sesskey=<?php echo $sesskey; ?>',
            success: function(data) {
                $("#nestable").children().append(data);

                // bind event on new <table>
                $(".modules_table").last().nestable({
                    listNodeName    : 'tbody',
                    itemNodeName    : 'tr',
                    rootClass       : 'dd',
                    listClass       : 'dd-list-table',
                    handleClass     : 'dd-handle2',
                    maxDepth        : 1,
                    group           : 1
                }).on('change', updateOutput);                  
            }
        });
    });            

    _dragitem = new Array();

    // =================================
    //
    //  function called each time
    //  we rename the current FOLDER
    //
    // =================================

    var renameFolder = function(newfolder) {
        var sesskey = "<?php echo $sesskey; ?>";
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxrenamefolder',
            type: "POST",
            data: "folderid=" + newfolder.id + "&foldername=" + newfolder.name + "&sesskey=" + sesskey,
            success: function(data) {
                debug.log(data);
                /**
                 * @TODO : update popup_duplication list
                 */ 
            }
        });	            
    }

    // =================================
    //
    //  toggle button to edit the name 
    //  of the current FOLDER
    //
    // =================================

    var toggleEditButton = function(ev) {
        if ($(this).attr('src') == '<?php echo EDIT; ?>') {
            $(this).attr('src','<?php echo EDIT_OK; ?>');
            $(this).parent().children('.input_course').css({'background-color': '#F0E0FF','cursor':'text'});
        }
        else {
            $(this).attr('src','<?php echo EDIT; ?>');
            $(this).parent().children('.input_course').css({'background-color': '#FFFFFF','cursor':'pointer'});
            var newfolder = {
                name: $(this).parent().children('.input_course').val(),
                id: $(this).parent().children('.input_course').attr('name')
            };
            renameFolder(newfolder);            
        }                
    };

    // =================================
    //
    //  record name of the PAGE if we press enter
    //
    // =================================

    var folderTitleKeypress = function(ev) {
        if(ev.keyCode == 13) {
            $(this).css({'background-color':'rgb(255,255,255)','cursor':'pointer'});
            $(this).parent().children('.edit_title').attr('src','<?php echo EDIT; ?>');
            var newfolder = {
                name: $(this).val(),
                id: $(this).attr('name')
            };                    
            renameFolder(newfolder);		
        }                
    };

   // =================================
   //
   //  If edit mode is active, edit title
   //
   // =================================

    var preventTitleModification = function(ev) {
       if ($(this).parent().children('.edit_title').attr('src') == '<?php echo EDIT; ?>') {
           ev.preventDefault();
       }
    };

    // =================================
    //
    //  Delete a FOLDER if it is empty
    //
    // =================================

    var deleteFolder = function(ev) {
        ev.preventDefault();
        var _folderid = $(this).data('folderid');
        var _li = $(this).parent().parent();
        debug.log("delete folder");
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletefolder',
            type: "POST",
            data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>"
        }).done(function(data){
            if (data != 'done') {
                $("#popup_deletefolder_forbidden").modal({
                    show: 'true',
                    backdrop: true,
                    keyboard: true
                });
            }
            else {
                _li.fadeOut('slow');
            }                    
        });
    };

    // =================================
    //
    //  Add COURSE in the current FOLDER
    //
    // =================================

    var addCourseCallback = function(object_clicked) {
                    
        var _table = object_clicked.parent().children(".dd");
        var _tbody = _table.children(".dd-list-table");
        var _folderid = object_clicked.data('folderid');
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxaddcourse',
            type: "POST",
            data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>"
        }).done(function(_data){
            if ((_data == "context_null")||(_data == "invalid_token")||(_data == "fail")) return;
            if (_tbody.children('.dd-empty').length != 0) {
                _tbody.children('.dd-empty').remove();
            }
            _tbody.append(_data);
            }
        );    
    }

    var addCourse = function(ev) {
        ev.preventDefault();
        var _table = $(this).parent().children(".dd");
        // tbody not visible
        if (_table.hasClass("hideactivities")) {
            showCourses(ev, addCourseCallback, $(this));
        }
        // tbody already visible
        else {
            addCourseCallback($(this));
        }
    };

    // =================================
    //
    //  function called at mousedown
    //  on a PAGE
    //
    // =================================
    var dragitem = function(e)
    {
        _dragitem.push($(this).parent().children(".input_course").attr('name'));
    };

    // =================================
    //
    //  function called each time
    //  we mousedown on an item
    //
    // =================================

    var drag_item = function(object_clicked) {
        
        _dragitem.push(object_clicked.parent().data("id"));

    };
    
    // =================================
    //
    //  function called each time
    //  a MODULE or a PAGE is dropped down
    //
    // =================================
    var updateOutput = function(e)
    {
        
        if (e.target.id == this.id){
            
            if ((this.id == 'nestable') && (_dragitem[0] != undefined)) {

                _parent = $("#page_"+_dragitem[0]).parent().parent().children(".input_course").attr('name');
                _previous = $("#page_"+_dragitem[0]).prev().children(".input_course").attr('name');
                _current = _dragitem[0];
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxmovefolder',
                    type: "POST",
                    data: "parentid=" + _parent + "&previousid=" + _previous + "&currentid=" + _current + "&sesskey=<?php echo $sesskey; ?>",
                    success: function(data) {
                        debug.log(data);
                    }
                }); 
            }
            else {
                if (_dragitem[0] != undefined) {
                    debug.log(_dragitem[0]);
                    var _tr = "#item_" + _dragitem[0];
                    var _tbody = $(_tr).parent();
                    var _table = $(_tbody).parent();
                    var _li = $(_table).parent();
                    var _currentItemId = _dragitem[0];
                    var _previousItemId = $(_tr).prev().data("id");
                    var _folderId = $(_li).children(".input_course").attr('name');
                    $.ajax({
                        url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxmovecourse',
                        type: "POST",
                        data: "previousitemId=" + _previousItemId + "&currentItemId=" + _currentItemId + "&folderid=" + _folderId + "&sesskey=<?php echo $sesskey; ?>",
                        success: function(data) {
                            debug.log(data);
                        }
                    });                     
                }
            }
            _dragitem.length = 0;
        }
    };

    // =================================
    //
    //  show COURSES of specified FOLDER
    //
    // =================================

    var showCourses = function(ev, addCourseCallback = null, object_clicked = null) {
        
        ev.preventDefault();
        object_clicked = object_clicked || $(this);
        var _folderid = object_clicked.data("folderid");

        _container = object_clicked.parent().children(".dd");
        if (_container.hasClass("hideactivities")) {
            object_clicked.attr('src','<?php echo FOLDER_OPENED; ?>');

            if (_container.children(".dd-list-table").length) {
                // Wrap table into a div to make slidedown effect working
                // the div wrapper is removed at the end of this effect
                _container.wrap("<div style='width:100%;' class='table-wrapper'/>");
                _container.parent().hide();
                _container.show();                         
                _container.parent().slideDown("slow", 'swing', function(){
                    _container.unwrap();                                            
                    if (addCourseCallback != null) {
                        addCourseCallback(object_clicked);
                    }
                });

            }
            else {
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxshowcourses',
                    type: "POST",
                    data: "folderid=" + _folderid + "&sesskey=<?php echo $sesskey; ?>",
                    success: function(data) {
                        _container.children(".dd-list-table").remove();
                        _container.append(data);
                        // Wrap table into a div to make slidedown effect working
                        // the div wrapper is removed at the end of this effect
                        _container.wrap("<div style='width:100%;' class='table-wrapper'/>");
                        _container.parent().hide();
                        _container.show();                         
                        _container.parent().slideDown("slow", 'swing', function(){
                            _container.unwrap();
                            if (addCourseCallback != null) {
                                addCourseCallback(object_clicked);
                            }                               
                        });
                     
                    }
                });
            }
        }
        else {
            _container.fadeOut('slow');
            object_clicked.attr('src','<?php echo FOLDER; ?>');
        }
        _container.toggleClass("hideactivities");
    };


    // =================================
    //
    //  function called each time
    //  we want to hide or show a COURSE
    //
    // =================================
    showhideCourse = function() {
        var _itemid = $(this).data("itemid");
        var _sesskey = '<?php echo $sesskey; ?>';
        if ($(this).hasClass('hiddencourse')) {
            $(this).attr('src','<?php echo EYE_OPENED; ?>');
        }
        else {
            $(this).attr('src','<?php echo EYE_CLOSED; ?>');
        }
        $(this).toggleClass('hiddencourse');
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxshowhidecourse',
            type: 'POST',
            data: 'itemid=' + _itemid + '&sesskey=' + _sesskey,
            success: function(data) {
                    //
            }
        });
    }

    // =================================
    //
    //  function called each time
    //  we want to delete a COURSE
    //
    // =================================
    deleteCourse = function() {

        // get the <TR> tag
        var _tr = $(this).parent().parent();
        
        // get the <TBODY> tag
        var _tbody = _tr.parent();
        
        var _itemid = $(this).data("itemid");
        var _sesskey = '<?php echo $sesskey; ?>';

        popupid = '#popup_delete'
        $(popupid).data('itemid', _itemid);
        $(popupid).data('sesskey', _sesskey);
        $(popupid).data('tr', _tr);
        $(popupid).data('tbody', _tbody);
        $(popupid).modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });
    }

    confirmDeleteCourse = function() {

        popupid = '#popup_delete';

        $(popupid).modal('hide');

        _itemid = $(popupid).data('itemid');
        _sesskey = $(popupid).data('sesskey');
        _tbody = $(popupid).data('tbody');
        _tr = $(popupid).data('tr');

        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/teacherboard/ajax.php?action=ajaxdeletecourse',
            type: 'POST',
            data: 'itemid=' + _itemid + '&sesskey=' + _sesskey,
            success: function(_data) {
                if ((_data == "context_null")||(_data == "invalid_token")) return;
                _tr.fadeOut('slow').remove();
                if (_tbody.children('.dd-item').length == 0) {
                    _tbody.append("<tr class='dd-empty'><td class='dd-empty' colspan='<?php echo $arrayNbColumns; ?>'></td></tr>")
                }
            }
        });


    }



    // =================================
    //
    //  Bind events
    //
    // =================================
    var bindEvents = function() {
        $('#nestable').on('mousedown', '.dd-handle', dragitem);
        $('.modules_table').on('mousedown', '.dd-handle2', function(e) {
            drag_item($(this));
        });

        $("#nestable").on('click', '.deletepage', deleteFolder);
        $(".enrolcohort").on('click', toggleEnrolCohort);
        $("#cohortfilter").on('keyup', filterCohorts);
        $("#nestable").on('click', '.enrolcohorts', enrolCohorts);
        $("#nestable").on('mousedown', '.input_course', preventTitleModification);
        $("#nestable").on('keypress', '.input_course', folderTitleKeypress);
        $("#nestable").on('click', '.edit_title', toggleEditButton);
        $("#nestable").on('click', '.showactivities', showCourses);
        $("#nestable").on('click', '.showhidecourse', showhideCourse);
        $("#nestable").on('click', '.addcoursefolder', addCourse);  
        $("#nestable").on('click', '.deleteitem', deleteCourse);
        $("#nestable").on('click', '.duplicatecourse', duplicateCourse);
        $('#nestable').nestable({
            maxDepth    : 3
        }).on('change', updateOutput);
        $(".modules_table").nestable({
            listNodeName    : 'tbody',
            itemNodeName    : 'tr',
            rootClass       : 'dd',
            listClass       : 'dd-list-table',
            handleClass     : 'dd-handle2',
            maxDepth        : 1,
            group           : 1
        }).on('change', updateOutput);
        $("#popup_delete_confirmation").on('click', confirmDeleteCourse);

    }

    bindEvents();

});
</script>
<script src='<?php echo $CFG->wwwroot; ?>/local/teacherboard/lib/template/js/overflowMgr.js'></script>

    // =================================
    //
    //  DOM Controller
    //  used to avoid jquery dependency
    //
    // =================================

    var DOMControllerObject = function() {

    }
    DOMControllerObject.prototype.on = function(target, event, callback, context) {
      (function(context){
        $(target).on(event, function(ev){
            callback(ev, context)
        })
      })(context)
    }
    DOMControllerObject.prototype.onDynamic = function(container, target, event, callback, context) {
      (function(context){
        $(container).on(event, target, function(ev){
            callback(ev, context)
        })
      })(context)
    }
    DOMControllerObject.prototype.data = function(target, item) {
      return $(target).data(item)
    }
    DOMControllerObject.prototype.html = function(target, content) {
      $(target).html(content)
    }
    DOMControllerObject.prototype.tabs = function(target) {
      $(target).tabs()
    }
    DOMControllerObject.prototype.addClass = function (params) {
      $(params.container).find(params.target).each(function(){
          $(this).addClass(params.class)
      })
    }
    DOMControllerObject.prototype.modalDialog = function(params) {
      $(params.target).dialog({
          title: params.title,
          buttons: {
              "OK": function() {
                  $(this).dialog("close")
              }
          },
          open: function(){
              var originalTop = $(this).parent().css('top')
              $(this).parent().css({top: '0px'})
              $(this).parent().animate({
                'top': originalTop,
              },{
                duration : 400,
                easing : "linear",
                queue : false
              })
          },
          show: "fade",
          width: '100%',
          closeOnEscape: true,
          modal:true
      })
    }

    if (typeof module !== 'undefined' && module.exports != null) {
        exports.DOMControllerObject = DOMControllerObject
    }

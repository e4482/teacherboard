// =================================
//
//  cohort Manager
//
// =================================

var CohortManagerObject = function(params) {

  this.debug = params.debug
  this.cohortItem = params.cohortItem
  this.cohortsContainer = params.cohortsContainer
  this.cohortsTab = params.cohortsTab
  this.DOMController = params.DOMController
  this.NetworkController = params.NetworkController
  this.serverRoot = params.serverRoot
  this.sesskey = params.sesskey
  this.title = params.title
  this.enrolledUsers = []

  // Bind 'click' event to cohort dialog
  var target = this.cohortItem
  var userEvent = 'click'
  var callback = this.dialog
  var context = this

  this.DOMController.on(target, userEvent, callback, context)

  //$("#dialog_cohorts").on('click','input', this.enrolUser)

  // Bind 'click' event to user item
  var container = "#dialog_cohorts"
  var target = 'input'
  var userEvent = 'click'
  var callback = this.enrolUser
  var context = this

  this.DOMController.onDynamic(container, target, userEvent, callback, context)

  // idem, I want to replace following calls by Dom Controller calls
  this.DOMController.tabs(this.cohortsContainer)

  this.DOMController.addClass({
    container : this.cohortsTab,
    target : "li",
    class : "cohort_not_selected"
  })

}


/*
 * Fired when clicking on a user checkbox
 *
 * ev : click event
 *
 */
CohortManagerObject.prototype.enrolUser = function(ev, cm) {
  var target = ev.currentTarget
  var newuser = {
    id : cm.DOMController.data(target, "userid"),
    firstname : cm.DOMController.data(target, "firstname"),
    lastname : cm.DOMController.data(target, "lastname")
  }
  var alreadyEnrolled = lodash.find(cm.enrolledUsers, { 'id': newuser.id})
  if (alreadyEnrolled == undefined) {
    cm.enrolledUsers.push(newuser)
  }
  else {
    cm.enrolledUsers = lodash.pullAllBy(cm.enrolledUsers, [{ 'id': newuser.id }], 'id');
  }
  cm.enrolledUsers = lodash.orderBy(cm.enrolledUsers, ['lastname'], ['asc'])
  var renderList = ""
  for (var i = 0;i < cm.enrolledUsers.length; i++) {
    renderList += "<div id='user_" + cm.enrolledUsers[i].id + "' data-userid='" + cm.enrolledUsers[i].id + "'>" + cm.enrolledUsers[i].lastname + " " + cm.enrolledUsers[i].firstname + "</div>"
  }
  $("#users_enrolled").html(renderList)

}

/*
 * Dialog used to display students from the selected cohort
 *
 * ev : click event
 * cm : cohortManager context
 *
 */
CohortManagerObject.prototype.dialog = function(ev, cm) {
    ev.preventDefault()
    var target = ev.currentTarget
    var _cohortid = cm.DOMController.data(target, 'id')
    cm.NetworkController.XmlHttpRequest({
      url : cm.serverRoot + "/local/teacherboard/ajax.php?action=ajaxgetcohortusers",
      type : 'POST',
      data: 'cohortid=' + _cohortid + '&sesskey=' + cm.sesskey,
      success : function(_data) {
        var _li_cohorts = [];
        if (_data.status == 'success') {
          var targetDialog = "#dialog_cohorts"
          var jsonObject = JSON.parse(_data.content)
          var renderList = ""
          for (var prop in jsonObject) {
            var alreadyEnrolled = lodash.find(cm.enrolledUsers, { 'id': parseInt(jsonObject[prop].id)})
            var check = (alreadyEnrolled != undefined) ? 'checked' : ""
            renderList += '<input ' + check + ' type="checkbox" id="userid_' + jsonObject[prop].id + '" data-userid="' + jsonObject[prop].id + '" data-firstname="' + jsonObject[prop].firstname + '" data-lastname="' + jsonObject[prop].lastname + '" name="userlist[]" value="' + jsonObject[prop].id + '"></input>'
            renderList += '<label for="userid_' + jsonObject[prop].id + '">' + jsonObject[prop].lastname + ' ' + jsonObject[prop].firstname + '</label>'
          }
          cm.DOMController.html(targetDialog, renderList)
          cm.DOMController.modalDialog({
            target : targetDialog,
            title : cm.title
          })
        }
        else {
          cm.DOMController.modalDialog({
            target : "#dialog_failure",
            title : "failure"
          })
        }
      }
    })
}

if (typeof module !== 'undefined' && module.exports != null) {
    exports.CohortManagerObject = CohortManagerObject
}

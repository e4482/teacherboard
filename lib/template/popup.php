<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/12/18
 * Time: 07:56
 */

/**
 * Class Popup
 * Class that display elements of popups in teacherboard.
 */
class Popup
{
    private $name;
    private $title;
    private $content;
    private $titleisidentified;

    /**
     * Popup constructor.
     *
     * @param $name : popup id
     * @param $title : title displayed
     * @param $content : content displayed
     *
     * title division is identified with ${name}_title
     */
    public function __construct($name, $title, $content)
    {
        $this->name = $name;
        $this->title = $title;
        $this->content = $content;
    }

    public function display() {

        return "
<div class=\"modal fade course-modal\" id=\"" . $this->name . "\" tabindex='-1'>
    <div class=\"modal-dialog\">
        <div class=\"modal-content container-fluid course-modal-container\">

            <div class=\"modal-header course-modal-header\">
                <button type=\"button\" class=\"close course-modal-close\" data-dismiss=\"modal\">x</button>
                <div><h3 id=\"" . $this->name . "_title\">" . $this->title . "</h3></div>
            </div>

            " . $this->content . "
        </div>
    </div>
</div>
        ";
    }
}
<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    teacherboard
 * @subpackage frontcontroller
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class View
{
    public function render($file, $assigns = array())
    {

        global $PAGE, $SITE, $CFG, $OUTPUT;

        $PAGE->set_url('/local/teacherboard/index.php');
        $context = context_system::instance();
        $PAGE->set_context($context);
        $PAGE->set_title($SITE->shortname);
        $PAGE->set_heading($SITE->fullname);
        if (property_exists($CFG, 'elea_release')) {
            $update = "?version=" . $CFG->elea_release;
        }
        else {
            $update = "";
        }
        $PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/teacherboard/lib/template/css/style.css' . $update));
        $PAGE->requires->jquery();
        $PAGE->requires->jquery_plugin('ui');
        $PAGE->requires->jquery_plugin('ui-css');
        $PAGE->requires->jquery_plugin('elea-jqueryui-css', 'theme_vital');
        $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/local/teacherboard/lib/template/js/jquery.nestable.js') );

        extract($assigns);
        ob_start();
        echo $OUTPUT->header();
        require($file);
        echo $OUTPUT->footer();
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }
    public function renderNoHeaders($file, $assigns = array())
    {
        extract($assigns);
        ob_start();
        require($file);
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }
}

?>

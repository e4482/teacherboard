<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:09
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");

class ParcoursGamifie extends GabaritParcours
{

    public function sectionNumber()
    {
        return 1;
    }

    public function makeContent($newcourse = null)
    {
        global $DB;

        $this->newcourse = $newcourse;

        /**
         * Section 0
         */

        $this->add_label_module(
            "<h3 style=\"text-align: center;\">La fin du monde est proche...</h3>
<h4 style=\"text-align: center;\">Titre à personnaliser en donnant un sens épique au parcours</h4>
<p><span style=\"font-size: 14px;\">Exemples de titres gamifiés : À la découverte de ...\", \"Dans la peau d'un journaliste\", etc.</span><br></p>
<p></p><div>
<h4><p style=\"text-align: center;\">----------</p></h4>
<p>Vous pouvez saisir ici le texte d'accueil du parcours.&nbsp;<span style=\"font-size: 14px;\">Il présente le scénario et annonce les objectifs. 
    La durée, les compétences travaillées peuvent également y être présentées.&nbsp;</span></p>
<p><span style=\"font-size: 14px;\">Dans un parcours gamifié, il est nécessaire de rédiger ce texte en suivant un fil conducteur cohérent avec le titre et la mise en situation choisie. 
    Par exemple si l'élève incarne un journaliste alors tous les textes de consignes incluront le champ lexical du journalisme.</span></p>
<p><span style=\"font-size: 14px;\">Pour en savoir plus sur la <b>gamification</b>, vous pouvez consulter les articles suivants :&nbsp;</span></p><p></p><ul><ul>
<ul><li><a href=\"http://www.education-aux-medias.ac-versailles.fr/la-gamification-les-enjeux-du-jeu\" target=\"_blank\" style=\"font-size: 14px;\">Gamification, les enjeux du jeu</a></li>
<li><a href=\"http://www.dane.ac-versailles.fr/nos-projets/e-education/gamification-sur-la-plateforme-elea\" target=\"_blank\" style=\"font-size: 14px;\">Gamification sur la plateforme Éléa</a></li>
<li><a href=\"http://www.dane.ac-versailles.fr/nos-projets/jeux-et-apprentissages/gamifiez-votre-sequence\" target=\"_blank\" style=\"font-size: 14px;\">Gamifiez votre séquence</a></li></ul></ul></ul>
<p></p><p><span style=\"font-size: 14px;\"><br></span></p></div><div><b>Pensez à \"Activer le mode édition\" pour faire les modifications 
    dans le gabarit et n'hésitez pas à choisir une autre carte de progression en cliquant sur \"Modifier\" puis \"Paramètres\".</b></div>
<p style=\"text-align: center;\">----------</p><p><br></p>
<p style=\"text-align: center;\"><img src=\"/local/teacherboard/lib/template/images/badges/superheros-b.png\" alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"100\" height=\"100\"><br></p><p></p>
    <h4>La Terre est en danger ! Il faut absolument la sauver. Pour cela tu vas devoir en apprendre plus sur ...&nbsp;</h4>
    <h4>Mais attention ton temps est limité : tu vas devoir réussir en moins de ... minutes.&nbsp;</h4><h4><br><p style=\"text-align: center;\"><b style=\"font-family: inherit;\">Es-tu prêt à franchir toutes les épreuves ?</b></p><p style=\"text-align: center;\"><b style=\"font-family: inherit;\"><br></b></p></h4>",
            0,
            true
        );

        $this->add_label_module(
            "<div style=\"width:30%;padding:10px;background-color:#F7D358;border-radius:50px;margin:0 auto;text-align:center;font-size:1.2em;\"><b>Lance-toi dans l'aventure !</b></div>",
            0,
            true
        );

        // Forcing creation of section 1 for mapmodule
        $cmid = $this->add_page_module(
            "<p><br></p><p style=\"text-align: center;\">
<img src=\"/local/teacherboard/lib/template/images/badges/superheros-b.png\" alt=\"\" width=\"100\" height=\"100\" class=\"img-responsive atto_image_button_center\"><br></p>
<p><b>La Terre est en danger ! </b>Une météorite
                    géante va entrer en collision avec notre belle
                    planète dans quelques jours. Le commandant Burk a
                    été missionné pour sauver le monde et il a besoin
                    d'un aventurier comme toi pour remplir
                    la mission la plus périlleuse de ta vie !</p>
                    <p><b>Mais seras-tu à la hauteur ? Pour le savoir, passe à l'activité suivante ...</b></p>",
            "",
            "Lance-toi dans l'aventure !",
            1
        );

        // After section 1 has been created
        $this->add_map_module(
            0,
            "4"
        );

        $this->add_folder_module(
            "Présentation du parcours gamifié",
            "<p><b>Élément invisible pour les élèves à destination des concepteurs de parcours.</b></p>
<p>Si vous souhaitez visualiser le parcours comme un élève, cliquez sur l'icône menu en haut à droite, puis choisissez \"Prendre le rôle de\" et \"Étudiant\". N'oubliez pas ensuite de revenir à votre rôle normal pour pouvoir modifier le parcours.</p><p><br></p><p>Ce parcours est organisé sur le modèle ci-dessous.</p><table ;=\"\" width=\"100%\" border=\"1\">
<tbody><tr>
<td style=\"text-align: center;\" width=\"15%\"><b>Section</b></td>
<td style=\"text-align: center;\"><b>Modalité</b>&nbsp;</td><td style=\"text-align: center;\"><b>Contenu</b></td>
<td style=\"text-align: center;\" width=\"45%\"><b>Paramètre</b></td>
</tr><tr>
<td style=\"text-align: center;\">Accueil<br></td>
<td style=\"text-align: center;\">&nbsp;Travail à distance ou en classe</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Étiquette\" visible</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Carte de progression\"<br>Ressource \"Dossier\" cachée (visible seulement des enseignants)<br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">&nbsp;Pas d'achèvement d'activité<br></div></td>
</tr><tr>
<td style=\"text-align: center;\"><br>Activités orphelines<br>(section 1 invisible pour les élèves)</td>
<td style=\"text-align: center;\">&nbsp;Travail à distance ou en classe</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Page\"<br>Activité \"Appariement\"</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Page\"<br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><div><span style=\"background-color: transparent;\"><div>Activité \"QCM\"<br></div></span></div></div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><div>Ressource \"Page\"</div><div>Activité \"Millionnaire\"<br>Ressource \"Page\"</div><br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Achèvement d'activité :&nbsp;</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">- pour les ressources -&gt; la page doit être affichée<br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">- pour les activités -&gt; l'activité doit être terminée (pas de note de passage)</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Restriction d'activité : il faut avoir fini l'activité précédente pour pouvoir faire l'activité suivante</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Obtention d'un badge après avoir affiché la dernière page (enregistrez l'image ci dessous et <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=concevoir-des-parcours&amp;item=ajouter-un-badge\" target=\"_blank\">ajoutez le badge sur la dernière activité en utilisant le tutoriel</a>).</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><img src=\"/local/teacherboard/lib/template/images/badges/superheros-b.png\" alt=\"\" width=\"100\" height=\"100\" class=\"img-responsive atto_image_button_text-bottom\"><br></div></td>
</tr>


</tbody>
</table>
<br><p></p><p>Accès aux tutoriels pour concevoir les parcours : <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof\" 
    target=\"_blank\">https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof&nbsp;</a>(si besoin vous pouvez consulter en particulier le tutoriel pour \"
    <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=concevoir-des-parcours&amp;item=cacher-les-sections-sur-la-page-d-accueil\" 
        target=\"_blank\">Cacher les sections sur la page d'accueil</a>\").</p><p>Pour comprendre comment fonctionne le bouton de lancement, vous pouvez regarder le tutoriel correspondant \"
    <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=creer-un-bouton-de-lancement-dans-l-etiquette-d-accueil\" 
        target=\"_blank\">Créer un bouton de lancement dans l'étiquette d'accueil</a>\".<br></p>",
            0,
            FileInModule::get_all_files_in_module(),
            true,
            false,
            true,
            true
        );

        /**
         * Section 1
         */

        $cmid = $this->add_epikmatching_module(
            "Le commandant veut savoir s'il peut te faire confiance en te posant quelques questions.&nbsp;<p><br></p><p>Relie chaque texte à son image respective.</p>",
            "Es-tu digne de cette mission ?",
            1,
            $cmid,
            [new AppariementContenu("La planète rouge"),
            new AppariementContenu("<img src=\"https://communaute.elea.ac-versailles.fr/mod/epikmatching/pix/planete_mars.png\" style=\"width:100px\" alt=\"la planète rouge\">"),

            new AppariementContenu("La planète bleue"),
            new AppariementContenu("<img src=\"/mod/epikmatching/pix/planete_terre.png\" style=\"width:100px\" alt=\"la planète bleue\">"),

            new AppariementContenu("La planète la plus froide"),
            new AppariementContenu("<img src=\"/mod/epikmatching/pix/planete_uranus.png\" style=\"width:100px\" alt=\"uranus\">")],
            false
        );

        $cmid = $this->add_page_module(
            "<p><b><img src=\"/local/teacherboard/lib/template/images/badges/superheros-b.png\" alt=\"\" width=\"100\" height=\"100\" class=\"img-responsive atto_image_button_center\"><br></b></p><p><b>Bravo ! Le commandant Burk est maintenant convaincu,
                    tu es l'homme de la situation !</b></p>
                    <p>Pour détruire la météorite, il faut activer le système secret défense
                    de dématérialisation des astéroïdes. Ce système est protégé par un ensemble de questions.
                    Déverrouille le système pour sauver le monde en passant aux fameuses questions...</p>",
            "",
            "La mission commence !",
            1,
            $cmid);

        $cmid = $this->add_millionnaire_module(
            "<p><br></p>Réponds aux questions pour activer le système de destruction des météorites !",
            "Désactive la protection...",
            1,
            $cmid,
            [new QuestionMillionnaire(
                "Comment s'appelle notre système ?",
                ["Le système solaire", "Le système polaire", "Le système molaire", "Le système scolaire"]),
            new QuestionMillionnaire(
                "Qu'est-ce que Pluton ?",
                ["Une planète naine", "Une planète avec des anneaux", "Une lune", "Un satellite"]),
            new QuestionMillionnaire(
                "Qu'est-ce que la voie lactée ?",
                ["Notre galaxie", "Notre système solaire", "Une galaxie lointaine, très lointaine...", "Un chemin rempli de lait"])
            ],
            3,
            false
        );

        $this->add_page_module(
            "<p></p><p style=\"text-align: center;\"><b>
<img src=\"/local/teacherboard/lib/template/images/badges/superheros-b.png\" alt=\"\" width=\"100\" height=\"100\" class=\"img-responsive atto_image_button_center\"><br></b></p>
<p><b>Bravo ! Tu as réussi ta mission !</b></p>
<p>La météorite est détruite et la planète est sauvée. Tu reçois le badge du commandant Burk comme preuve de ta réussite. À bientôt pour de nouvelles aventures...</p><br>
<p></p><img src=\"/local/teacherboard/lib/template/images/victory.jpg\" style=\"margin:0 auto;display:block;\" alt=\"victoire\">",
            "",
            "La mission est terminée !",
            1,
            $cmid
        );




            // update sections


        $sections = $DB->get_records_sql('
                        SELECT *
                        FROM {course_sections}
                        WHERE course ='.$newcourse->id.'
                        ORDER BY section ASC;
                    ');

        $sectionsContent = array(
            '1' => array(
                'name' => "La mission",
                'summary' => "<img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_globe.jpg\" alt=\"puzzle1\" width=\"100\" height=\"100\" style=\"float:left;width:200px;margin:20px;\" class=\"img-responsive atto_image_button_center\"> .
                                <h3 style=\"padding:20px;\">Vous avez dit météorite ?</h3>
                                <h4>À toi de sauver la planète !</h4>"
            )
        );

        $i = 1;
        foreach($sections as $section) {
            if ($section->section != 0) {
                $section->name = $sectionsContent[$i]['name'];
                $section->summary = $sectionsContent[$i]['summary'];
                $section->visible = false;
                $i++;
                $DB->update_record('course_sections', $section);
            }
        }

        //$this->add_badge($newcourse->id, $newcourse->fullname);
    }
}
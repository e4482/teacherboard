<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:17
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");

class ParcoursInverse extends GabaritParcours
{

    public function sectionNumber()
    {
        return 2;
    }

    public function makeContent($newcourse = null)
    {
        global $DB;

        $this->newcourse = $newcourse;

        /**
         * Section 0
         */

        /** Module étiquette */

        $label_content = "
<p><br></p><h3 style=\"text-align: center;\">&nbsp;titre du parcours à personnaliser<br></h3><p><br></p><table style=\"background:#ECF6CE\" width=\"100%\"><tbody><tr>

<td><br><div class=\"editor-indent\" style=\"margin-left: 30px;\"><h4>Vous pouvez saisir ici le texte d'accueil du parcours. </h4></div><div class=\"editor-indent\" style=\"margin-left: 30px;\">Il présente le scénario et annonce les objectifs. La durée, les compétences travaillées peuvent également y être présentées.</div><div class=\"editor-indent\" style=\"margin-left: 30px;\"><br></div><div class=\"editor-indent\" style=\"margin-left: 30px;\"><b>Pensez à \"Activer le mode édition\" pour faire les modifications dans le gabarit.</b><br></div><div class=\"editor-indent\" style=\"margin-left: 30px;\"><br></div><br></td>
</tr>
</tbody>
</table>
<br><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-etiquette\" target=\"_blank\">Consultez si besoin le tutoriel associé à cette ressource</a>.<br></p>
";
        $section_number = 0;
        $this->add_label_module($label_content, $section_number, true);

        /** Module dossier */
        $name="Présentation du parcours inversé";
        $desc = "
<p>Élément invisible pour les élèves à destination des concepteurs de parcours.</p><p>Ce parcours est organisé sur le modèle suivant : <br></p><p><br>
</p><table ;=\"\" width=\"100%\" border=\"1\">
<tbody><tr>
<td style=\"text-align: center;\"><b>Section</b></td>
<td style=\"text-align: center;\"><b>Modalité</b>&nbsp;</td><td style=\"text-align: center;\"><b>Contenu</b></td>
<td style=\"text-align: center;\"><b>Paramètre</b></td>
</tr><tr>
<td style=\"text-align: center;\">Accueil<br></td>
<td style=\"text-align: center;\">&nbsp;Travail à distance</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Étiquette\" visible<br>Ressource \"Dossier\" cachée (visible seulement des enseignants)<br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">&nbsp;Pas d'achèvement d'activité<br></div></td>
</tr><tr>
<td style=\"text-align: center;\">&nbsp;Section 1<br></td>
<td style=\"text-align: center;\">Travail à distance</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Page\"<br>Activité \"Appariement\"</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><div><span style=\"background-color: transparent;\">Ressource \"URL\" (lien internet)</span><br></div><div><span style=\"background-color: transparent;\"><div>Activité \"Millionnaire\"</div></span></div>Ressource \"Page\"<br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">&nbsp;Achèvement d'activité simple<br></div></td>
</tr>
<tr>
<td style=\"text-align: center;\">Section 2<br></td>
<td style=\"text-align: center;\">&nbsp;Travail en présence</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Page\"<br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Fichier\"<br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Activité \"QCM\"</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Activité \"Devoir\"<br>Ressource \"Page\"</div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Achèvement d'activité simple</div></td>
</tr>

</tbody>
</table>
<br><br><p></p><p>Accès aux tutoriels pour concevoir les parcours : <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof\" target=\"_blank\">https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof&nbsp;</a></p>
        ";
        $displayfolder = true;
        $displayunfold = false;
        $downloadable = true;


        $files = FileInModule::get_all_files_in_module();

        $this->add_folder_module($name, $desc, $section_number, $files, $displayfolder, $displayunfold, $downloadable, true);



        /**
         * Section 1
         * Travail à distance
         */
        $section_number = 1;

        /** Module titre */
        $page_desc = "";
        $page_title = "Titre de la première Page du parcours à personnaliser";
        $page_content = "
<p><br></p>
<h4>Vous pouvez saisir ici un <b>texte</b> en le mettant en forme librement et insérer les <b>ressources</b> (visuels, vidéos, liens, etc.)</h4>
<p><br></p><p>L'image ci-dessous peut-être supprimée et/ou remplacée par un autre fichier.</p>
<p style=\"text-align: center;\">
    <img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_puzzle1.png\" 
        alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"460\" height=\"340\">
<br></p>
<p>N'hésitez pas à personnaliser l'<b>organisation de la page</b> (alignement, taille de la police) avec votre contenu pour la rendre plus engageante.</p>
<p><br></p>
<p>Vous pouvez aussi insérer des <b>vidéos</b> directement dans les \"Pages\" sur Éléa.</p>
<div style=\"text-align: center;\">
    <iframe src=\"//scolawebtv.crdp-versailles.fr/?iframe&amp;id=11570\" allowfullscreen=\"\" width=\"570\" height=\"321\" frameborder=\"0\"></iframe>
</div>
<p><br></p>
<p>Pensez également à rajouter des consignes de navigation et à rappeler régulièrement les objectifs dans votre parcours.<br></p>
<p><br></p>
<p>
    <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-page\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette ressource</a>.
    <br>
</p>
<p><br></p>
";
        $cmid = $this->add_page_module($page_content, $page_desc, $page_title, $section_number);

        /** Module appariement*/
        $title = "Titre de l'activité Appariement à personnaliser ou à supprimer";
        $desc = "<p><br></p>
<p>La description vous permet de préciser les consignes de l'activité. <br></p>
<p><br></p>
<p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-appariement\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.<br></p>";
        $couples = [

            new AppariementContenu("
<p>Élément 1 (texte/image/vidéo/audio)</p>
<p><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_globe.jpg\" 
    alt=\"\" width=\"100\" height=\"100\" class=\"img-responsive atto_image_button_center\"><br>
</p>
<p>Source : Pixabay CC0</p>"),
            new AppariementContenu("Élément 1 bis (texte/image/vidéo/audio)"),

            new AppariementContenu("<p></p>
<p>Élément 2 (texte/image/vidéo/audio)</p>
<img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/video-logo.png\" 
    alt=\"logo\" width=\"100\" height=\"93\" class=\"img-responsive atto_image_button_middle\">
    <p>Source : Pixabay CC0<br></p><p></p><p><br></p><p></p>"),
            new AppariementContenu("Élément 2 bis (texte/image/vidéo/audio)"),

            new AppariementContenu("<p></p>
<p>Élément 3 (texte/image/vidéo/audio)</p>
<p><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_micro.png\" 
    alt=\"\" class=\"img-responsive atto_image_button_text-bottom\" width=\"100\" height=\"100\"><br></p>
    <p>Source : Freepik CC-BY Freebik</p><br><p></p>"),
            new AppariementContenu("Élément 3 bis (texte/image/vidéo/audio)")
        ];

        $cmid = $this->add_epikmatching_module($desc, $title, $section_number, false, $couples, 0);

        /** Module URL */
        $title = "Titre de la ressource URL/lien à personnaliser ou à supprimer";
        $url = "https://fr.wikipedia.org/wiki/Wikipédia:Accueil_principal";
        $desc = "<p><br></p>
<p>Vous pouvez insérer ici le texte de présentation associé au site internet à consulter.</p>
<p>Si le site accepte l'intégration (comme Wikipédia), il pourra s'afficher directement dans Éléa. <br></p>
<p>Si l'affichage du site ne fonctionne pas (nombreux sites institutionnels), il faut changer le paramètre \"Apparence\" pour obtenir un lien cliquable en sélectionnant l'option \"Ouvrir dans une fenêtre surgissante\". <br></p>
<p><br></p>";

        $cmid = $this->add_url_module($title, $url, $section_number, $desc, false, 0);

        /** Module Millionnaire */
        $title = "Titre de l'activité Millionnaire à personnaliser ou à supprimer";
        $desc = "<p><br></p><p></p><h4>
<p>Les consignes pour l'activité Millionnaire doivent être saisies ici.</p>
<p>Idéalement pour cette activité, 6 questions à choix multiple et réponse unique doivent être intégrées.<br></p></h4><br>
<p></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-millionnaire\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.<br></p>";
        $nbpoints = 10;
        $questions = [new QuestionMillionnaire(
            "Question 1",
            ["La réponse juste", "Une mauvaise réponse", "Une autre mauvaise réponse", "Une dernière mauvaise réponse"]),
            ];
        $cmid =$this->add_millionnaire_module($desc, $title, $section_number, false, $questions, $nbpoints, 0);

        /** Module page fin de section */
        $title = "Titre de la dernière Page de la section 1 à personnaliser";
        $content = "<p><br></p>
<p style=\"text-align: center;\"><img src=\"/local/teacherboard/lib/template/images/Travail-maison.jpg\" 
    alt=\"\" width=\"250\" height=\"186\" class=\"img-responsive atto_image_button_center\"><br></p>
<p>Cette page est un <b>élément de transition</b> : elle est à compléter avec des informations sur les différentes étapes du scénario. <br></p>
<p>On en positionne une à la fin de chaque section/étape ainsi qu'à la fin du parcours pour rappeler les objectifs, ouvrir sur la suite du déroulé, féliciter les élèves, etc.</p>
<p>Ce type de page doit permettre l'engagement des élèves et intégrer par exemple des éléments visuels.<br></p>
<p><br></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-page\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette ressource</a>.<br></p><p><br></p>";
        $cmid = $this->add_page_module($content, $desc, $title, $section_number, false, 0);


        /**
         * Section 2
         * Travail en présence
         */
        $section_number = 2;

        /** Page de description */
        $title = "Titre de la première Page de la section 2 à personnaliser";
        $content = "<p><br></p>
<h4>Vous pouvez saisir ici un <b>texte</b> en le mettant en forme librement et insérer les <b>ressources</b> (visuels, vidéos, liens, etc.)</h4>
<p><br></p><p>L'image ci-dessous peut-être supprimée et/ou remplacée par un autre fichier.</p>
<p style=\"text-align: center;\"><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_puzzle1.png\" 
    alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"460\" height=\"340\"><br></p>
<p>N'hésitez pas à personnaliser l'<b>organisation de la page</b> (alignement, taille de la police) avec votre contenu pour la rendre plus engageante.</p>
<p><br></p><p>Vous pouvez aussi insérer des <b>vidéos</b> directement dans les \"Pages\" sur Éléa.</p>
<div style=\"text-align: center;\"><iframe src=\"//scolawebtv.crdp-versailles.fr/?iframe&amp;id=11570\" 
    allowfullscreen=\"\" width=\"570\" height=\"321\" frameborder=\"0\"></iframe></div>
<p><br></p><p>Pensez également à rajouter des consignes de navigation et à rappeler régulièrement les objectifs dans votre parcours.<br></p>
<p><br></p>
<p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-page\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette ressource</a>.<br></p>
<p><br></p>";
        $cmid = $this->add_page_module($content, "", $title, $section_number, false, 0);

        /** Module Fichier */
        $cmid = $this->add_file_module(
            "Exemple de ressource Fichier à personnaliser ou à supprimer",
            "<p><br></p>
<p>La description permet de présenter la ressource à consulter et précise les consignes. Il peut par exemple s'agir de la version numérique d'une fiche de consignes pour une activité en classe.</p>

<p>Si vous insérez un fichier non modifiable, il s'affichera directement dans Éléa comme dans l'exemple ci-dessous.</p>
<p></p>
<a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-fichier\" target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.
<p><br></p>,
",
            $section_number,
            new FileInModule("Exemples de ressources à intégrer/", "PDF test Elea.pdf"),
            true,
            0,
            0
        );

        /** Module QCM */
        $title = "Exemple d'activité QCM à personnaliser ou à supprimer";
        $desc = "<p><br></p><p>Vous pouvez saisir ici les consignes pour l'activité QCM.</p>
<p><br></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-qcm\" target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.<br></p>
<p><br></p>";
        $nbpoints = 5;
        $qcm = [
          new QcmQuestion("Intitulé de la question 1",
              ["Réponse 1 juste", "Réponse 2 fausse", "Réponse 3 fausse"],
              [true, false, false])
        ];
        $cmid = $this->add_qcm_module($desc, $title, $section_number, false, $qcm, $nbpoints, 0);

        /** Module devoir */
        $title = "Exemple d'activité Devoir à personnaliser ou à supprimer";
        $desc = "<p><br></p>
<p>Vous pouvez insérer ici les consignes pour l'activité devoir en précisant le type de travail à déposer.</p>
<p><br></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-devoir\" target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.</p>";
        $cmid = $this->add_assignment_module($title, $desc, $section_number, false, 0);

        /** Module page fin de section */
        $title = "Titre de la dernière Page du parcours à personnaliser";
        $content = "<p><br></p><p style=\"text-align: center;\"><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_applauding-41640_640.png\" alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"150\" height=\"157\"><br></p><p>Cette page termine le parcours. <br></p><p>Elle clôture donc l'activité des élèves et permet de faire la synthèse des éléments abordés. Vous pouvez y rappeler les objectifs et ouvrir sur d'autres activités.</p>";
        $cmid = $this->add_page_module($content, "", $title, $section_number, false, 0);

        /**
         * Finalisation
         */

        // update sections

        $sections = $DB->get_records_sql('
                        SELECT *
                        FROM {course_sections}
                        WHERE course ='.$newcourse->id.'
                        ORDER BY section ASC;
                    ');

        $sectionsContent = array(
            '1' => array(
                'name' => "Travail à distance",
                'summary' => "&nbsp;<p><br>
</p>
<table width=\"100%\">
<tbody><tr>
<td style=\"text-align: center;\" width=\"20%\"><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_travail-maison.jpg\" alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"150\" height=\"111\"></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Présentation succincte de la section 1...</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Objectifs : <br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Durée : <br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">À faire pour le : <br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div></td>
</tr>
</tbody>
</table>
<br><br><p></p>"
            ),
            '2' => array(
                'name' => "Travail en présence",
                'summary' => "&nbsp;<p><br>
</p>
<table width=\"100%\">
<tbody><tr>
<td style=\"text-align: center;\" width=\"20%\"><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_classe.jpg\" alt=\"\" class=\"img-responsive atto_image_button_center\" width=\"150\" height=\"111\"></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Présentation succincte de la section 2...</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Objectifs : <br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Durée :&nbsp; <br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div></td>
</tr>
</tbody>
</table>
<br><br><p></p>"
            ),

        );

        $i = 1;
        foreach($sections as $section) {
            if ($section->section != 0) {
                $section->name = $sectionsContent[$i]['name'];
                $section->summary = $sectionsContent[$i]['summary'];
                $i++;
                $DB->update_record('course_sections', $section);
            }
        }

        //$this->add_badge($newcourse->id, $newcourse->fullname);

    }
}
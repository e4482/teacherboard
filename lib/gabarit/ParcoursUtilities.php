<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/10/18
 * Time: 15:30
 */
global $CFG;
define('GABARIT_FILE_PATH', $CFG->dirroot."/local/teacherboard/lib/gabarit/modfolder/");

class AppariementContenu {

    public $text;

    public function __construct($unTexte)
    {
        $this->text = $unTexte;
    }

    public function format() {
        return ['text' => $this->text, 'format' => FORMAT_HTML, 'itemid' => null];
    }
}

class QuestionMillionnaire {
    public $question;
    public $reponses;

    public function __construct($unequestion, $tabreponses)
    {
        $this->question = $unequestion;
        $this->reponses = $tabreponses;
    }
}

class QcmQuestion {

    public $question;
    public $reponses;
    public $validitesrep;

    public function __construct($unequestion, $desreponses, $desvalidites)
    {
        $this->question = $unequestion;
        $this->reponses = $desreponses;
        $this->validitesrep = $desvalidites;
    }
}

class FileInModule {
    private $pathname;
    private $filename;

    public function __construct($pathname, $filename)
    {

        $this->filename = $filename;
        $this->pathname = $pathname;
    }

    public function record_in_db ($contextid, $ismodfolder=true) {

        if($ismodfolder) {
            $newpathname = $this->pathname;
            $component = 'mod_folder';
        } else {
            $newpathname = '';
            $component = 'mod_resource';
        }

        $filerecord = array(
            'contextid' => $contextid,
            'component' => $component,
            'filearea'  => 'content',
            'itemid'    => 0,
            'filepath'  => '/'.$newpathname,
            'filename'  => $this->filename,
        );

        $fs = get_file_storage();
        $fs->create_file_from_pathname($filerecord, GABARIT_FILE_PATH.$this->pathname.$this->filename);

    }

    public static function get_all_files_in_module($folder = '', $files = []) {
        $directory = dir(GABARIT_FILE_PATH.$folder);

        while ( ($entry = $directory->read()) !== false) {
            if($entry == '.') {
                //nothing to do $files[] = new FileInModule($folder, '.');
            } else if ($entry == '..' ) {
                // nothing to do
            } else if (is_dir(GABARIT_FILE_PATH.$folder.$entry)) {
                $files = array_merge(self::get_all_files_in_module($folder.$entry.'/', $files));
            } else {
                $files[] = new FileInModule($folder, $entry);//array_push($files, );
            }
        }

        $directory->close();

        return $files;
    }


}

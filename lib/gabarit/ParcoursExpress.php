<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:17
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");

class ParcoursExpress extends GabaritParcours
{

    public function sectionNumber()
    {
        return 0;
    }

    public function makeContent($newcourse = null)
    {
        $this->newcourse = $newcourse;

        /**
         * Section 0
         */

        /** Module étiquette */
        $this->add_label_module(
            "<p></p><h3 style=\"text-align: center;\">&nbsp;titre du parcours à personnaliser</h3><p></p><br>
<table style=\"background:#F5F6CE\" width=\"100%\">
<tbody><tr><td style=\"text-align: center;\" width=\"25%\">
    <img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_classe.jpg\" 
        alt=\"\" width=\"150\" height=\"111\" role=\"presentation\" class=\"img-responsive atto_image_button_text-bottom\">
        <br>Logo à personnaliser</td>
<td><div class=\"editor-indent\" style=\"margin-left: 30px;\"><h4>
    <b>Bienvenue sur ce parcours</b>, ...</h4><h4><br></h4><p>Vous allez ...</p><p><br></p><p></p><p>Durée :&nbsp;<i>15 minutes</i><br></p></div></td>
</tr>
</tbody></table>

 <br><br>
<div style=\"width:30%;padding:10px;background-color:#D8F781;border-radius:50px;margin:0 auto;text-align:center;font-size:1.2em;\"><h4>
<b>Débutez le parcours !</b><br></h4></div><div><br></div><div><br></div><div><br></div>",
            0,
            true
        );

        /** Module dossier */
        $this->add_folder_module(
            "Présentation du parcours express",
            "<p><b>Élément invisible pour les élèves à destination des concepteurs de parcours.</b></p><p>Si vous souhaitez visualiser le parcours comme un élève, cliquez sur l'icône menu en haut à droite, puis choisissez \"Prendre le rôle de\" et \"Étudiant\". N'oubliez pas ensuite de revenir à votre rôle normal pour pouvoir modifier le parcours.</p><p><br></p><p>Ce parcours est organisé sur le modèle ci-dessous.</p><table ;=\"\" width=\"100%\" border=\"1\">
<tbody><tr>
<td width=\"15%\" style=\"text-align: center;\"><b>Section</b></td>
<td style=\"text-align: center;\"><b>Modalité</b>&nbsp;</td><td style=\"text-align: center;\"><b>Contenu</b></td>
<td width=\"35%\" style=\"text-align: center;\"><b>Paramètre</b></td>
</tr><tr>
<td rowspan=\"2\" style=\"text-align: center;\">Accueil/section 0<br></td>
<td rowspan=\"2\" style=\"text-align: center;\">&nbsp;Travail à distance ou accompagné</td><td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Étiquette\" visible avec texte d'accueil et bouton de lancement</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Dossier\" cachée (visible seulement des enseignants)<br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">&nbsp;Pas d'achèvement d'activité</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"></div></td></tr><tr>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Ressource \"Page\"<br>Activité \"QCM\"</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><div>Activité \"Devoir\"<br>Ressource \"Page\"</div><br></div></td>
<td><div style=\"margin-left: 30px;\" class=\"editor-indent\">Achèvement d'activité :&nbsp;</div><div style=\"margin-left: 30px;\" class=\"editor-indent\">- pour les ressources -&gt; la page doit être affichée<br></div><div style=\"margin-left: 30px;\" class=\"editor-indent\">- pour les activités -&gt; l'activité doit être terminée (pas de note de passage)</div><div style=\"margin-left: 30px;\" class=\"editor-indent\"><br></div></td></tr></tbody></table><p>Accès aux tutoriels pour concevoir les parcours : <a href=\"https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof\" target=\"_blank\">https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=prof</a>.</p><p>Pour comprendre comment fonctionne le bouton de lancement, vous pouvez regarder le tutoriel correspondant \"<a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=creer-un-bouton-de-lancement-dans-l-etiquette-d-accueil\" target=\"_blank\">Créer un bouton de lancement dans l'étiquette d'accueil</a>\".<br></p>",
            0,
            FileInModule::get_all_files_in_module(),
            true,
            false,
            true,
            true
        );

        $this->add_page_module(
            "<p><br></p>Ceci est une ressource Page où vous pouvez saisir un <b>texte</b> 
en le mettant en forme librement et insérer les <b>ressources</b> (visuels, vidéos, liens, etc.)<p><br></p>
<p>L'image ci-dessous peut-être supprimée et/ou remplacée par un autre fichier.</p>
<p style=\"text-align: center;\">
    <img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_puzzle1.png\" 
        alt=\"\" width=\"200\" height=\"148\" role=\"presentation\" class=\"img-responsive atto_image_button_center\"><br></p>
<p>N'hésitez pas à personnaliser l'<b>organisation de la page</b> (alignement, taille de la police) avec votre contenu 
pour la rendre plus engageante.</p><p><br></p><p>Vous pouvez aussi insérer des <b>vidéos</b> directement dans 
les \"Pages\" sur Éléa.</p>
<div style=\"text-align: center;\"><iframe src=\"//scolawebtv.crdp-versailles.fr/?iframe&amp;id=11570\" 
    allowfullscreen=\"\" width=\"570\" height=\"321\" frameborder=\"0\"></iframe></div><p><br></p>
<p>Pensez également à rajouter des consignes de navigation et à rappeler régulièrement les objectifs dans votre parcours.<br></p>
<p><br></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-ressources&amp;item=integrer-une-ressource-page\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette ressource</a>.</p><p><br></p>",
            "",
            "Débutez le parcours !",
            0
        );

        $this->add_qcm_module(
            "<p><br></p><p>Vous pouvez saisir ici les consignes pour l'activité QCM.</p>
<p><br></p><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-qcm\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.<br></p><p><br></p>",
            "Vérifiez vos connaissances...",
            0,
            false,
            [new QcmQuestion(
                "<p><br></p><p>Intitulé de la question 1</p>",
                ["Réponse 1 juste", "Réponse 2 fausse", "Réponse 3 fausse"],
                [true, false, false])],
            5,
            0
        );

        $this->add_assignment_module(
            "Qu'avez-vous appris aujourd'hui ?",
            "<p>Ceci est une activité Devoir. <br></p>
<p>Il s'agit d'une activité qui permet à un élève d'écrire un texte directement dans la plateforme ou bien de déposer un fichier.</p>
<p></p><div id=\"intro\"><div><p><a href=\"https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&amp;element=integrer-des-activites&amp;item=integrer-une-activite-devoir\" 
    target=\"_blank\">Consultez si besoin le tutoriel associé à cette activité</a>.</p></div></div><div><br></div><p></p>
    <p>Personnalisez la consigne à destination des élèves dans la description de l'activité.<br></p>",
            0,
            false,
            false
        );

        $this->add_page_module(
            "<p></p><p>Cette page termine le parcours et sert à indiquer à l'élève qu'il a réussi
 les différentes étapes. On peut aussi lui proposer un exercice 
d'approfondissement.&nbsp;</p>
<p>Voici un exemple à personnaliser.</p><p></p><div class=\"editor-indent\" style=\"margin-left: 30px;\"><div class=\"editor-indent\" style=\"margin-left: 30px;\"><h3><b><br>
</b></h3><h3><b><br></b></h3><h3><b><img src=\"/local/teacherboard/lib/gabarit/modfolder/Logos%20divers%20CC0/logo_applauding-41640_640.png\" 
    alt=\"\" width=\"150\" height=\"157\" role=\"presentation\" class=\"img-responsive atto_image_button_left\"></b></h3></div></div>
    <h3><b>Bravo !</b></h3><p><b>Vous avez terminé le parcours.</b><br></p><p><br></p><p>Maintenant, vous savez que ........<br></p><p><br></p>
    <p><br></p><p>&nbsp;Vous avez notamment travaillé les compétences ci-dessous :&nbsp;</p><div style=\"margin-left: 30px;\" class=\"editor-indent\">
    <div style=\"margin-left: 30px;\" class=\"editor-indent\"><ul><ul><ul><ul><li>...</li><li>...</li><li>...</li></ul></ul></ul></ul></div></div><br>
    <p><b>Vous allez pouvoir ...&nbsp;<br></b></p><br><p></p>",
            "",
            "Vous avez terminé le parcours",
            0
        );

   }
}
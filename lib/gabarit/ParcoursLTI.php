<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:17
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");
require_once($CFG->dirroot.'/mod/lti/locallib.php');

class ParcoursLTI extends GabaritParcours
{

    public function sectionNumber()
    {
        return 0;
    }

    public function makeContent($newcourse = null)
    {
        $this->newcourse = $newcourse;

        /** Section 0 */
        $this->add_lti_module(
            "Parcours Louise",
            "https://formation.elea.ac-versailles.fr/enrol/lti/cartridge.php/1/c0b0d39be3f9e1912c80a78afdb1bf4a/cartridge.xml",
            "Ak0RWbHNeDBiZDLVqJwxqvy9JHDo2Yba");

        /**
         * Finalisation
         */
    }
}
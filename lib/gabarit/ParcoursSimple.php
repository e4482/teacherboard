<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:32
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/GabaritParcours.php");

class ParcoursSimple extends GabaritParcours
{
    private $activitytype = 0;

    public function __construct($anActivitytype) {
        $this->activitytype = $anActivitytype;
    }

    public function sectionNumber() {
        return -1;
    }

    public function makeContent($newcourse = null) {
        return $this->activitytype;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/09/18
 * Time: 16:05
 */

global $CFG;
include_once($CFG->dirroot . "/local/teacherboard/lib/gabarit/ParcoursUtilities.php");
include_once($CFG->dirroot . "/mod/simpleqcm/lib.php");

use mod_simpleqcm\local\models\SimpleQcmActivity;



abstract class GabaritParcours
{

    /**
     * @var variable storing course
     */
    protected $newcourse;



    /**
     * @return the number of section to set at initialization, 0 means a unique activity
     */
    abstract public function sectionNumber();

    /**
     * Create the content of the course
     *
     * @return
     */
    abstract public function makeContent($newcourse = null);

    private function createStandardModuleInfo($title, $section_number, $last_cmid_for_completion = false, $showdescription = true) {
        $moduleinfo = new stdClass();
        $moduleinfo->name = $title;
        $moduleinfo->showdescription = $showdescription;
        $moduleinfo->course = $this->newcourse->id;
        $moduleinfo->section = $section_number;
        $moduleinfo->visible = true;
        $moduleinfo->cmidnumber = '';

        $moduleinfo->completion = COMPLETION_TRACKING_AUTOMATIC;
        $moduleinfo->completionview = COMPLETION_VIEW_REQUIRED;
        $moduleinfo->completiongradeitemnumber = 1;
        $moduleinfo->completionexpected = time() + (7 * 24 * 3600);

        // this module is visible if previous module is achieved
        if ($last_cmid_for_completion) {
            $moduleinfo->availability = '{"op":"&","c":[{"type":"completion","cm":' . $last_cmid_for_completion . ',"e":1}],"showc":[true]}';
        }

        return $moduleinfo;
    }

    protected function end_module_creation($moduleinfo) {
        list($module, $context, $cw) = can_add_moduleinfo($this->newcourse, $moduleinfo->modulename, $moduleinfo->section);
        $moduleinfo->module = $module->id;
        add_moduleinfo($moduleinfo, $this->newcourse, null);

        return $moduleinfo->coursemodule;
    }

    protected function add_badge($courseid, $coursename)
    {
        global $USER, $DB, $CFG;
        $fordb = new stdClass();
        $fordb->id = null;
        $now = time();
        $fordb->name = $coursename;
        $fordb->description = "Badge de validation de " . $coursename;
        $fordb->timecreated = $now;
        $fordb->timemodified = $now;
        $fordb->usercreated = $USER->id;
        $fordb->usermodified = $USER->id;
        $fordb->issuername = "ELEA";
        $fordb->issuerurl = "plateforme elea";
        $fordb->issuercontact = "noreply-dane@ac-versailles.fr";
        $fordb->expiredate = null;
        $fordb->expireperiod = null;
        $fordb->type = BADGE_TYPE_COURSE;
        $fordb->courseid = $courseid;
        $fordb->messagesubject = get_string('messagesubject', 'badges');
        $fordb->message = get_string('messagebody', 'badges',
            html_writer::link($CFG->wwwroot . '/badges/mybadges.php', get_string('managebadges', 'badges')));
        $fordb->attachment = 1;
        $fordb->notification = BADGE_MESSAGE_NEVER;
        $fordb->status = BADGE_STATUS_ACTIVE_LOCKED;

        $newid = $DB->insert_record('badge', $fordb, true);

        $tempbadge = tempnam('/tmp', 'UL_IMAGE');
        $badgenum = rand(1,16);
        $img = file_get_contents($CFG->dirroot . '/local/teacherboard/lib/template/images/badges/badge' . $badgenum. '.png');
        file_put_contents($tempbadge, $img);

        $newbadge = new badge($newid);
        badges_process_badge_image($newbadge, $tempbadge);


        /* GIGA HACKISH ! */
        /*$data = new \stdClass();
        $data->badgeid = $newid;
        $data->criteriatype = 1;
        $data->method = 2;
        $data->descriptionformat = 1;
        $critid = $DB->insert_record('badge_criteria', $data, true);

        $data = new \stdClass();
        $data->critid = $critid;
        $data->name = "module_" . $moduleid;
        $data->value = $moduleid;
        $DB->insert_record('badge_criteria_param', $data, true);*/

        /* END OF GIGA HACK */


    }

    protected function add_map_module($section_number, $theme_index)
    {
        global $DB, $CFG;

        $standardmaps = json_decode(file_get_contents($CFG->dirroot."/mod/mapmodules/res/standard_maps.json"));
        $foundmap = null;
        foreach ($standardmaps as $standardmap) {
            if ($standardmap->themeid === $theme_index) {
                $foundmap = $standardmap;
                break;
            }
        }

        $moduleinfo = $this->createStandardModuleInfo("Carte standard : {$foundmap->themename}", $section_number, false);
        $moduleinfo->modulename = "mapmodules";
        $moduleinfo->path = $foundmap->path;
        $moduleinfo->iconset = $foundmap->iconset;
        $moduleinfo->buttonwidth = $foundmap->buttonwidth;
        $moduleinfo->displaymodulenames = 0;
        $moduleinfo->descriptionheadereditor = array('text' => '', 'format' => FORMAT_HTML, 'itemid' => 0);
        $moduleinfo->descriptionfootereditor = array('text' => '', 'format' => FORMAT_HTML, 'itemid' => 0);

        $firstsection = $DB->get_record_sql("
            SELECT id, section
            FROM {course_sections}
            WHERE course = :courseid
            AND section = '1'
        ",
            ['courseid' => $this->newcourse->id]);
        $moduleinfo->targetsection = $firstsection->section;

        return $this->end_module_creation($moduleinfo);
    }

    protected function add_page_module($content, $description, $title, $section_number, $previouscmid = false, $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo($title, $section_number, $previouscmid, $showdescription);
        $moduleinfo->modulename = "page";
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);
        $moduleinfo->content = $content;
        $moduleinfo->contentformat = FORMAT_HTML;
        $moduleinfo->display = RESOURCELIB_DISPLAY_OPEN;
        $moduleinfo->printheading = 1;
        $moduleinfo->printintro = 0;

        return $this->end_module_creation($moduleinfo);
    }

    protected function add_final_page_module($content, $description, $title, $section_number, $previouscmid, $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo($title, $section_number, $previouscmid, $showdescription);
        $moduleinfo->modulename = "page";
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);
        $moduleinfo->content = $content;
        $moduleinfo->contentformat = FORMAT_HTML;

        return $this->end_module_creation($moduleinfo);
    }

    protected function add_qcm_module($description, $title, $section_number, $previouscmid, $qcm, $nbpoints, $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo($title, $section_number, $previouscmid, $showdescription);
        $moduleinfo->modulename = "simpleqcm";
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);

        $moduleinfo->nbquestions = count($qcm);
        for ($iQ = 0; $iQ < $moduleinfo->nbquestions; $iQ++) {
            $question = $qcm[$iQ];

            $questionnamefield = SimpleQcmActivity::question_name_field($iQ);
            $questionidfield = SimpleQcmActivity::question_id_field($iQ);
            $questiontextfield = SimpleQcmActivity::question_text_field($iQ);
            $questiontextformatfield = SimpleQcmActivity::question_text_format_field($iQ);
            $questiontextitemidfield = SimpleQcmActivity::question_text_item_id_field($iQ);
            $moduleinfo->$questionidfield = 0;
            $moduleinfo->$questionnamefield = $question->question;
            $moduleinfo->$questiontextfield = $question->question;
            $moduleinfo->$questiontextformatfield = FORMAT_HTML;
            $moduleinfo->$questiontextitemidfield = null;

            $answers = $question->reponses;
            $answerscorrectness = $question->validitesrep;
            for ($iA = 0; $iA < count($answers); $iA++) {
                $answeridfield = SimpleQcmActivity::answer_id_field($iQ, $iA);
                $answertextfield = SimpleQcmActivity::answer_text_field($iQ, $iA);
                $answertextformatfield = SimpleQcmActivity::answer_text_format_field($iQ, $iA);
                $answertextitemidfield = SimpleQcmActivity::answer_text_item_id_field($iQ, $iA);
                $answeriscorrectfield = SimpleQcmActivity::answer_is_correct_field($iQ, $iA);
                $moduleinfo->$answeridfield = 0;
                $moduleinfo->$answertextfield = $answers[$iA];
                $moduleinfo->$answertextformatfield = FORMAT_HTML;
                $moduleinfo->$answertextitemidfield = 0;
                $moduleinfo->$answeriscorrectfield = $answerscorrectness[$iA];
            }
        }

        $moduleinfo->questionids = '';
        $moduleinfo->completionusegrade = 1;
        $moduleinfo->grade = $nbpoints;
        $moduleinfo->grademin = 0;

        return $this->end_module_creation($moduleinfo);
    }

    protected function add_epikmatching_module($description, $title, $section_number, $previouscmid, $couples = [], $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo($title, $section_number, $previouscmid, $showdescription);
        $moduleinfo->modulename = "epikmatching";
        $moduleinfo->grade = 20;
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);

        $MAX_FIELDS = 9;
        $nbPaire = count($couples)/2;
        for ($i = 1; $i <= $MAX_FIELDS; $i++) {
            $fieldName1 = 'pair1_' . $i . '_editor';
            $fieldName2 = 'pair2_' . $i . '_editor';

            if($i<=$nbPaire) {
                $moduleinfo->$fieldName1 = $couples[($i-1)*2]->format();
                $moduleinfo->$fieldName2 = $couples[($i-1)*2 + 1]->format();
            } else {
                $moduleinfo->$fieldName1 = array('text' => "", 'format' => '', 'itemid' => null);
                $moduleinfo->$fieldName2 = array('text' => "", 'format' => '', 'itemid' => null);
            }
        }


        // Completion common to all module.
        $moduleinfo->completionusegrade = 1;

        return $this->end_module_creation($moduleinfo);
    }


    protected function add_millionnaire_module($description, $title, $section_number, $previouscmid, $questions, $nbpoints, $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo($title, $section_number, $previouscmid, $showdescription);
        $moduleinfo->modulename = "millionnaire";
        $moduleinfo->grade = $nbpoints;
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);

        $nbquestions = count($questions);

        for ($i=1;$i<=6;$i++) {

            $question = 'question_' . $i;
            $answer1 = 'goodanswer_' . $i;
            $answer2 = 'badanswer1_' . $i;
            $answer3 = 'badanswer2_' . $i;
            $answer4 = 'badanswer3_' . $i;

            if($i<=$nbquestions) {
                $moduleinfo->$question = $questions[$i-1]->question;
                $moduleinfo->$answer1 = $questions[$i-1]->reponses[0];
                $moduleinfo->$answer2 = $questions[$i-1]->reponses[1];
                $moduleinfo->$answer3 = $questions[$i-1]->reponses[2];
                $moduleinfo->$answer4 = $questions[$i-1]->reponses[3];
            } else {
                $moduleinfo->$question = "";
                $moduleinfo->$answer1 = "";
                $moduleinfo->$answer2 = "";
                $moduleinfo->$answer3 = "";
                $moduleinfo->$answer4 = "";
            }

        }

        $moduleinfo->completionusegrade = 1;

        return $this->end_module_creation($moduleinfo);
    }


    protected function add_label_module($content, $section_number, $visible, $showdescription = true)
    {
        $moduleinfo = $this->createStandardModuleInfo("", $section_number, false, $showdescription);
        $moduleinfo->modulename = "label";
        $moduleinfo->visible = $visible;
        $moduleinfo->introeditor = array('text' => $content, 'format' => FORMAT_HTML, 'itemid' => 0);

        return $this->end_module_creation($moduleinfo);
    }

    protected function add_folder_module($name, $description, $section_number, $files, $display, $showexpanded, $showdownload, $showdescription = true)
    {
        global $CFG;
        global $DB;
        require_once($CFG->libdir . '/filelib.php');

        $moduleinfo = $this->createStandardModuleInfo($name, $section_number, false, $showdescription);
        $moduleinfo->modulename = "folder";
        $moduleinfo->introeditor = array('text' => $description, 'format' => FORMAT_HTML, 'itemid' => 0);
        $moduleinfo->completion = COMPLETION_TRACKING_NONE;

        $moduleinfo->display = $display;
        $moduleinfo->showexpanded = $showexpanded;
        $moduleinfo->showdownloadfolder = $showdownload;

        $moduleinfo->files = 0;

        $moduleinfo->visible = 0;

        $cmid = $this->end_module_creation($moduleinfo);

        // Write files in DB
        $contextid = $DB->get_record('context', ['contextlevel' => CONTEXT_MODULE, 'instanceid' => $cmid], 'id')->id;
        foreach ($files as $file) {
            $file->record_in_db ($contextid);
        }

        return $cmid;
    }

    protected function add_url_module($title, $url, $sectionnb, $desc, $prevcmid = false, $showdescription = true) {
        $moduleinfo = $this->createStandardModuleInfo($title, $sectionnb, $prevcmid, $showdescription);
        $moduleinfo->modulename = "url";
        $moduleinfo->externalurl = $url;
        $moduleinfo->introeditor = array('text' => $desc, 'format' => FORMAT_HTML, 'itemid' => 0);

        $moduleinfo->display = RESOURCELIB_DISPLAY_EMBED;
        $moduleinfo->popupwidth = 620;
        $moduleinfo->popupheight = 450;
        $moduleinfo->printintro = 1;


        return $this->end_module_creation($moduleinfo);
    }

    protected function add_assignment_module($title, $desc, $sectionnb, $prevcmid = false, $showdescription = true) {
        $moduleinfo = $this->createStandardModuleInfo($title, $sectionnb, $prevcmid, $showdescription);
        $moduleinfo->introeditor = array('text' => $desc, 'format' => FORMAT_HTML, 'itemid' => 0);
        $moduleinfo->modulename = "assign";

        $moduleinfo->alwaysshowdescription = 1;
        $moduleinfo->nosubmissions = 0;
        $moduleinfo->submissiondrafts = 0;
        $moduleinfo->sendnotifications = 0;
        $moduleinfo->sendlatenotifications = 0;
        $moduleinfo->duedate = time() + 7 * 24 * 3600;
        $moduleinfo->allowsubmissionsfromdate = time();
        $moduleinfo->grade = 10;
        $moduleinfo->requiresubmissionstatement = 0;
        $moduleinfo->completionsubmit = 1;
        $moduleinfo->cutoffdate = 0;
        $moduleinfo->gradingduedate = 0;
        $moduleinfo->teamsubmission = 0;
        $moduleinfo->requireallteammemberssubmit = 0;
        $moduleinfo->blindmarking = 0;
        $moduleinfo->markingworkflow = 0;
        $moduleinfo->markingallocation = 0;


        return $this->end_module_creation($moduleinfo);
    }

    protected function add_file_module($title, $desc, $sectionnb, $file, $display, $previouscmid = false, $showdescription = true) {
        global $DB;

        $moduleinfo = $this->createStandardModuleInfo($title, $sectionnb, $previouscmid, $showdescription);
        $moduleinfo->modulename = 'resource';
        $moduleinfo->introeditor = array('text' => $desc, 'format' => FORMAT_HTML, 'itemid' => 0);

        $moduleinfo->display = $display;
        $moduleinfo->popupwidth = 620;
        $moduleinfo->popupheight = 450;
        $moduleinfo->printintro = 1;
        $moduleinfo->files = 0;

        $cmid = $this->end_module_creation($moduleinfo);

        // Write files in DB
        $contextid = $DB->get_record('context', ['contextlevel' => CONTEXT_MODULE, 'instanceid' => $cmid], 'id')->id;
        $file->record_in_db ($contextid, false);

        return $cmid;
    }

    protected function add_lti_module($title, $url, $secret) {
        $moduleinfo = $this->createStandardModuleInfo($title, 0);
        $moduleinfo->modulename = 'lti';
        $moduleinfo->typeid = 0;
        $moduleinfo->toolurl = $url;
        $moduleinfo->securetoolurl = "";
        $moduleinfo->password = $secret;
        $moduleinfo->launchcontainer = LTI_LAUNCH_CONTAINER_WINDOW;
        $moduleinfo->resourcekey = strval(microtime());

        return $this->end_module_creation($moduleinfo);
    }
}
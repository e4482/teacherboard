<?php
namespace teacherboard;
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


global $CFG;
include_once($CFG->libdir . '/completionlib.php');
include_once($CFG->libdir . '/coursecatlib.php');
include_once($CFG->libdir . '/moodlelib.php');
include_once($CFG->libdir . '/enrollib.php');
include_once($CFG->dirroot . '/cohort/lib.php');
include_once($CFG->dirroot . '/enrol/cohort/lib.php');
include_once($CFG->dirroot . '/course/externallib.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/view/view.class.php');

/**
 * Class used to manage board
 *
 * @package    teacherboard
 * @subpackage model
 * @copyright  2016 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Cohort {

    function __construct($cohortid) {
        global $DB;
        $this->id = $cohortid;
        $cohort = $DB->get_record('cohort', array('id' => $this->id));
        if ($cohort) {
          $this->contextid = $cohort->contextid;
          $this->name = $cohort->name;
        }
        else {
          $this->contextid = null;
          $this->name = null;
        }

    }

    /*
     *
     */

    public function getUsers() {

     global $DB;
     $usersList = $DB->get_records_sql("
      SELECT u.id, u.firstname, u.lastname
      FROM {user} AS u, {cohort_members} as cm
      WHERE cm.userid = u.id
      AND cm.cohortid = '".$this->id."'
      ORDER BY u.lastname ASC");

     return $usersList;
    }
   /*
    *
    */

    public function getParentCategory() {

      global $DB;
      $category = null;
      $context = $DB->get_record('context', array('id' => $this->contextid));
      if ($context->contextlevel == 40) {
          $category = $DB->get_record('course_categories', array('id' => $context->instanceid));
      }
      return $category;
    }
}

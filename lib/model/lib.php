<?php
namespace teacherboard;
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


global $CFG;

include_once($CFG->libdir . '/completionlib.php');
include_once($CFG->libdir . '/coursecatlib.php');
include_once($CFG->libdir . '/moodlelib.php');
include_once($CFG->libdir . '/enrollib.php');
include_once($CFG->dirroot . '/cohort/lib.php');
include_once($CFG->dirroot . '/enrol/cohort/lib.php');
include_once($CFG->dirroot . '/course/externallib.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/classes/role.class.php');

/**
 * Class used to manage board
 *
 * @package    teacherboard
 * @subpackage model
 * @copyright  2016 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class User {

    // user id
    private $_id = 0;

    private $_rneid = Array();

    // list of schools RNE where this teacher is working in
    // it is used as idnumber of courses categories
    // example :
    // $_rne = array('0921241z', '0784513a')
    private $_rne = Array();

    // list of courses where current user is enrolled
    // courses have been selected from categories where user is allowed to work
    private $_courses = Array();


    // COURSE ARRAY

    private $_arrayCourses = Array(
        Array(
            'head' => '<th></th>',
            'body' => "<td class='dd-handle2'>
                <img style='height:20px;' src='{{MOVE}}' alt='{{STRMOVECOURSE}}' />
                </td>"

        ),
        Array(
            'head' => '<th></th>',
            'body' => "<td class='cell'>
                {{BUTTON_SHOWHIDECOURSE}}
                {{BUTTON_COURSEPARAMS}}
                {{BUTTON_USERSENROL}}
                {{BUTTON_TEACHERENROL}}
                {{BUTTON_METACOURSE}}
                {{BUTTON_USERSLIST}}
                {{BUTTON_DUPLICATECOURSE}}
                {{BUTTON_SHARECOURSE}}
                {{BUTTON_BADGES}}</td>"
        ),
        Array(
            'head' => '<th></th>',
            'body' => "<td class='cell object_name'>
                {{FIELD_COURSENAME}}
                {{BUTTON_COURSENAMEEDIT}}
                <a href='{{COURSELINK}}'>
                <!-- <img style='height:20px;padding-left:10px;'
                class=''
                src='{{MOVEIN}}' alt='' title='' />--></a></td>"
        ),
        Array(
            'head' => '<th></th>',
            'body' => "<td class='cell'><a href='{{ACHIEVEMENT}}'>
                <img style='height:20px;' data-itemid='{{ITEMID}}'
                class='achievement' src='{{STATS}}' alt='{{ACHIEVEMENT_LABEL}}' title='{{ACHIEVEMENT_LABEL}}' />
                </a>
                </td>"
        ),
        Array(
            'head' => '<th></th>',
            'body' => "<td class='cell'>{{BUTTON_COURSEDELETE}}</td>"
        )
    );

    // fields used in user profil attributes
    private $_categShortnames = Array(
        'rne1',
        'rne2',
        'rne3',
        'rne4',
        'rne5');

    private $_sessionselectedfolder = 'teacherboard_selectedfolder';
    private $folderid_prefix = "page_";

    function __construct() {
        global $DB, $USER;
        $this->_view = new \View();
        $this->_id = $USER->id;
        $this->_rneid = $this->getCategoriesFromShortnames($this->_categShortnames);
        $this->_rne = $this->getUserCategIdNumber($this->_rneid);
        $this->_courses = $this->getCoursesFromCategories($this->_rne);

    }

    /*
     * check if current user has groups
     *
     */

    public function hasGroups() {
        global $DB;
        return $DB->count_records('teacherboard_group', array('userid' => $this->_id));
    }
  /*
   * check if current user has groups
   *
   */

    public function getGroups() {
      global $DB;

      return $DB->get_records('teacherboard_group', array('userid' => $this->_id));
    }
    /*
     * check if RNE is defined for the current user
     *
     */

    public function hasRne() {

    if (count($this->_rne)) {
        return true;
    }
    return false;

    }
    
    /*
     * get user's first RNE
     * 
     */
    public function getFirstRne() {

    if (count($this->_rne)) {
        return $this->_rne[0];
    }
    return false;

    }

    /*
     * get timeRetrieved
     *
     */
    public function getTimeRetrieved() {

    global $DB;
    $tr_field = $DB->get_record('user_info_field', array('shortname' => 'timeretrieved'), 'id');
    if($tr_field !== false){
        $tr_field_id = $tr_field->id;
        $content = $DB->get_record('user_info_data',
            array('userid' => $this->_id, 'fieldid' => $tr_field_id), 'data');
        if($content){
            return $content->data;
        }
    }
    return false;

    }

    /*
     * is user from last school year?
     *
     */
    public function isOldUser(){
        if (date("m") === "08" || date("m") === "09") {
            return false;
        }
        $year = date("Y");
        //August 25th, 8AM of current year elea/platform#353
        $last_year_end = strtotime("08/25/".$year) + 8*3600;
        return time() > $last_year_end && $this->getTimeRetrieved() && $this->getTimeRetrieved() < $last_year_end;
    }

    /*
     * store folderid in _SESSION
     *
     */

    public function storeSelectedFolder($folderid) {
        $_SESSION[$this->_sessionselectedfolder] = $folderid;
    }

    /*
     * get folderid from _SESSION
     *
     */

    public function getSelectedFolder() {
        if (array_key_exists($this->_sessionselectedfolder, $_SESSION)) {
            return $this->folderid_prefix . $_SESSION[$this->_sessionselectedfolder];
        }
        return null;
    }

    /*
     * get course modules
     *
     */
     public function getCourseModules($courseid) {

        global $DB;

        $modulesid = array();
        $sections = array();
        $sectionslist = array_values(
            $DB->get_records(
                'course_sections',
                array('course' => $courseid)
            )
        );

        for ($i = 0 ; $i < count($sectionslist) ; $i++) {
            $sections[$i] = array();
            $sectionmodulesid = explode(
                ",",
                $sectionslist[$i]->sequence
            );
            foreach ($sectionmodulesid as $moduleid) {
                $currentmodule = $DB->get_record_sql("
                    SELECT cm.*, m.name AS name
                    FROM {course_modules} AS cm, {modules} AS m
                    WHERE cm.id = '". $moduleid ."'
                    AND m.id = cm.module"
                );
                if ($currentmodule) {
                    $currentinstance = $DB->get_record_sql("
                        SELECT   i.id AS id,
                                i.name AS name
                        FROM {". $currentmodule->name ."} AS i
                        WHERE i.id = '". $currentmodule->instance ."'
                    ");
                    array_push(
                        $sections[$i],
                        array(
                            'cm' => $currentmodule,
                            'name' => $currentinstance->name
                        )
                    );
                }
            }
        }
        $course = $DB->get_record("course", array('id' => $courseid));
        $completion = new \completion_info($course);
        $completion_enabled = false;
        if ($completion->is_enabled_for_site() && $completion->is_enabled()) {
            $completion_enabled = true;
        }
        foreach ($sections as &$modules) {
            foreach($modules as &$module) {
                if ($completion_enabled && $completion->is_enabled($module['cm'])) {
                    $module['completion'] = true;
                }
                else {
                    $module['completion'] = false;
                }
            }
        }
        return $sections;
    }

    /*
     * get html representation of courses
     *
     */

    public function getHtmlCourse($item) {

        global $CFG, $DB, $USER;
        $result = "";
        $template = "";
        $context = \context_course::instance($item['course']->id);

        foreach($this->_arrayCourses as $arrayEntry) {
            $template .= $arrayEntry['body'];
        }
        $currentRow = $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/courseRow.php");
        $currentRow = str_replace("{{COURSEROW}}", $template, $currentRow);

        // include buttons according to user permissions
        if (has_capability('moodle/course:visibility', $context)) {
            $currentRow = str_replace("{{BUTTON_SHOWHIDECOURSE}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonShowHideCourse.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_SHOWHIDECOURSE}}",
                    "", $currentRow);
        }

        // include buttons according to user permissions
        if (has_capability('moodle/course:update', $context)) {
            $currentRow = str_replace("{{BUTTON_COURSEPARAMS}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonCourseParams.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_COURSEPARAMS}}",
                    "", $currentRow);
        }

        // include buttons according to user permissions

        if (has_capability(PERMISSION_USERS_COURSE, $context)) {
            $currentRow = str_replace("{{BUTTON_METACOURSE}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonMetacourse.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_METACOURSE}}", "", $currentRow);
        }

        if (has_capability(PERMISSION_USERS_COURSE, $context)) {
            $currentRow = str_replace("{{BUTTON_USERSENROL}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                    "/local/teacherboard/lib/template/buttonUsersEnrol.php")
                , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_USERSENROL}}", "", $currentRow);
        }

        if (has_capability(PERMISSION_ENROL_TEACHERS, $context)) {
            $currentRow = str_replace("{{BUTTON_TEACHERENROL}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonTeacherEnrol.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_TEACHERENROL}}", "", $currentRow);
        }

        if (has_capability(PERMISSION_USERS_COURSE, $context)) {
            $currentRow = str_replace("{{BUTTON_USERSLIST}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonUsersList.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_USERSLIST}}", "", $currentRow);
        }


        // include buttons according to user permissions
        // duplication disabled because actually unstable
        if (false) {
        //if ($this->hasRNE()) {
            $currentRow = str_replace("{{BUTTON_DUPLICATECOURSE}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonDuplicateCourse.php")
                    , $currentRow);
            //$currentRow = str_replace("{{BUTTON_SHARECOURSE}}",
            //    $this->_buttonShareCourse, $currentRow);
            $currentRow = str_replace("{{BUTTON_SHARECOURSE}}",
                    "", $currentRow);

        }
        else {
            $currentRow = str_replace("{{BUTTON_DUPLICATECOURSE}}",
                    "", $currentRow);
            $currentRow = str_replace("{{BUTTON_SHARECOURSE}}",
                    "", $currentRow);
        }

        if (has_capability(PERMISSION_MANAGE_BADGES, $context)) {
            $currentRow = str_replace("{{BUTTON_BADGES}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonBadges.php")
                    , $currentRow);
        }
        else {
            $currentRow = str_replace("{{BUTTON_BADGES}}", "", $currentRow);
        }

        // include buttons according to user permissions
        /*if (has_capability(PERMISSION_CHANGENAME_COURSE, $context)) {
            $currentRow = str_replace("{{FIELD_COURSENAME}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/fieldCourseName.php")
                    , $currentRow);
            $currentRow = str_replace("{{BUTTON_COURSENAMEEDIT}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonCourseNameEdit.php")
                    , $currentRow);
        }*/
        //else {
            $currentRow = str_replace("{{FIELD_COURSENAME}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/fieldCourseNameLocked.php")
                    , $currentRow);
            $currentRow = str_replace("{{BUTTON_COURSENAMEEDIT}}",
                "", $currentRow);
        //}

        // include buttons according to user permissions
        if (has_capability('local/teacherboard:deletecourse', $context)) {
            $currentRow = str_replace("{{BUTTON_COURSEDELETE}}",
                $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonCourseDelete.php")
                    , $currentRow);
        }
        else {
            //if profile is not student, user may remove his enrollment into course
            $user_field_profil = $DB->get_record('user_info_field', [
                'shortname' => "profil"
            ], 'id', MUST_EXIST)->id;
            $user_profil = $DB->get_record('user_info_data', [
                'fieldid' => $user_field_profil,
                'userid'  => $USER->id
            ], 'data', IGNORE_MISSING);
            $enrol = $DB->get_record_sql(
                'SELECT e.* FROM mdl_enrol e
                     INNER JOIN mdl_user_enrolments ue
                     ON ue.enrolid = e.id
                     WHERE ue.userid = :userid
                     AND e.enrol IN ("manual", "self")
                     AND courseid = :courseid',
                ['userid' => $USER->id, 'courseid' => $item['course']->id],
            IGNORE_MISSING);
            if($user_profil && $user_profil->data === 'teacher' && $enrol){
                $currentRow = str_replace("{{BUTTON_COURSEDELETE}}",
                    $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/buttonCourseUnEnrol.php")
                    , $currentRow);
            } else {
                //else there is no way to remove course
                $currentRow = str_replace("{{BUTTON_COURSEDELETE}}",
                    "", $currentRow);
            }
        }

        // include attributes
        $currentRow = str_replace("{{ITEMID}}",
                $item['folderitem']->id, $currentRow);
        $currentRow = str_replace("{{COURSEID}}",
                $item['course']->id, $currentRow);
        $currentRow = str_replace("{{MOVE}}", MOVE, $currentRow);
        $currentRow = str_replace("{{DELETE}}", DELETE, $currentRow);
        $currentRow = str_replace("{{UNENROL}}", UNENROL, $currentRow);
        $currentRow = str_replace("{{STRDELETECOURSE}}",
                get_string("course:delete", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{STRUNENROLCOURSE}}",
            get_string("course:unenrol", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{EDIT}}", EDIT, $currentRow);
        $currentRow = str_replace("{{STREDITCOURSENAME}}",
                get_string("course:editname", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{MOVEIN}}", MOVEIN, $currentRow);
        $currentRow = str_replace("{{COURSENAME}}",
                $item['course']->fullname, $currentRow);
        /*$section1 = $DB->get_record_sql("
            SELECT id
            FROM {course_sections}
            WHERE course = ".$item['course']->id."
            AND section = 1
            ");
        if (!$section1) {
            $data = new \stdClass();
            $data->course = $item['course']->id;
            $data->section = 1;
            $data->summaryformat = 1;
            $data->visible = 1;
            $DB->insert_record("course_sections", $data);
        }*/
        /*$currentRow = str_replace("{{COURSELINK}}", $CFG->wwwroot
                . '/course/view.php?section=1&id=' . $item['course']->id, $currentRow);*/

        // Check if course is only a link (unique LTI activity) and role is not an editor
        $islink = false;
        $modules = $DB->get_records('course_modules', ['course' => $item['course']->id]);
        if (count($modules) === 1) {
            $modules = array_values($modules);
            $ltimoduleid = $DB->get_record('modules', ['name' => 'lti'], 'id', MUST_EXIST)->id;

            if ($modules[0]->module == $ltimoduleid) {
                $context = \context_course::instance($item['course']->id);
                $roles = $DB->get_records_list('role','shortname', ['coursecreator','editingteacher'],'','id');
                //If user is an editingteacher or coursecreator, normal display is left for edition
                foreach($roles as $role) {
                    $roleassignment = $DB->get_record('role_assignments',
                        array(
                            'roleid' => $role->id,
                            'contextid' => $context->id,
                            'userid' => $this->_id
                        )
                    );
                    $islink = !$roleassignment;
                }
            }

        }

        if ($islink) {
            $currentRow = str_replace("{{COURSELINK}}", $CFG->wwwroot
                . '/mod/lti/view.php?id=' . $modules[0]->id, $currentRow);
        } else {
            $currentRow = str_replace("{{COURSELINK}}", $CFG->wwwroot
                . '/course/view.php?id=' . $item['course']->id, $currentRow);
        }

        $currentRow = str_replace("{{ACHIEVEMENT}}", $CFG->wwwroot
                . '/local/teacherboard/index.php?action=completion&id='
                . $item['course']->id, $currentRow);

        $currentRow = str_replace("{{ACHIEVEMENT_LABEL}}"
                , get_string("achievement:label", "local_teacherboard")
                , $currentRow);

        $currentRow = str_replace("{{USERSENROL}}", $CFG->wwwroot
                . '/user/index.php?id=' . $item['course']->id, $currentRow);
        $currentRow = str_replace("{{COURSEPARAMS}}", $CFG->wwwroot
                . '/course/edit.php?id=' . $item['course']->id, $currentRow);
        $currentRow = str_replace("{{PARAMS}}", PARAMS, $currentRow);
        $currentRow = str_replace("{{STRCOURSEPARAMS}}",
                get_string("course:params", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{STATS}}", STATS, $currentRow);
        $currentRow = str_replace("{{DUPLICATE}}", DUPLICATE, $currentRow);
        $currentRow = str_replace("{{STRDUPLICATE}}",
                get_string("course:duplicate", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{BADGES}}", BADGES, $currentRow);
        $currentRow = str_replace("{{STRBADGES}}",
                get_string("course:badges", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{EDITBADGES}}", $CFG->wwwroot
                . '/local/teacherboard/index.php?action=badges&id='
                . $item['course']->id, $currentRow);


        $currentRow = str_replace("{{ACLS}}", ACLS, $currentRow);
        $currentRow = str_replace("{{STRACLS}}",
                get_string("course:enrolledusers", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{SHARE}}", SHARE, $currentRow);
        $currentRow = str_replace("{{USERS}}", USERS, $currentRow);
        $currentRow = str_replace("{{METACOURSELINK}}", METACOURSELINK, $currentRow);
        $currentRow = str_replace("{{STRMETACOURSE}}",
            get_string("course:enrolmetacourse", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{GROUPS}}", GROUPS, $currentRow);
        $currentRow = str_replace("{{STRGROUPS}}",
                get_string("course:enrolcohorts", "local_teacherboard"), $currentRow);
        $currentRow = str_replace("{{TEACHER}}", TEACHER, $currentRow);
        $currentRow = str_replace("{{STRTEACHER}}",
                get_string("course:enrolteacher", "local_teacherboard"), $currentRow);
        if ($item['course']->visible) {
            $currentRow = str_replace("{{EYE}}", EYE_OPENED, $currentRow);
            $currentRow = str_replace("{{HIDDEN}}", "", $currentRow);
        }
        else {
            $currentRow = str_replace("{{EYE}}", EYE_CLOSED, $currentRow);
            $currentRow = str_replace("{{HIDDEN}}", "hiddencourse", $currentRow);
        }
        $currentRow = str_replace("{{STREYE}}",
                get_string("course:showhide", "local_teacherboard"), $currentRow);
        $result .= $currentRow;
        return $result;
    }

    public function getNbColumns() {
        return count($this->_arrayCourses);
    }

    public function get($name) {
        return $this->$name;
    }
    /*
     *
     *
     */
    public function dumpCourses() {
        echo '<pre>';
        ob_start();
        var_dump($this->_courses);
        $a=ob_get_contents();
        ob_end_clean();
        echo htmlspecialchars($a,ENT_QUOTES);
        echo '</pre>';
    }

    /*
     * get the id of profil field $name
     *
     */
    public function getCategoriesFromShortnames($shortnameArray) {
        global $DB;
        $categoriesList = Array();
        foreach ($shortnameArray as $shortname) {
            if ($DB->record_exists('user_info_field',
                    array('shortname' => $shortname))) {
                $rne = $DB->get_record('user_info_field',
                        array('shortname' => $shortname));
                array_push($categoriesList, $rne->id);
            }
        }
        return $categoriesList;
    }
    /*
     * get the data field of profil field
     */
    public function getUserCategIdNumber($categoriesList) {
        global $DB, $USER;
        $categoriesIdsNumber = Array();
        foreach ($categoriesList as $id) {
            if ($DB->record_exists('user_info_data',
                    array('userid' => $USER->id, 'fieldid' => $id))) {
                $rne = $DB->get_record('user_info_data',
                        array('userid' => $USER->id, 'fieldid' => $id));
                if ($rne->data != "") {
                    array_push($categoriesIdsNumber, strtolower($rne->data));
                }
            }
        }
        return $categoriesIdsNumber;
    }
    /*
     * get list of courses where the current user is enrolled
     */
    public function getCoursesFromCategories($categArray) {
        global $USER, $DB;
        $finalCoursesList = Array();
        foreach ($categArray as $categIdNumber) {
            $coursesArray = $DB->get_records_sql(
                "SELECT c.* "
                    . "FROM {course} as c, {course_categories} as cc "
                    . "WHERE c.category = cc.id "
                    . "AND cc.idnumber = '$categIdNumber'"
            );
            foreach ($coursesArray as $course) {
                if ($course) {
                    $context = \context_course::instance($course->id);
                    if (is_enrolled($context, $USER)) {
                        array_push($finalCoursesList, $course);
                    }
                }
            }
        }
        return $finalCoursesList;
    }

    /*
     * get first category where user is supposed to create new courses
     */
    public function getDefaultCategory() {
        global $DB;
        if (is_array($this->_rne) && array_key_exists(0, $this->_rne)) {
            $categIdNumber = $this->_rne[0];
        }
        else {
            $categIdNumber = NULL;
        }
        if ($categIdNumber) {
            $category = $DB->get_record('course_categories',
                    array('idnumber' => $categIdNumber));
            if (!$category) {
                $data = new \stdClass();
                $data->idnumber = $categIdNumber;
                $data->name = $categIdNumber;
                \coursecat::create($data);
                $category = $DB->get_record('course_categories',
                        array('idnumber' => $categIdNumber));
            }
            return $category;
        }
        else {
            return NULL;
        }
    }
    /*
     *  get pages from current user
     *  and chain them like this : (next, previous, child and parent)
     *  first page
     *   |
     *   |- child page1 -> child page2
     *   |
     *   second page
     *   |
     *   |- child page 1
     *       |
     *       |- child page 1 -> child page 2
     */
    public function getChainedPages() {
        global $DB;
        $tab = array();
        $pages = $DB->get_records_sql(
                "SELECT * "
                . "FROM {teacherboard_page} "
                . "WHERE userid = " . $this->_id . " "
                . "ORDER BY parentid, sortorder ASC");
        foreach ($pages as $page) {
                $tab[$page->id] = $page;
        }
        $this->generateChain($tab);
        return $tab;
    }
    /*
     *  recursive routine to generate chained pages
     *  used by getChainedPages
     */
    public function generateChain(&$t) {
        $frere = array();$enfant = array();
        while (current($t)) {
            $i = key($t);
            $parent = $t[$i]->parentid;
            if ($parent === NULL) $parent = 0;
            if (isset($frere[$parent])) {
                $t[$i]->previous = $frere[$parent];
                $t[$frere[$parent]]->next = $i;
                $frere[$parent] = $i;
            }
            else {
                    $frere[$parent] = $i;
                    if ((!isset($enfant[$parent])) && ($parent !=0)) {
                        $enfant[$parent] = 'done';
                        $t[$parent]->child = $i;
                    }
            }
            next($t);
        }
    }
    /*
     * html representation of one FOLDER
     */
    public function folderHtml($id, $name) {
        global $CFG;
        $line = $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/folderRow.php");


        // include buttons according to user permissions
        if (count($this->_rne)) {
            $line = str_replace("{{BUTTON_ADDCOURSE}}",
                    $this->folderAddCourseButton($id), $line);
        }
        else {
            $line = str_replace("{{BUTTON_ADDCOURSE}}", "", $line);
        }

        // include needed attributes
        $line = str_replace("{{STRADDCOURSE}}",
            get_string("addcourseicon", "local_teacherboard"), $line);
        $line = str_replace("{{FOLDERID}}", $id, $line);
        $line = str_replace("{{FOLDERID_PREFIX}}", $this->folderid_prefix, $line);
        $line = str_replace("{{FOLDERNAME}}", $name, $line);

        $line = str_replace("{{MOVE}}", MOVE, $line);
        $line = str_replace("{{MOVEFOLDER}}",
            get_string("folder:move", "local_teacherboard"), $line);
        $line = str_replace("{{SHOWFOLDERCOURSES}}",
            get_string("folder:showcourses", "local_teacherboard"), $line);
        $line = str_replace("{{FOLDER}}", FOLDER, $line);
        $line = str_replace("{{FOLDEREDITNAME}}",
            get_string("folder:editname", "local_teacherboard"), $line);
        $line = str_replace("{{FOLDERDELETE}}",
            get_string("folder:delete", "local_teacherboard"), $line);
        $line = str_replace("{{ADD_MODULE}}", ADD_MODULE, $line);
        $line = str_replace("{{EDIT}}", EDIT, $line);
        $line = str_replace("{{DELETE}}", DELETE, $line);

        $thead = "";
        foreach($this->_arrayCourses as $arrayEntry) {
            $thead .= $arrayEntry['head'];
        }
        $line = str_replace("{{THEAD}}", $thead, $line);

        return $line;
    }
    /*
     * html representation of one FOLDER
     */
    public function folderAddCourseButton($folderid) {
        global $CFG;
        $line = $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/folderAddCourse.php");
        $line = str_replace("{{FOLDERID}}", $folderid, $line);
        $line = str_replace("{{ADDCOURSEINTHISFOLDER}}",
            get_string("folder:addcourse", "local_teacherboard"), $line);

        return $line;
    }



    /**
     * create HTML string representing pages tree
     *
     * @param int $i (default 0) internal param used in recursion
     * @param array $tab array generated with getChainedPages
     * @param $indent (default 0) internal param used in recursion
     */
    public function generateHtmlPagesTree($i,$tab, $indent) {
        $end = false;

        while (isset($tab[$i]->id) && !$end) {
            echo $this->folderHtml($tab[$i]->id, $tab[$i]->name);
            if (isset($tab[$i]->child)) {
                echo "<ol class='dd-list'>";
                $this->generateHtmlPagesTree($tab[$i]->child, $tab, $indent);
                echo "</ol>";
            }
            if (!isset($tab[$i]->next)) {
                $end = true;
            }
            else {
                $i = $tab[$i]->next;
            }
            echo "</li>\n";
        }
    }
    /*
     *  verify if current user has folders
     */
    public function hasFolders() {
        global $DB;
        $nbFolders = $DB->count_records('teacherboard_page',
                array('userid' => $this->_id));
        return $nbFolders;
    }
    /*
     *  add a root folder for this user
     */
    public function addRootFolder($folderName = "folder:root") {
        global $DB;
        $folder = new \stdClass();
        $folder->userid = $this->_id;
        $folder->parentid = 0;
        $folder->sortorder = 0;
        $maxorder = $DB->get_record_sql(
                'SELECT sortorder '
                . 'FROM {teacherboard_page} '
                . 'WHERE userid="' . $this->_id . '"'
                . "ORDER BY sortorder DESC "
                . "LIMIT 1"
        );
        if ($maxorder) {
            $folder->sortorder = $maxorder->sortorder + 1;
        }
        $folder->name = get_string($folderName, "local_teacherboard");
        $newfolderid = $DB->insert_record('teacherboard_page', $folder, true);
        return $newfolderid;
    }
    /*
     * get first folder id, add one if none exists
     */
    public function getFirstFolder() {
        global $DB;
        $data = $DB->get_record('teacherboard_page', ['userid' => $this->_id], 'id', IGNORE_MULTIPLE);
        // If a folder exist, return the ID
        if($data) {
            return $data->id;
        }
        // If no folder exists, create one
        return $this->addRootFolder();
    }
    /*
     *  rename folder
     */
    public function renameFolder($folderid, $foldername) {
        global $DB;

        $folder = $DB->get_record('teacherboard_page',
                array('id' => $folderid, 'userid' => $this->_id));
        if (is_object($folder)) {
            if ($folder->userid == $this->_id) {
                $updatefolder = new \stdClass();
                $updatefolder->id = $folderid;
                $updatefolder->name = $foldername;
                $updatefolder->userid = $folder->userid;
                $updatefolder->parentid = $folder->parentid;
                $updatefolder->sortorder = $folder->sortorder;
                $DB->update_record('teacherboard_page', $updatefolder);
            }
        }
    }
    /*
     *  delete folder
     */
    public function deleteFolder($folderid) {
        global $DB;
        $content = 'fail';
        $folder = $DB->get_record('teacherboard_page',
                array('id' => $folderid, 'userid' => $this->_id));
        if (is_object($folder)) {
            $nbitems = $DB->count_records('teacherboard_page_items',
                    array('pageid' => $folder->id));
            if ($nbitems == 0) {
                $DB->delete_records('teacherboard_page',
                        array('id' => $folder->id));
                $content = 'done';
            }
            else {
                $content = get_string('folder:contains_items', 'local_teacherboard');
            }
        }
        return $content;
    }

    /**
     * When a page 'n' is moved from a parent page, we can fill the gap by
     * moving down pages 'n+1', 'n+2'... to 'n', 'n+1'...
     *
     *
     */
    public function reorderCurrentBrothers($folderid) {
            global $DB;

            $folder = $DB->get_record('teacherboard_page',
                    array('id' => $folderid));

            $brothers = $DB->get_records_sql(
                    "SELECT * "
                    . "FROM {teacherboard_page} "
                    . "WHERE parentid = '" . $folder->parentid . "' "
                    . "AND userid='" . $this->_id . "' "
                    . "ORDER BY sortorder ASC "
            );
            foreach($brothers as $brother) {
                if ($brother->sortorder > $folder->sortorder) {
                    $brother->sortorder = $brother->sortorder - 1;
                    $DB->update_record('teacherboard_page', $brother);
                }
            }
    }
    /**
     * When a page is added just behind page 'n',
     * we first move up pages 'n+1', 'n+2' to 'n+2', 'n+3'...
     *
     *
     */

    public function reorderPreviousBrothers($previous, $parent, $current) {
        global $DB;

        if ($previous != 'undefined') {
            $rec = $DB->get_record_sql(""
                    . "SELECT * "
                    . "FROM {teacherboard_page} "
                    . "WHERE id = '".$previous."' "
            );
            $parent = $rec->parentid;
            $rank = $rec->sortorder;
        }
        else {
            $rank = 0;
        }

        if ($parent == 'undefined') $parent = 0;
        $brothers = $DB->get_records_sql(""
            . "SELECT * "
            . "FROM {teacherboard_page} "
            . "WHERE parentid = '" . $parent . "' "
            . "AND userid='" . $this->_id . "' "
            . "ORDER BY sortorder ASC "
        );
        foreach($brothers as $brother) {
            if ($brother->sortorder > $rank) {
                $brother->sortorder = $brother->sortorder+1;
                $DB->update_record('teacherboard_page', $brother);
            }
        }
        $folder = $DB->get_record_sql("SELECT * "
                . "FROM {teacherboard_page} "
                . "WHERE id = '".$current."' "
        );
        $folder->sortorder = $rank+1;
        $folder->parentid = $parent;
        $DB->update_record('teacherboard_page', $folder);
    }
    /*
     * get courses located in folderid
     *
     */

    public function getCoursesFromFolder($folderid) {
        global $DB, $USER;
        $courses = Array();
        $folder = $DB->get_record('teacherboard_page',
                array('id' => $folderid, 'userid' => $this->_id));
        if ($folder) {
            $folderitems = $DB->get_records_sql('SELECT * '
                    . 'FROM {teacherboard_page_items} '
                    . 'WHERE pageid = "' . $folder->id . '" '
                    . 'ORDER BY sortorder ASC ');
            foreach($folderitems as $folderitem) {
                $currentCourse = $DB->get_record('course',
                        array('id' => $folderitem->courseid));
                if ($currentCourse) {
                    $context = \context_course::instance($currentCourse->id);
                    if (is_enrolled($context, $USER)) {
                        array_push($courses,
                                Array('course' => $currentCourse,
                                    'folderitem' => $folderitem));
                    }
                }
                else {
                    $DB->delete_records('teacherboard_page_items',
                            array('id' => $folderitem->id));
                }
            }
        }
        return $courses;
    }
    /*
     * get html representation of courses
     *
     */

    public function getHtmlArrayCourses($items) {

        global $CFG;
        $result = "<tbody class='dd-list-table'>";
        $template = "";
        foreach($this->_arrayCourses as $arrayEntry) {
            $template .= $arrayEntry['body'];
        }

        foreach ($items as $item) {
            $result .= $this->getHtmlCourse($item);
        }
        if (count($items) == 0) {
            $result .= $this->_view->renderNoHeaders($CFG->dirroot .
                        "/local/teacherboard/lib/template/courseRowEmpty.php");
            $result = str_replace("{{NBCOLUMNS}}", $this->getNbColumns(), $result);
        }
        $result .= "</tbody>";
        return $result;
    }



    /*
     * add course to folder
     *
     */

    public function addCourseToFolder($folderid, $courseid) {
        global $DB;
        $item = new \stdClass();
        $item->courseid = $courseid;
        $item->pageid = $folderid;

        $maxorder = $DB->get_record_sql(
            'SELECT sortorder '
            . 'FROM {teacherboard_page_items} '
            . 'WHERE pageid="' . $folderid . '"'
            . "ORDER BY sortorder DESC "
            . "LIMIT 1"
        );
        if ($maxorder) {
            $item->sortorder = $maxorder->sortorder + 1;
        }
        else {
            $item->sortorder = 0;
        }

        $item->id = $DB->insert_record("teacherboard_page_items", $item, true);
        return $item;
    }

    /*
     * add course to folder
     *
     */

    public function showHideCourse($itemid) {
        global $DB, $USER;
        error_log("showHideCourse");
        $item = $DB->get_record_sql('SELECT tpi.courseid '
                . 'FROM {teacherboard_page_items} as tpi, {teacherboard_page} as tp '
                . 'WHERE tpi.id = "' . $itemid . '" '
                . 'AND tp.id = tpi.pageid '
                . 'AND tp.userid = "' . $USER->id . '" ');
        if ($item) {
            $course = $DB->get_record('course', array('id' => $item->courseid));
            if ($course) {
                $course->visible = ($course->visible + 1) % 2;
                $DB->update_record('course', $course);
            }
        }

    }

    /*
     * delete course from folder
     *
     */

    public function deleteCourseFromFolder($itemid) {
        global $DB, $USER, $CFG;
        $item = $DB->get_record_sql('SELECT tpi.courseid '
                . 'FROM {teacherboard_page_items} as tpi, {teacherboard_page} as tp '
                . 'WHERE tpi.id = "' . $itemid . '" '
                . 'AND tp.id = tpi.pageid '
                . 'AND tp.userid = "' . $USER->id . '" ');
        $return = false;
        if ($item) {
            $course = $DB->get_record('course', array('id' => $item->courseid));
            if ($course) {
                $context = \context_course::instance($course->id);
                if(has_capability('local/teacherboard:deletecourse', $context)) {
                    delete_course($course, false);
                    $DB->delete_records('teacherboard_page_items', array('id' => $itemid));

                    $return = true;
                } else {
                    //Else we check if user is a teacher in its "profil" field, but we don't delete course then
                    $user_field_profil = $DB->get_record('user_info_field', [
                        'shortname' => "profil"
                    ], 'id', MUST_EXIST)->id;
                    $user_profil = $DB->get_record('user_info_data', [
                        'fieldid' => $user_field_profil,
                        'userid' => $USER->id
                    ], 'data', IGNORE_MISSING);
                    if ($user_profil && $user_profil->data === 'teacher') {
                        require_once $CFG->dirroot . '/lib/enrollib.php';
                        $enrol = $DB->get_record_sql(
                            'SELECT e.* FROM mdl_enrol e
                                INNER JOIN mdl_user_enrolments ue
                                ON ue.enrolid = e.id
                                WHERE ue.userid = :userid
                                AND e.enrol IN ("manual", "self")
                                AND courseid = :courseid'
                            , ['userid' => $USER->id, 'courseid' => $course->id], MUST_EXIST);
                        switch ($enrol->enrol) {
                            case 'manual':
                                require_once $CFG->dirroot . '/enrol/manual/lib.php';
                                $instance = new \enrol_manual_plugin();
                                break;
                            case 'self':
                                require_once $CFG->dirroot . '/enrol/self/lib.php';
                                $instance = new \enrol_self_plugin();
                                break;
                            default:
                                throw new Exception("Not manage case: trying to unrol with $enrol->enrol plugin");
                        }
                        $instance->unenrol_user($enrol, $USER->id);
                        $DB->delete_records('teacherboard_page_items', array('id' => $itemid));
                        $return = true;
                    }
                }
            }
        }
        return $return;

    }
    /**
     * Reorder a module in a page or move a module to a new page
     *
     * @param int $moduleid     id of the selected module (activity or block)
     * @param int $pageid       id of the targeted page
     * @param int $previousid     id of the previous module in the DOM
     *
     */
    public function moveFolderItem($moduleid, $pageid,$previousid) {
        global $DB;

        $currentmodule = $DB->get_record_sql("SELECT * FROM {teacherboard_page_items} WHERE id = '".$moduleid."' ");
        if ($currentmodule) {
            $currentmodule->pageid = $pageid;
            if ($previousid != "undefined") {
                $previousmodule = $DB->get_record_sql("SELECT * FROM {teacherboard_page_items} WHERE id = '".$previousid."'
                    AND pageid = '".$pageid."' ");
                if ($previousmodule) {
                    error_log("previous");
                    $uppermodules = $DB->get_records_sql("SELECT * FROM {teacherboard_page_items} WHERE pageid = '".$pageid."'
                        AND sortorder > '".$previousmodule->sortorder."' ");
                    foreach ($uppermodules as $uppermodule) {
                        error_log($uppermodule->sortorder);
                        $uppermodule->sortorder++;
                        $DB->update_record('teacherboard_page_items', $uppermodule);
                    }
                    $currentmodule->sortorder = $previousmodule->sortorder + 1;
                }
                else {
                    $currentmodule->sortorder = 0;
                }
            }
            else {
                $allmodules = $DB->get_records_sql("SELECT * FROM {teacherboard_page_items} WHERE pageid = '".$pageid."' ");
                foreach ($allmodules as $allmodule) {
                    $allmodule->sortorder++;
                    $DB->update_record('teacherboard_page_items', $allmodule);
                }
                $currentmodule->sortorder = 0;
            }

            $DB->update_record('teacherboard_page_items', $currentmodule);
        }
        else {
            //echo "module does not exist";
        }
    }
    /*
     * reaffect orphans courses in first folder
     */
    public function checkOrphansCourses() {
        global $DB, $CFG;
        $courses = enrol_get_my_courses();
        $nbFolderItems = $DB->get_records_sql("SELECT tbpi.id, tbpi.courseid "
                . "FROM {teacherboard_page_items} as tbpi, {teacherboard_page} as tbp "
                . "WHERE tbp.userid = '".$this->_id."' "
                . "AND tbpi.pageid = tbp.id");

        $courseTeacherboard = array();
        $courseRemoval = array();
        $cleanup = false;
        foreach($nbFolderItems as $folderItem) {
            $courseTeacherboard[$folderItem->courseid] = array(
                'folderid' => $folderItem->id,
                'available' => false
            );
        }
        foreach ($courses as $course) {
            $courseTeacherboard[$course->id]['available'] = true;
        }
        foreach($courseTeacherboard as $course) {
            if (!$course['available']) {
                $DB->delete_records('teacherboard_page_items', array('id' => $course['folderid']));
                $cleanup = true;
            }
        }
        if ($cleanup) {
            $nbFolderItems = $DB->get_records_sql("SELECT tbpi.id, tbpi.courseid "
                    . "FROM {teacherboard_page_items} as tbpi, {teacherboard_page} as tbp "
                    . "WHERE tbp.userid = '".$this->_id."' "
                    . "AND tbpi.pageid = tbp.id");

        }
        $coursesStored = Array();
        $msg = "";
        if (count($courses) > count($nbFolderItems)) {
            $firstfolder = $DB->get_record_sql("SELECT * FROM {teacherboard_page} "
                    . "WHERE userid = '" . $this->_id . "' "
                    . "AND parentid = '0' "
                    . "ORDER BY sortorder ASC "
                    . "LIMIT 1");
            if ($firstfolder) {
                foreach($courses as $course) {
                    $alreadyStored = $DB->get_record_sql("SELECT tbpi.id "
                            . "FROM {teacherboard_page_items} as tbpi, {teacherboard_page} as tbp "
                            . "WHERE tbp.userid = '".$this->_id."' "
                            . "AND tbpi.pageid = tbp.id "
                            . "AND tbpi.courseid = '" . $course->id . "' ");
                    if (!$alreadyStored) {
                        array_push($coursesStored, array('fullname' => $course->fullname, 'id' => $course->id));
                        $this->addCourseToFolder($firstfolder->id, $course->id);
                    }
                }
            }
            else {
                $msg = "can't find first folder";
            }
        }

        if (count($coursesStored)) {
            $msg .= get_string('course:affectation', 'local_teacherboard')
                    . "<ul class='coursesAffectation'>";
            foreach($coursesStored as $currentCourse) {
                $msg .= "<li><a href='"
                    . $CFG->wwwroot
                    . "/course/view.php?id="
                    . $currentCourse['id']
                    . "'>"
                    . $currentCourse['fullname']
                    . "</a></li>";
            }
            $msg .= "</ul>";
        }
        return $msg;
    }
    /*
     * get cohorts
     */
    public function getCohorts() {
        global $DB;

        $cohortsCategorized = Array();

        foreach($this->_rne as $idnumber) {
            if ($idnumber) {
                $category = $DB->get_record('course_categories',
                        array('idnumber' => $idnumber));
                if ($category) {
                    $context = \context_coursecat::instance($category->id);
                    $result = cohort_get_cohorts($context->id, 0, 0);
                    array_push($cohortsCategorized, Array(
                        "category" => $category,
                        "cohorts" => $result['cohorts']
                    ));
                }
            }
        }
        return $cohortsCategorized;
    }

    /*
     * get cohorts enrolled in specific courses
     */
    public function getCohortsEnrolled($courseid) {
        global $DB;

        $course = $DB->get_record('course', array('id' => $courseid));
        if ($course) {
            $context = \context_course::instance($courseid);
            $role_assignment = $DB->get_record('role_assignments',
                array(
                    'roleid' => 3,
                    'contextid' => $context->id,
                    'userid' => $this->_id
                )
            );

            if (has_capability(PERMISSION_USERS_COURSE, $context)) {
                $enrolled = $DB->get_records_sql("
                    SELECT customint1 "
                    . "FROM {enrol} "
                    . "WHERE enrol = 'cohort' "
                    . "AND courseid = '$courseid'"
                );
                return $enrolled;
            }
        }
        return null;
    }

    /*
     * get courses (for meta course purpose) sorted by folders
     */
    public function getCoursesForMetaEnrolment($courseid) {
        global $DB;

        $folders = $DB->get_records('teacherboard_page', ['userid' => $this->_id], 'sortorder ASC', 'id, name');
        foreach ($folders as &$folder) {
            $allcourses = $this->getCoursesFromFolder($folder->id);
            $eligiblecourses = [];
            foreach ($allcourses as &$course) {
                if (has_capability('moodle/course:update', \context_course::instance($course['course']->id, MUST_EXIST))) {
                    $metaenrolment = $DB->get_record(
                        'enrol',
                        [   'enrol' => 'meta',
                            'courseid' => $courseid,           // course with enrolment rule
                            'customint1' => $course['course']->id] // possible metacourse
                    );
                    $course['enrolled'] = $metaenrolment ? true : false;
                    $eligiblecourses[] = $course;
                }
            }
            $folder->courses = $eligiblecourses;
        }
        return $folders;
    }

    /*
     * get teacher enrolled in the course and teachers
     * who have rne field defined to id category
     */
    public function getTeachers($courseid) {
        global $DB, $USER;

        $teachers = Array();

        // get teachers enrolled in the current course
        $contextid = $DB->get_record(
            'context',
            ['contextlevel' => 50,
                'instanceid' => $courseid],
            'id')
            ->id;

        $coursecreatoruserid = $DB->get_record(
            'role_assignments',
            ['roleid' => Role::instance()->COURSE_CREATOR, 'contextid' => $contextid],
            'userid')
            ->userid;

        $enrolledTeachers = $DB->get_records_select(
            'role_assignments',
            'contextid = ? and (roleid = ? or roleid = ?)',
            [$contextid, Role::instance()->EDITING_TEACHER, Role::instance()->TEACHER],
            '',
            'userid'
        );

        foreach ($enrolledTeachers as $enrolledTeacher) {
            $teacherObj = new \stdClass();
            $teacherObj->id = $enrolledTeacher->userid;
            $teacherObj->enrolment = True;
            $teachers[$teacherObj->id] = $teacherObj;
        }

        foreach ($this->_rne as $categoriesIdNumber) {
            $teachersFromEtab = $DB->get_records_sql("
                SELECT uid.userid
                FROM {user_info_data} AS uid, {user_info_field} AS uif
                WHERE uid.data = '" . $categoriesIdNumber . "'
                AND uif.shortname RLIKE 'rne[1-5]'
                AND uid.fieldid = uif.id
            ");

            foreach ($teachersFromEtab as $officialTeacher) {
                $teacherObj = new \stdClass();
                $teacherObj->id = $officialTeacher->userid;
                if (!array_key_exists($officialTeacher->userid, $teachers)) {
                    $teacherObj->enrolment = False;
                    $teachers[$teacherObj->id] = $teacherObj;
                }

            }
        }

        // Remove course creator from list as it cannot be affected by changes
        unset($teachers[$coursecreatoruserid]);

        foreach ($teachers as $teacher) {
            $user = $DB->get_record_sql("
                SELECT firstname, lastname
                FROM {user}
                WHERE id = " . $teacher->id . "
            ");
            $teacher->name = $user->lastname . " " . $user->firstname;
            $teacher->lastname = $user->lastname;
        }

        // Sort the multidimensional array
        usort($teachers, function ($a,$b) {
              return $a->lastname > $b->lastname;
         });

        return $teachers;
    }


    /*
     * get html representation of teachers
     *
     * <div id="teachers-block">
     *   <ul>
     *     <li class='enrolteacher_li' data-id='1' >
     *       <div class='enrolled'>teacher 1</div>
     *     </li>
     *     <li class='enrolteacher_li'  data-id='2' >
     *       <div class='notenrolled'>teacher 2</div>
     *     </li>
     *   </ul>
     * </div>
     */
    public function getTeachersHtml($teachersList) {
        $result     = "";
        $ul         =   "<ul>";
        $ul_end     = "</ul>";
        $li_teacher = "<li class='enrolteacher_li' data-id='{{TEACHERID}}'>"
                . "<div class='{{ENROLMENTSTATUS}}'>"
                . "{{TEACHERNAME}}"
                . "</div>"
                . "</li>";
        $blockdiv_start = "<div id='teachers-block'>";
        $blockdiv_end = "</div>";

        foreach($teachersList as $teacher) {
            $current_li_teacher = $li_teacher;
            $current_li_teacher = str_replace("{{TEACHERNAME}}",
                    $teacher->name,
                    $current_li_teacher);
            $current_li_teacher = str_replace("{{TEACHERID}}",
                    $teacher->id,
                    $current_li_teacher);
            $current_li_teacher = str_replace("{{COHORTID}}",
                    $teacher->id,
                    $current_li_teacher);
            if ($teacher->enrolment) {
                $current_li_teacher = str_replace("{{ENROLMENTSTATUS}}",
                        "enrolled",
                        $current_li_teacher);
            }
            else {
                $current_li_teacher = str_replace("{{ENROLMENTSTATUS}}",
                        "notenrolled",
                        $current_li_teacher);

            }
            $result .= $current_li_teacher;
        }
        return $ul . $blockdiv_start . $result . $blockdiv_end . $ul_end;
    }


    /*
     * get html representation of cohorts (for jqueryui tabs)
     *
     * <div id="tabs">
     *   <ul>
     *     <li><a href="#tab-1">categ1</a></li>
     *     <li><a href="#tab-2">categ2</a></li>
     *   </ul>
     *   <div id='tab-cohorts-block'>
     *     <div id="tab-1">
     *       <ul>
     *         <li class='enrolcohort' data-id='1' >
     *           <div class='enroldiv'>cohort 1</div>
     *         </li>
     *         <li class='enrolcohort'  data-id='1' >
     *           <div class='enroldiv'>cohort 2</div>
     *         </li>
     *       </ul>
     *     </div>
     *     <div id="tab-2">
     *       <ul>
     *         <li class='enrolcohort'  data-id='1' >
     *           <div class='enroldiv'>cohort 3</div>
     *         </li>
     *         <li class='enrolcohort'  data-id='1' >
     *           <div class='enroldiv'>cohort 4</div>
     *         </li>
     *       </ul>
     *     </div>
     *   </div>
     * </div>
     */
    public function getCohortsHtml($cohortsCategorized) {
        $result = "";
        $ul =   "<ul>";
        $li =     "<li>"
                . "<a href='#tab-{{TAG}}'>{{CATEGNAME}}</a>"
                . "</li>";
        $div =    "<div id='tab-{{TAG}}'>"
                . "<ul>{{COHORTS}}</ul>"
                . "</div>";
        $li_cohort = "<li class='enrolcohort' data-id='{{COHORTID}}'>"
                . "<div class='enroldiv'>"
                . "{{COHORTNAME}}"
                . "</div>"
                . "</li>";
        $blockdiv_start = "<div id='tab-cohorts-block'>";
        $search = '';
        $blockdiv_end = "</div>";
        foreach($cohortsCategorized as $category) {
            $current_li = $li;
            $current_div = $div;
            $current_div_cohorts = "";

            $current_li = str_replace("{{TAG}}", $category['category']->idnumber, $current_li);
            $current_li = str_replace("{{CATEGNAME}}", $category['category']->name, $current_li);
            $ul .= $current_li;
            $current_div = str_replace("{{TAG}}", $category['category']->idnumber, $current_div);
            if(count($category['cohorts']) > 10){
                $search = '<div id="cohort-filter-block" style="margin-top: 0.5em;">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    Rechercher une classe : <input type="text" id="cohortfilter"/><br/>
                    <span id="cohort-filter-block-fail" style="display: none">Aucune classe ne correspond à votre recherche...</span>
                </div>';
            }
            foreach($category['cohorts'] as $cohort) {
                $current_li_cohort = $li_cohort;
                $current_li_cohort = str_replace("{{COHORTNAME}}", $cohort->name, $current_li_cohort);
                $current_li_cohort = str_replace("{{COHORTID}}", $cohort->id, $current_li_cohort);
                $current_div_cohorts .= $current_li_cohort;
            }
            $current_div = str_replace("{{COHORTS}}", $current_div_cohorts, $current_div);
            $result .= $current_div;
        }
        $ul .= "</ul>";

        return $ul . $blockdiv_start . $search . $result . $blockdiv_end;
    }
    /*
     * enrol/unenrol cohort in specified course
     */
    public function toggleEnrolCohort($cohortid, $courseid) {
        global $DB;
        $return = "";
        $course = $DB->get_record('course', array('id' => $courseid));
        if ($course) {
            $context = \context_course::instance($courseid);
            /*$role_assignment = $DB->get_record('role_assignments',
                    array(
                        'roleid' => 3,
                        'contextid' => $context->id,
                        'userid' => $this->_id
                    )
            );*/
            /*$current_user_is_a_teacher = $DB->get_records_sql("
                SELECT *
                FROM {role_assignments}
                WHERE userid = ?
                AND contextid = ?
                AND (roleid = '3' OR roleid='4')"
                , array($this->_id, $context->id)
            );*/

            //if ($current_user_is_a_teacher) {
            if (has_capability(PERMISSION_USERS_COURSE, $context)) {

                $enrolstate = $DB->get_records("enrol", array(
                    'customint1' => $cohortid,
                    'courseid' => $courseid
                    )
                );

                if (!$enrolstate) {
                    error_log('cohort not yet enrolled');
                    // cohort not yet enrolled in that course
                    $cohort = $DB->get_record('cohort', array('id' => $cohortid));
                    $enrol = enrol_get_plugin('cohort');
                    $enrol->add_instance($course, array(
                        'name'=>$cohort->name,
                        'status'=> '0',
                        'customint1'=> $cohortid,
                        'roleid'=> '5',
                        'customint2'=>-1) // to create one group per cohort elea/platform#404
                    );
                    $return = "enrolled";
                }
                else {
                    // cohort already enrolled in that course
                    $enrol = enrol_get_plugin('cohort');
                    foreach ($enrolstate as $enrolentry) {
                        $enrol->delete_instance($enrolentry);
                    }
                    $return = "unenrolled";
                }
            }
        }
        return $return;
    }
    /*
     * enrol/unenrol teacher in specified course
     */
    public function toggleEnrolTeacher($teacherid, $courseid) {
        global $DB;
        $return = "";
        $course = $DB->get_record('course', array('id' => $courseid));
        if ($course) {
            $context = \context_course::instance($courseid);
            $role_assignment = $DB->get_record('role_assignments',
                    array(
                        'roleid' => 3,
                        'contextid' => $context->id,
                        'userid' => $this->_id
                    )
            );
            //if ($role_assignment) {
            if (has_capability(PERMISSION_ENROL_TEACHERS, $context)) {
                $enrolstate = $DB->get_records_sql("
                    SELECT ra.userid
                    FROM {role_assignments} AS ra, {context} AS c
                    WHERE c.contextlevel = '50'
                    AND c.instanceid = '" . $courseid . "'
                    AND ra.roleid = '4'
                    AND ra.contextid = c.id
                    AND ra.userid = '" . $teacherid . "'
                ");

                if (!$enrolstate) {
                    error_log('teacher not yet enrolled');
                    // teacher not yet enrolled in that course

                    $enrol = enrol_get_plugin('manual');
                    $enrolinstances = enrol_get_instances($courseid, true);
                    $instance = null;
                    foreach ($enrolinstances as $courseenrolinstance) {
                        if ($courseenrolinstance->enrol == "manual") {
                            $instance = $courseenrolinstance;
                            break;
                        }
                    }
                    if ($instance) {
                        $enrol->enrol_user($instance, $teacherid, 4);
                        $return = "enrolled";
                    }
                    else {
                        $return = "no manual enrolment available";
                    }


                }
                else {

                    $enrol = enrol_get_plugin('manual');
                    $enrolinstances = enrol_get_instances($courseid, true);
                    $instance = null;
                    foreach ($enrolinstances as $courseenrolinstance) {
                        if ($courseenrolinstance->enrol == "manual") {
                            $instance = $courseenrolinstance;
                            break;
                        }
                    }
                    if ($instance) {
                        $enrol->unenrol_user($instance, $teacherid);
                        $return = "unenrolled";
                    }
                    else {
                        $return = "no manual enrolment available";
                    }

                }
            }
        }
        return $return;
    }
    /*
     * duplicate course
     */
    public function duplicateCourse($courseid) {
        global $DB;
        $content = null;
        error_log($courseid);
        //@todo : don't use RBAC but capability instead
        $course = $DB->get_record('course', array('id' => $courseid));
        if (!$course) {
            return null;
        }

        $context = \context_course::instance($courseid);
        $role_assignment = $DB->get_record('role_assignments',
                array(
                    'roleid' => 3,
                    'contextid' => $context->id,
                    'userid' => $this->_id
                )
        );
        if ($role_assignment) {
            error_log("let's duplicate !");
            $duplication = \core_course_external::duplicate_course(
                    $course->id,
                    get_string("course:prefixduplication", "local_teacherboard") . $course->fullname,
                    get_string('course:shortname', 'local_teacherboard') . "-" . uniqid(),
                    $this->getDefaultCategory()->id,
                    0
            );
            error_log("great");
            if ($duplication) {
                error_log("duplication done");
                $newcourse = $DB->get_record('course', Array('id' => $duplication['id']));
                if ($newcourse) {
                    $currentFolder = $DB->get_record_sql('SELECT tp.* '
                            . 'FROM {teacherboard_page_items} as tpi, {teacherboard_page} as tp '
                            . 'WHERE tpi.courseid = "' . $course->id . '" '
                            . 'AND tp.id = tpi.pageid '
                            . 'AND tp.userid = "' . $this->_id . '" ');
                    if ($currentFolder) {
                        $item = Array();
                        $item['course'] = $newcourse;
                        $item['folderitem'] = $this->addCourseToFolder($currentFolder->id, $newcourse->id);
                        $content = $this->getHtmlCourse($item);
                    }
                }
            }
            else {
                error_log("gasp...duplication is not possible.Verify account permissions");
            }
        }

        return $content;
    }
    /*
     * extract students completion state
     *
     */
    public function getStudentsCompletion($courseid) {
        global $DB;
        //@todo : don't use RBAC but capability instead
        $course = $DB->get_record('course', array('id' => $courseid));
        if (!$course) {
            return null;
        }
        $studentsArray = Array();
        $completion = new \completion_info($course);
        $activities = $completion->get_activities();

        $usersProgress = $completion->get_progress_all();

        $context = \context_course::instance($courseid);

        $role_assignment = $DB->get_record('role_assignments',
                array(
                    'roleid' => 3,
                    'contextid' => $context->id,
                    'userid' => $this->_id
                )
        );

        if (has_capability(PERMISSION_USERS_COURSE, $context)) {
        //if ($role_assignment) {
            // user is a teacher in this course

            foreach($usersProgress as $user) {

                $studentsArray[$user->id] = Array();
                $studentsArray[$user->id]['sortfield'] = strtolower($user->lastname);
                $studentsArray[$user->id]['identity'] = $user->lastname . " " . $user->firstname;
                $studentsArray[$user->id]['activities'] = Array();
                $cells = Array();
                // Progress for each activity

                // modeinfo example
                //$moduleinfo->completion = COMPLETION_TRACKING_AUTOMATIC;
                //$moduleinfo->completionview = COMPLETION_VIEW_REQUIRED;
                //$moduleinfo->completiongradeitemnumber = 1;
                //$moduleinfo->completionexpected = time() + (7 * 24 * 3600);
                //$moduleinfo->completionunlocked = 1;


                foreach($activities as $activity) {
                    $cells[$activity->id] = Array();
                    $cells[$activity->id]['circle'] = null;
                    $cells[$activity->id]['attempts'] = null;
                    $cells[$activity->id]['title'] = str_replace("'", " ", $activity->name);
                    // Get progress information and state
                    if (array_key_exists($activity->id,$user->progress)) {
                        $thisprogress = $user->progress[$activity->id];
                        $state = $thisprogress->completionstate;
                        error_log($activity->name);

                        if ($state == COMPLETION_INCOMPLETE) {
                            error_log($state . " " . COMPLETION_INCOMPLETE);
                            if ($activity->completiongradeitemnumber != NULL) {
                                $cells[$activity->id]['circle'] = "transparent";
                            }
                        }
                        elseif ($state == COMPLETION_COMPLETE) {
                            if ($activity->completiongradeitemnumber != NULL) {
                                $cells[$activity->id]['circle'] = "black";
                            }
                        }
                        elseif ($state == COMPLETION_COMPLETE_PASS) {
                            if ($activity->completiongradeitemnumber != NULL) {
                                $cells[$activity->id]['circle'] = "transparent";
                            }
                        }
                        elseif ($state == COMPLETION_COMPLETE_FAIL) {
                            if ($activity->completiongradeitemnumber != NULL) {
                                $cells[$activity->id]['circle'] = "transparent";
                            }
                        }

                        //If it's a millionnaire, we handle this differently by always showing the dot and number of attempts
                        if($activity->modname == "millionnaire"){
                            $cells[$activity->id]['circle'] = "black";
                            $attempts = $DB->get_record('millionnaire_grades',
                                ['userid' => $user->id, 'millionnaire' => $activity->instance], 'attempts', IGNORE_MISSING);
                            if($attempts) $cells[$activity->id]['attempts'] = $attempts->attempts;
                        }

                        //epikmatching too !
                        if($activity->modname == "epikmatching"){
                            $cells[$activity->id]['circle'] = "black";
                            $attempts = $DB->get_record('epikmatching_success',
                                ['userid' => $user->id, 'epikmatching' => $activity->instance], 'attempts', IGNORE_MISSING);
                            if($attempts) $cells[$activity->id]['attempts'] = $attempts->attempts;
                        }

                        $date = userdate($thisprogress->timemodified);
                        $cells[$activity->id]['style'] = 'cell_green';

                    } else {
                        $state=COMPLETION_INCOMPLETE;
                        $date='';
                        $cells[$activity->id]['style'] = 'cell_grey';
                        if ($activity->completiongradeitemnumber != NULL) {
                            $cells[$activity->id]['circle'] = "transparent";
                        }
                    }
                    $studentsArray[$user->id]['activities'][$activity->id] = $cells[$activity->id];
                }
            }
        }
        else {
            $role_assignment = $DB->get_records('role_assignments',
                    array(
                        'roleid' => 5,
                        'contextid' => $context->id,
                        'userid' => $this->_id
                    )
            );

            if ($role_assignment) {
                // user is a student in this course

            }
        }
        return $studentsArray;

    }

    /*
     * extract students completion state by enrolement
     */
    public function getStudentsCompletionByEnrol($courseid) {
        global $DB;

        $usersbyenrol = [];
        $completions = $this->getStudentsCompletion($courseid);


        // users by cohort
        $cohorts = $DB->get_records('enrol', ['courseid' => $courseid, 'enrol' => 'cohort'], 'name', 'name, customint1');
        foreach ($cohorts as $cohort) {
            $users = $DB->get_records('cohort_members', ['cohortid' => $cohort->customint1]);
            $usersbyenrol[$cohort->name] = $this->handleUsersCompletion($users, $completions);
            if (empty($usersbyenrol[$cohort->name])) {
                unset($usersbyenrol[$cohort->name]);
            }
        }

        // users by other ways
        $enrols = $DB->get_records('enrol', ['courseid' => $courseid], '', 'id, enrol');
        $allotherusers = [];
        foreach ($enrols as $enrol) {
            if ($enrol->enrol !== 'cohort') {
                $users = $DB->get_records('user_enrolments', ['enrolid' => $enrol->id]);
                $allotherusers = array_merge($allotherusers, $users);
            }
        }
        if (count($allotherusers)) {
            $other = get_string('achievement:others', 'local_teacherboard');
            $usersbyenrol[$other] = $this->handleUsersCompletion($allotherusers, $completions);
        }
        return $usersbyenrol;
    }

    /*
     * get completion for an array of users and sort them
     */
    private function handleUsersCompletion($users, $completions) {
        $sort = [];
        $userscompletion = [];
        foreach ($users as $user) {
            if (isset($completions[$user->userid])) {
                $sort[] = $completions[$user->userid]['sortfield'];
                $userscompletion[] = $completions[$user->userid];
            }
        }
        array_multisort($userscompletion, $sort);
        return $userscompletion;
    }

    /*
     * extract students completion state
     *
     */
    public function getCourseName($courseid) {
        global $DB;
        $course = $DB->get_record_sql("SELECT fullname FROM {course} WHERE id='" . $courseid . "'");
        return $course->fullname;
    }
    /*
     * modify course fullname
     * Warning : permissions are not checked here !
     *
     */
    public function renameCourse($courseid, $coursename) {
        global $DB;
        $course = $DB->get_record("course", array('id' => $courseid));
        if ($course) {
            $course->fullname = $coursename;
            $DB->update_record("course", $course);
        }
    }

    /*
     *
     */

    public function enrolledInCohort($cohortid) {
        return cohort_is_member($cohortid, $this->_id);
    }

    /*
     *
     */

    public function enrolInCohort($cohortid) {
        cohort_add_member($cohortid, $this->_id);
    }

    /*
     *
     */

    public function unenrolInCohort($cohortid) {
        cohort_remove_member($cohortid, $this->_id);
    }

     /*
      *
      */

   public function isTeacherIn($category) {
     if ($category) {
       if (in_array($category->idnumber, $this->_rne)) return true;
     }
     return false;
   }

}

<?php
namespace teacherboard;
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


global $CFG;
include_once($CFG->libdir . '/completionlib.php');
include_once($CFG->libdir . '/coursecatlib.php');
include_once($CFG->libdir . '/moodlelib.php');
include_once($CFG->libdir . '/enrollib.php');
include_once($CFG->dirroot . '/cohort/lib.php');
include_once($CFG->dirroot . '/enrol/cohort/lib.php');
include_once($CFG->dirroot . '/course/externallib.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/view/view.class.php');

/**
 * Class used to manage board
 *
 * @package    teacherboard
 * @subpackage model
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Output {

    function __construct() {


    }

    /*
     *
     */

    public function renderUsersJson($usersList) {
     return json_encode($usersList);
    }
    /*
     *
     */

    public function renderUsers($usersList) {
     $result = '';
     foreach($usersList as $user) {
       $result .= '<input type="checkbox" id="userid_'.$user->id.'" data-userid="' . $user->id . '" data-firstname="' . $user->firstname . '" data-lastname="' . $user->lastname . '" name="userlist[]" value="'.$user->id.'"></input>';
       $result .= '<label for="userid_'.$user->id.'">' . $user->lastname . ' ' . $user->firstname . '</label>';
     }
     return $result;
    }


    /*
     *
     */

    public function renderGroups($groupsList) {
        $result = '';
        $result .= '<div style="display:flex;flex-wrap:wrap;width:100%;margin:0 auto 20px;">';
        foreach($groupsList as $group) {
        $result .= '<div style="width:25%;padding:10px;">'
            . '<div style="padding:10px;min-height:50px;background-color:#A8C131;color:white;">'
            . '<h3 style="background-color:transparent;padding:0;">' . $group->name . '</h3>'
            . '<p>53 inscrits</p>'
            . '<img data-id="'. $group->id .'" class="edit_group" style="margin:10px;height:30px" src="' . EDIT_WHITE . '" alt="edit">'
            . '<img data-id="'. $group->id .'" class="delete_group" style="margin:10px;height:30px" src="' . DELETE_WHITE . '" alt="delete">'
            . '</div>'
            . '</div>';
        }
        $result .= '</div>';
        return $result;
    }
}

<?php
namespace teacherboard;
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


global $CFG;
include_once($CFG->libdir . '/completionlib.php');
include_once($CFG->libdir . '/coursecatlib.php');
include_once($CFG->libdir . '/moodlelib.php');
include_once($CFG->libdir . '/enrollib.php');
include_once($CFG->dirroot . '/cohort/lib.php');
include_once($CFG->dirroot . '/enrol/cohort/lib.php');
include_once($CFG->dirroot . '/course/externallib.php');
include_once($CFG->dirroot . '/local/teacherboard/lib/view/view.class.php');

/**
 * Class used to manage custom groups
 *
 * @package    teacherboard
 * @subpackage model
 * @copyright  2017 Pascal Fautrero - DANE Versailles
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Group {

    function __construct($data) {
        $this->ownerid = $data['ownerid'];
        $this->id = null;
        if (array_key_exists('id', $data)) $this->id = $data['id'];
    }

    /*
     *
     */

    public function save($data) {
        global $DB;
        $this->name = $data['name'];
        $group = new \stdClass();
        $group->name = $this->name;
        $group->userid = $this->ownerid;
        $groupid = $DB->insert_record("teacherboard_group", $group, true);
        $this->id = $groupid;
        return $groupid;
    }

    /*
     *  Remove current group
     */

    public function remove() {
        global $DB;
        $content = array(
            'state' => 'fail',
            'message' => "That's some bad hat, Harry..."
        );
        if (!$this->id) {
            return $content;
        }
        $content = array(
            'state' => 'fail',
            'message' => "That's some bad hat, Harry..."
        );
        $group = $DB->get_record('teacherboard_group', array(
            'id' => $this->id,
            'userid' => $this->ownerid
        ));
        if ($group) {
            $DB->delete_records('teacherboard_group_members', array(
                'groupid' => $this->id
            ));
            $DB->delete_records('teacherboard_group', array(
                'id' => $this->id
            ));
            $content = array(
                'state' => 'success',
                'message' => 'Groupe supprimé avec succès'
            );
        }

        return $content;
    }

    /*
     *
     */

    public function clearAll() {
      global $DB;
      if (!$this->id) {
        return false;
      }
      $DB->delete_records('teacherboard_group_members', array(
        'groupid' => $this->id
      ));
    }

    /*
     *
     */

    public function enrol($array_users) {
        global $DB;
        if (!$this->id) {
            return false;
        }
        if (!is_array($array_users)) {
            return false;
        }
        foreach($array_users as $userid) {
            $enrol = $DB->get_record("teacherboard_group_members", array(
              'groupid' => $this->id,
              'userid' => intval($userid)
            ));
            if (!$enrol) {
                $enrol = new \stdClass();
                $enrol->groupid = $this->id;
                $enrol->userid = intval($userid);
                $enrolid = $DB->insert_record("teacherboard_group_members", $enrol, true);
            }
        }
        return true;
    }

    /*
     *
     */

    public function getUsers() {
        global $DB;
        $usersList = $DB->get_records_sql("
            SELECT u.id, u.firstname, u.lastname
            FROM {user} AS u, {cohort_members} as cm
            WHERE cm.userid = u.id
            AND cm.cohortid = '".$this->id."'
            ORDER BY u.lastname ASC"
        );
        return $usersList;
    }
}

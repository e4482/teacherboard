<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * global constants
 *
 * @package teacherboard
 * @subpackage config
 */

global $CFG;

/** just choose one style for your icons : 'default' */
define('IMG_FOLDER','local/teacherboard/lib/template/images/default');

// ============ Define paths to images

define('MOVE',$CFG->wwwroot . '/' . IMG_FOLDER . '/move.png');
define('FOLDER',$CFG->wwwroot . '/' . IMG_FOLDER . '/folder.png');
define('FOLDER_OPENED',$CFG->wwwroot . '/' . IMG_FOLDER . '/folder_opened.png');
define('ADD_MODULE',$CFG->wwwroot . '/' . IMG_FOLDER . '/addcourse.png');
define('EDIT',$CFG->wwwroot . '/' . IMG_FOLDER . '/edit.png');
define('EDIT_WHITE',$CFG->wwwroot . '/' . IMG_FOLDER . '/edit_white.png');
define('EDIT_OK',$CFG->wwwroot . '/' . IMG_FOLDER . '/edit_ok.png');
define('DELETE',$CFG->wwwroot . '/' . IMG_FOLDER . '/delete.png');
define('DELETE_WHITE',$CFG->wwwroot . '/' . IMG_FOLDER . '/delete_white.png');
define('UNENROL',$CFG->wwwroot . '/' . IMG_FOLDER . '/unenrol.png');
define('EYE_OPENED',$CFG->wwwroot . '/' . IMG_FOLDER . '/eye_opened.png');
define('EYE_CLOSED',$CFG->wwwroot . '/' . IMG_FOLDER . '/eye_closed.png');
define('PARAMS',$CFG->wwwroot . '/' . IMG_FOLDER . '/params.png');
define('USERS',$CFG->wwwroot . '/' . IMG_FOLDER . '/users.png');
define('GROUPS',$CFG->wwwroot . '/' . IMG_FOLDER . '/groups.png');
define('METACOURSELINK',$CFG->wwwroot . '/' . IMG_FOLDER . '/metacourse.png');
define('STATS',$CFG->wwwroot . '/' . IMG_FOLDER . '/stats.png');
define('ACLS',$CFG->wwwroot . '/' . IMG_FOLDER . '/list.png');
define('SHARE',$CFG->wwwroot . '/' . IMG_FOLDER . '/share.png');
define('DUPLICATE',$CFG->wwwroot . '/' . IMG_FOLDER . '/duplicate.png');
define('RETURNMENU',$CFG->wwwroot . '/' . IMG_FOLDER . '/return.png');
define('MOVEIN',$CFG->wwwroot . '/' . IMG_FOLDER . '/movein.png');
define('UP',$CFG->wwwroot . '/' . IMG_FOLDER . '/up.png');
define('DOWN',$CFG->wwwroot . '/' . IMG_FOLDER . '/down.png');
define('TEACHER',$CFG->wwwroot . '/' . IMG_FOLDER . '/teacher.png');
define('BADGES',$CFG->wwwroot . '/' . IMG_FOLDER . '/badges.png');
define('LOUPE',$CFG->wwwroot . '/' . IMG_FOLDER . '/loupe.png');

define('PERMISSION_USERS_COURSE', 'moodle/course:enrolreview');
define('PERMISSION_ENROL_TEACHERS', 'moodle/course:enrolconfig');
define('PERMISSION_MANAGE_BADGES', 'moodle/badges:createbadge');